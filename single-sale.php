<?php
/**
 * The template for displaying single post type sale
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package LaFleur
 */

get_header();
?>

        <!-- <section pagination start -->
		<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>	
		<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
			<div class="section-pagination__wrapper all-width">
				<ul class="breadcrumb">
					<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">La Fleur</a></li>
					<li><a href="<?php echo la_fleur_get_page( 'sales', 'url' ); ?>"><?php echo la_fleur_get_page( 'sales', 'title' ); ?></a></li>
					<li><?php the_title(); ?></li>
				</ul>
			</div>
		</div>
        
        <?php
        $singl_sale_content = get_field( 'singl_sale_content' );
        $singl_sale_start = get_field( 'singl_sale_start' );
        $singl_sale_end = get_field( 'singl_sale_end' );
        $button_show_all = get_field( 'button_show_all' );
        ?>
		<div class="page-this-sale w_100">
            <div class="page-this-sale_wr all-width d-f-row-stre-s_b">
                <!-- is mobile title -->
                <div class="page-this-sale_title w_100 is_mobile">
                    <h3><?php the_title(); ?></h3>
                </div>

                <!-- left -->
                <div class="page-this-sale_l">
					<?php
					if ( $singl_sale_content['image'] ) {
						echo '<img src="'. esc_url( $singl_sale_content['image']['url'] ) .'" alt="'. esc_html( $singl_sale_content['image']['alt'] ) .'">';
					} else {
						echo '<img src="'. LA_FLEUR_THEME_URI .'/img/png/sale-item.png" alt="sale img">';
					}
					?>
                    
                </div>

                <!-- right -->
                <div class="page-this-sale_r d-f-column-f_s-f_s">
                    <div class="page-this-sale_title w_100 is_descktop">
                        <h3><?php the_title(); ?></h3>
                    </div>

                    <div class="page-this-sale_description">
						<?php
						if ( ! empty( $singl_sale_content['p_one'] ) ) {
							echo '<h4>'. wp_kses_post( $singl_sale_content['p_one'] ) .'</h4>';
						}						
						if ( ! empty( $singl_sale_content['p_two'] ) ) {
							echo '<p>'. wp_kses_post( $singl_sale_content['p_two'] ) .'</p>';
						}
						if ( ! empty( $singl_sale_content['p_three'] ) ) {
							echo '<p>'. wp_kses_post( $singl_sale_content['p_three'] ) .'</p>';
						}
						if ( ! empty( $singl_sale_content['p_four'] ) ) {
							echo '<p>'. wp_kses_post( $singl_sale_content['p_four'] ) .'</p>';
						}
						?>
                    </div>

                    <div class="page-this-sale_system d-f-row-c-s_b w_100">		
						<div class="page-this-sale_system_date d-f-row-c-f_s">
							<?php if ( ! empty( $singl_sale_end ) && $singl_sale_end > time() ) { ?>
								<svg width="31" height="22" viewBox="0 0 31 22" fill="none"
									xmlns="http://www.w3.org/2000/svg">
									<circle cx="10.5303" cy="11.083" r="9.96191" stroke="black" />
									<line x1="10.6934" y1="11.5957" x2="17.1179" y2="11.5957" stroke="black"
										stroke-linecap="round" />
									<line x1="10.3691" y1="11.5957" x2="10.3691" y2="3.82119" stroke="black"
										stroke-linecap="round" stroke-linejoin="round" />
									<line x1="30.3574" y1="21.5449" x2="30.3574" y2="0.62112" stroke="black" />
								</svg>							
								<p><?php _e( 'Осталось ', 'la-fleur' ); ?><span><?php echo la_fleur_get_end_time( $singl_sale_end, true ); ?></span></p>
							<?php } ?>
						</div>						
						<div class="page-this-sale_system_link">
							<div class="all_link hover_effect-for">							
								<a href="<?php echo esc_url( home_url('/sales/') ); ?>">
									<p><?php echo esc_html( $button_show_all ); ?></p>
								</a>								
							</div>                           
						</div>                       
                    </div>
                </div>
            </div>
        </div>

<?php
get_footer();
