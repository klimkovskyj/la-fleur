<?php
/**
 * Template name: Template My Account
 *
 * @package LaFleur
 */

if ( ! is_user_logged_in() ) {
	wp_redirect( home_url() );    
    exit;
}

get_header();
?>
	
	<!-- <section pagination start -->
	<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>	
	<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
		<div class="section-pagination__wrapper all-width">
			<ul class="breadcrumb">
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">La Fleur</a></li>
				<li><?php the_title(); ?></li>
			</ul>
		</div>
	</div>	
	<!-- <section pagination end -->
        
    <!-- main content start -->
    <!-- <div class="main-content d-f-column-f_c-f_s">
        this div in the header -->


    <!-- page cabinet is descktop -->
	<div class="page-cabinet">
		<div class="page-cabinet_wr d-f-row-stre-s_b">

			<!-- left menu -->
			<div class="page-cabinet_l">
				<div class="page-cabinet_l_wr d-f-column-f_s-f_s">
					<?php
					do_action( 'woocommerce_account_navigation' );
					?>
				</div>
			</div>
			
			<!-- right data -->
			<div class="page-cabinet_r">
				<div class="page-cabinet_r_wr d-f-column-f_s-f_s">
					<?php do_action( 'woocommerce_account_content' ); ?>
					
					<?php //wc_get_template( 'myaccount/form-edit-account.php', array( 'user' => get_user_by( 'id', get_current_user_id() ) ) ); ?>
																				
				</div>
			</div>
			
		</div>
	</div>
        
        
<?php
get_footer();
