<?php
/**
 * Template name: Template Cart
 *
 * @package LaFleur
 */

get_header();
?>
	
	<!-- <section pagination start -->
	<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
	<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
		<div class="section-pagination__wrapper all-width">
			<ul class="breadcrumb">
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">La Fleur</a></li>
				<li><?php the_title(); ?></li>
			</ul>
		</div>
	</div>	
	<!-- <section pagination end -->
	
	<?php echo do_shortcode( '[woocommerce_cart]' ); ?>
	
<?php
get_footer();
	
