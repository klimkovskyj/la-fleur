<?php
/**
 * The template for displaying front page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package LaFleur
 */

get_header();

$fp_slider_text = get_field( 'fp_slider_text' );
$fp_slider_images = get_field( 'fp_slider_images' );
?>

        <!-- section main start -->
        <div class="section-main">
            <div class="section-main_wrapper all-width d-f-row-c-c">
                <div class="all_big_title">

                    <?php if ( ! empty( $fp_slider_text ) ) { ?>
						<?php if ( ! IS_MOBIL ) { ?>
							<h2 class="is_descktop animation-opacity _anim-items">
								<span><?php echo esc_html( $fp_slider_text['part_1'] ); ?> <span></span></span>
								<span><?php echo esc_html( $fp_slider_text['part_2'] ); ?> <?php echo esc_html( $fp_slider_text['part_3'] ); ?></span>
								<span><?php echo esc_html( $fp_slider_text['part_4'] ); ?> <?php echo esc_html( $fp_slider_text['part_5'] ); ?></span>
								<span></span>
							</h2>
						<?php } ?>

						<?php if ( IS_MOBIL ) { ?>
							<h2 class="is_mobile animation-opacity _anim-items">
								<span><?php echo esc_html( $fp_slider_text['part_1'] ); ?> <br><span></span> <span><?php echo esc_html( $fp_slider_text['part_2'] ); ?></span></span>
								<span> <?php echo esc_html( $fp_slider_text['part_3'] ); ?> <?php echo esc_html( $fp_slider_text['part_4'] ); ?> <br><span><?php echo esc_html( $fp_slider_text['part_5'] ); ?></span> <span></span></span>
							</h2>
						<?php } ?>
                    <?php } ?>
                    
                </div>

                <!-- animation-img _anim-items -->			
				<div class="section-main_big-img">
					<div class="section-mail-all-img_wrapper animation-img-right _anim-items">
						<?php
						if ( ! empty( $fp_slider_images['img_1'] ) ) {
							echo '<img src="'. esc_url( $fp_slider_images['img_1']['url'] ) .'" alt="'. esc_html( $fp_slider_images['img_1']['alt'] ) .'">';
						} else {	
							echo '<img src="'. LA_FLEUR_THEME_URI .'/img/png/main-big.png" alt="img">';
						}
						?>
					</div>
				</div>

				<div class="section-main_midle-img">
					<div class="section-mail-all-img_wrapper animation-img-left _anim-items">
						<?php
						if ( ! empty( $fp_slider_images['img_2'] ) ) {
							echo '<img src="'. esc_url( $fp_slider_images['img_2']['url'] ) .'" alt="'. esc_html( $fp_slider_images['img_2']['alt'] ) .'">';
						} else {	
							echo '<img src="'. LA_FLEUR_THEME_URI .'/img/png/main-midle.png" alt="img">';
						}
						?>
					</div>
				</div>

				<div class="section-main_small-img">
					<div class="section-mail-all-img_wrapper animation-img-left _anim-items">
						<?php
						if ( ! empty( $fp_slider_images['img_3'] ) ) {
							echo '<img src="'. esc_url( $fp_slider_images['img_3']['url'] ) .'" alt="'. esc_html( $fp_slider_images['img_3']['alt'] ) .'">';
						} else {	
							echo '<img src="'. LA_FLEUR_THEME_URI .'/img/png/main-small.png" alt="img">';
						}
						?>
					</div>
				</div>               
            </div>
        </div>
        <!-- section main end -->

        <?php
        $fp_brands = get_field( 'fp_brands' );
        $fp_brands_button = get_field( 'fp_brands_button' );
        ?>
        <!-- section brand is descktop start -->
        <?php if ( ! IS_MOBIL ) { ?>
			<div class="section-brand is_descktop">
				<div class="section-brand_wrapper all-width">

					<?php
					if ( ! empty( $fp_brands ) ) {
						$top_img_array = '';
						$bottom_img_array = '';
						foreach ( $fp_brands as $brand ) {
							// top_img
							$top_img_array .= '<div class="brand-slick_item">';
								$top_img_array .= '<div class="brand-slick_item__wrapper">';
									$top_img_array .= '<div class="brand_img">';								
										$top_img_array .= '<img src="'. esc_url( $brand['top_img']['url'] ) .'" alt="'. esc_html( $brand['top_img']['alt'] ) .'">';
									$top_img_array .= '</div>';
								$top_img_array .= '</div>';
							$top_img_array .= '</div>';
							
							// bottom_img
							$bottom_img_array .= '<div class="brand-slick_item-img animation-img-top _anim-items">';
								$bottom_img_array .= '<a href="'. esc_url( $brand['link'] ) .'">';
									$bottom_img_array .= '<img src="'. esc_url( $brand['bottom_img']['url'] ) .'" alt="'. esc_html( $brand['bottom_img']['alt'] ) .'">';
								$bottom_img_array .= '</a>';
							$bottom_img_array .= '</div>';
						}
					}
					?>
					<!-- slider top img -->
					<div class="section-brand_slick animation-border-top-left animation-border-bottom-right _anim-items">
						<button id="btn-prev_brand">
							<svg width="107" height="14" viewBox="0 0 107 14" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<line x1="106.218" y1="7.23535" x2="1.00041" y2="7.23537" stroke="black" />
								<line x1="7.28414" y1="13.0889" x2="0.866493" y2="6.67125" stroke="black" />
								<line x1="0.866173" y1="7.33492" x2="7.28382" y2="0.917274" stroke="black" />
							</svg>
						</button>

						
						<div id="section-all-breds">
							 <?php echo $top_img_array; ?>
						</div>

						<button id="btn-next_brand">
							<svg width="107" height="14" viewBox="0 0 107 14" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<line x1="0.592773" y1="6.77051" x2="105.81" y2="6.7705" stroke="black" />
								<line x1="99.5264" y1="0.916954" x2="105.944" y2="7.33461" stroke="black" />
								<line x1="105.944" y1="6.67094" x2="99.5267" y2="13.0886" stroke="black" />
							</svg>
						</button>
					</div>

					<!-- slider bottom img -->
					<div id="section-all-breds__img">
						<?php echo $bottom_img_array; ?>
					</div>

					<div class="all_link hover_effect-for">
						<?php
						if ( ! empty( $fp_brands_button ) ) {
							echo '<a href="'. esc_url( home_url( '/catalog/' ) ) .'">';
							echo '<p>'. esc_html( $fp_brands_button ) .'</p>';
							echo '</a>';
						}
						?>
					</div>
				</div>
			</div>
		<?php } ?>
		<!-- section brand is descktop end -->

        <!-- section brand is mobile start -->
		<?php if ( IS_MOBIL ) { ?>
			<div class="section-moile-brand is_mobile">
				<div class="section-moile-brand_wrapper d-f-column-f_s-f_s">

					<div id="mobile-brand-items" class="mobile-brand-items d-f-row-stre-f_s">
						<?php
							if ( ! empty( $fp_brands ) ) {
								$count_all_brands = count( $fp_brands );
								$counter = 1;
								$brands_number_start = array(1, 5, 9, 13, 17, 21, 25);
								$brands_number_end = array(4, 8, 12, 16, 20, 24, 28);
								foreach ( $fp_brands as $brand ) {
									if ( in_array( $counter, $brands_number_start ) ) {
										echo '<div class="moile-brand-this-item">';
									}
										if ( ! empty( $brand['link'] ) ) { $link = esc_url( $brand['link'] ); } else { $link = '#'; }
										echo '<div class="main-mobile-brand-item animation-img-top _anim-items">';
											echo '<a href="'. $link .'">';
												echo '<img src="'. esc_url( $brand['top_img']['url'] ) .'" alt="'. esc_html( $brand['top_img']['alt'] ) .'">';
											echo '</a>';
										echo '</div>';
									if ( in_array( $counter, $brands_number_end ) || $counter == $count_all_brands ) {
										echo '</div>';
									}
									$counter++;
								}
							}
						?>
					</div>

					<div class="mobile-brand-btn all-width d-f-row-stre-f_s">
						<button class="mobilebrand-arrow d-f-row-c-c" id="btn-prev_mobile-brand">
							<svg width="50" height="14" viewBox="0 0 50 14" fill="none" xmlns="http://www.w3.org/2000/svg">
								<line x1="50" y1="6.68457" x2="1" y2="6.68457" stroke="black" />
								<line x1="7.28414" y1="12.8184" x2="0.866493" y2="6.40075" stroke="black" />
								<line x1="0.866173" y1="7.06442" x2="7.28382" y2="0.646766" stroke="black" />
							</svg>
						</button>

						<button class="mobilebrand-arrow d-f-row-c-c" id="btn-next_mobile-brand">
							<svg width="50" height="14" viewBox="0 0 50 14" fill="none" xmlns="http://www.w3.org/2000/svg">
								<line x1="-4.37114e-08" y1="6.5" x2="49" y2="6.5" stroke="black" />
								<line x1="42.7159" y1="0.646447" x2="49.1335" y2="7.0641" stroke="black" />
								<line x1="49.1338" y1="6.40043" x2="42.7162" y2="12.8181" stroke="black" />
							</svg>
						</button>
					</div>

					<div class="all_link all-width hover_effect-for link-for-slider">
						<?php
						if ( ! empty( $fp_brands_button ) ) {
							echo '<a href="'. esc_url( home_url( '/catalog/' ) ) .'">';
							echo '<p>'. esc_html( $fp_brands_button ) .'</p>';
							echo '</a>';
						}
						?>
					</div>
				</div>
			</div>
		<?php } ?>
        <!-- section brand is mobile start -->

        <?php
        $fp_perfunery = get_field( 'fp_perfunery' );
        $fp_news = get_field( 'fp_news' );
        $fp_hits = get_field( 'fp_hits' );
        ?>
        <div class="section-perfunery all_padding">
            <div class="section-perfunery__wrapper d-f-column-f_s-f_s">

                <div class="section-perfunery_btn all-width d-f-row-c-s_b">
                    <div class="all-title-block animation-translateY _anim-items">
						<?php if ( ! empty( $fp_perfunery['title'] ) ) { ?>
                        <h2><?php echo esc_html( $fp_perfunery['title'] ); ?></h2>
                        <?php } ?>
                    </div>

                    <div class="main_small-filter d-f-row-c-f_s animation-translateY _anim-items">
                        <button id="btn-main-filter-news" class="active_filter-main" data-f="news">
                            <?php echo esc_html( $fp_perfunery['sub_title_1'] ); ?>
                        </button>

                        <div class="main_small-filter_line"></div>

                        <button id="btn-main-filter-hit" data-f="hit">
                            <?php echo esc_html( $fp_perfunery['sub_title_2'] ); ?>
                        </button>
                    </div>
                </div>

                <div class="section-perfumency_news" id="main-brand_show-news" data-f="news">
                    <div id="main-brand_show-news_slider" class="section-perfumency_news__wrapper d-f-row-stre-s_b">
                        <!-- item 1 -->
                        <?php					
						$posts = get_posts( array(
							'post_type' => 'product',
							'post_status' => 'publish',
							'include' => implode( ',', $fp_hits ),
							'orderby' => 'post__in',
						) );
						foreach ( $posts as $post ) {
							setup_postdata( $post );
							global $product;
							if ( empty( $product ) || ! $product->is_visible() ) { return; }
							
							?>
							<div class="perfumency-with-product">
								<?php wc_get_template_part( 'content', 'product' ); ?>
							</div>
					<?php } ?>
					<?php wp_reset_postdata(); ?>
                    </div>  

                    <div class="mobile-brand-btn is_mobile all-width d-f-row-stre-f_s">
                        <button class="mobilebrand-arrow d-f-row-c-c" id="btn-prev_mobile-perfumency-news">
                            <svg width="50" height="14" viewBox="0 0 50 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <line x1="50" y1="6.68457" x2="1" y2="6.68457" stroke="black" />
                                <line x1="7.28414" y1="12.8184" x2="0.866493" y2="6.40075" stroke="black" />
                                <line x1="0.866173" y1="7.06442" x2="7.28382" y2="0.646766" stroke="black" />
                            </svg>
                        </button>

                        <button class="mobilebrand-arrow d-f-row-c-c" id="btn-next_mobile-perfumency-news">
                            <svg width="50" height="14" viewBox="0 0 50 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <line x1="-4.37114e-08" y1="6.5" x2="49" y2="6.5" stroke="black" />
                                <line x1="42.7159" y1="0.646447" x2="49.1335" y2="7.0641" stroke="black" />
                                <line x1="49.1338" y1="6.40043" x2="42.7162" y2="12.8181" stroke="black" />
                            </svg>
                        </button>
                    </div>
                </div>

                <div class="section-perfumency_hit" id="main-brand_show-hit" data-f="hit">
                    <div id="main-brand_show-hit_slider" class="section-perfumency_hit__wrapper d-f-row-stre-s_b">
                        <!-- item 1 -->
                        <?php					
						$posts = get_posts( array(
							'post_type' => 'product',
							'post_status' => 'publish',
							'include' => implode( ',', $fp_news ),
							'orderby' => 'post__in',
						) );
						foreach ( $posts as $post ) {
							setup_postdata( $post );
							global $product;
							if ( empty( $product ) || ! $product->is_visible() ) { return; }
							
							?>
							<div class="perfumency-with-product">
								<?php wc_get_template_part( 'content', 'product' ); ?>
							</div>
					<?php } ?>
					<?php wp_reset_postdata(); ?>
                       
                    </div>


                    <div class="mobile-brand-btn is_mobile all-width d-f-row-stre-f_s">
                        <button class="mobilebrand-arrow d-f-row-c-c" id="btn-prev_mobile-perfumency-hit">
                            <svg width="50" height="14" viewBox="0 0 50 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <line x1="50" y1="6.68457" x2="1" y2="6.68457" stroke="black" />
                                <line x1="7.28414" y1="12.8184" x2="0.866493" y2="6.40075" stroke="black" />
                                <line x1="0.866173" y1="7.06442" x2="7.28382" y2="0.646766" stroke="black" />
                            </svg>
                        </button>

                        <button class="mobilebrand-arrow d-f-row-c-c" id="btn-next_mobile-perfumency-hit">
                            <svg width="50" height="14" viewBox="0 0 50 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <line x1="-4.37114e-08" y1="6.5" x2="49" y2="6.5" stroke="black" />
                                <line x1="42.7159" y1="0.646447" x2="49.1335" y2="7.0641" stroke="black" />
                                <line x1="49.1338" y1="6.40043" x2="42.7162" y2="12.8181" stroke="black" />
                            </svg>
                        </button>
                    </div>
                </div>

                <?php if ( ! empty( $fp_perfunery['bottom_buttom'] ) ) { ?>
					<div class="all_link link-for-slider hover_effect-for link-main-in-hit">                   
						<?php
							echo '<a href="'. esc_url( home_url( '/catalog/' ) ) .'">';
							echo '<p>'. esc_html( $fp_perfunery['bottom_buttom'] ) .'</p>';
							echo '</a>';
						?>
					</div>
                <?php } ?>

            </div>
        </div>
    
<?php
get_footer();
