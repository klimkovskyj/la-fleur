<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

$post_thumbnail_id = $product->get_image_id();
?>

		<!-- <section pagination start -->
        <?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
		<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
			<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
				<div class="section-pagination__wrapper all-width">
					<ul class="breadcrumb">
						<?php woocommerce_breadcrumb(); ?>
					</ul>
				</div>
			</div>
		<?php } ?>
        <!-- <section pagination end -->

        <div id="product-<?php the_ID(); ?>" class="section-product">
            <div class="section-product__wrapper all-width d-f-column-f_s-f_s">
                <!-- main  -->
                <div class="section-product-main d-f-row-stre-s_b">

                    <div class="section-product-main__img">
                        <div class="section-product-main__img__wrapper d-f-row-c-c animation-img-bottom _anim-items">
                            <?php
							if ( $product->get_image_id() ) {
								//$html = wc_get_gallery_image_html( $post_thumbnail_id, true );
								$html = woocommerce_get_product_thumbnail( 'full' );
							} else {
								$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
								$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
								$html .= '</div>';
							}

							echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id ); // phpcs:disable WordPress.XSS.EscapeOutput.OutputNotEscaped

							do_action( 'woocommerce_product_thumbnails' );
							?>
                        </div>
                                                 
						<?php
						$pa_product_kind = $product->get_attribute( 'pa_product-kind' );
						if ( ! empty( $pa_product_kind ) ) {									
							$pa_product_kind_arr = explode( ',', $pa_product_kind );
							echo '<div class="label_for_type-product label_for_type-product_in-pr">';
							echo '<p>';
							echo esc_html( $pa_product_kind_arr[0] );
							echo '</p>';
							echo '</div>';
						}
						?>
                                                 
                    </div>

                    <div class="section-product-main__description d-f-column-f_s-f_s">
                        <div class="section-product-main__description__wrapper">
                            <div class="page-product-brand animation-translateY _anim-items">
                                <h4>
                                <?php
								$pa_brand = $product->get_attribute( 'pa_brand' );
								if ( ! empty( $pa_brand ) ) {
									$pa_brand_arr = explode( ',', $pa_brand );
									echo esc_html( $pa_brand_arr[0] );
								}
								?>
                                </h4>
                            </div>

                            <div class="page-product-name-count d-f-row-c-s_b animation-translateY _anim-items">
                                <h3><?php echo get_the_title(); ?></h3>

                                <p>					
								<?php
									$price_html = $product->get_price_html();

									// Others product types than variable
									if ( ! $product->is_type('variable') ) {
										echo '<div class="perfumency-with-product_top__price is_descktop">';
										la_fleur_get_price( $product );
										echo '</div>';
										//echo '<span class="product-price">' . $price_html . '</span>';
									}
									// For variable products
									else {
										ob_start();

										?>
										<script type="text/javascript">							
										jQuery( function($){
											var p = '<?php echo $price_html; ?>', s = 'span.product-price';

											$( 'form.variations_form.cart' ).on('show_variation', function(event, data) {
												$(s).html(data.price_html); // Display the selected variation price
											});

											$( 'form.variations_form.cart' ).on('hide_variation', function() {
												$(s).html(p); // Display the variable product price range
											});
										});
										</script>
										<?php

										echo ob_get_clean() . '<span class="product-price">' . $price_html . '</span>';
									}
								?>																		
                                </p>
                            </div>

                            <div class="page-product_art_type d-f-row-c-f_s animation-opacity _anim-items">
                                <?php
                                $product_sku = $product->get_sku();
                                if ( ! empty( $product_sku ) ) {
									echo '<p>';
									_e( 'АРТ: ', 'la-fleur' );
									echo esc_html( $product_sku );
									echo '</p>';
								}
                                
								$pa_product_type = $product->get_attribute( 'pa_product-type' );
								if ( ! empty( $pa_product_type ) ) {
									$pa_product_type_arr = explode( ',', $pa_product_type );
									echo '<div class="line-for-art-type"></div>';
									echo '<p>';
									echo esc_html( $pa_product_type_arr[0] );
									echo '</p>';
									//echo '';
								}
								?>
                            </div>    
                            <div
                                class="sectio_mobile-product_price is_mobile animation-opacity _anim-items d-f-row-c-c">
                                
                                <div class="this_mobile_count_for-pr">
									<?php
										$price_html = $product->get_price_html();

										// Others product types than variable
										if ( ! $product->is_type('variable') ) {
											echo '<div class="perfumency-with-product_top__price f-s-36">';
											la_fleur_get_price( $product );
											echo '</div>';
										}
										// For variable products
										else {
											ob_start();

											?>
											<script type="text/javascript">
											jQuery( function($){
												var p = '<?php echo $price_html; ?>', s = 'span.product-price';

												$( 'form.variations_form.cart' ).on('show_variation', function(event, data) {
													$(s).html(data.price_html); // Display the selected variation price
												});

												$( 'form.variations_form.cart' ).on('hide_variation', function() {
													$(s).html(p); // Display the variable product price range
												});
											});
											</script>
											<?php

											echo ob_get_clean() . '<span class="product-price">' . $price_html . '</span>';
										}
									?>
                                </div>
                            </div>

                            <div class="page-product_description product_width animation-opacity _anim-items">
                                <p>
                                    <span class="small-description-product">
                                        <?php the_content(); ?>
                                    </span>
                                    <span class="big-description-product">
                                        <?php the_excerpt(); ?>
                                    </span>
                                </p>

                                <button id="btn-more-description-product" class="is_mobile"><?php _e( 'Больше', 'la-fleur' ); ?></button>
                            </div>
                                                 
                            <?php woocommerce_template_single_add_to_cart(); ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $product_c_c = get_field( 'product_custom_content' );
        $product_c_c_svd = '<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">';
		$product_c_c_svd .= '<line y1="-0.5" x2="20.7549" y2="-0.5" transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 15.0332 0)" stroke="black" />';
		$product_c_c_svd .= '<line x1="0.5" y1="2.34906" x2="0.500001" y2="14.5694" stroke="black" />';
		$product_c_c_svd .= '<line x1="12.5146" y1="15.0695" x2="0.000465348" y2="15.0695" stroke="black" />';
		$product_c_c_svd .= '</svg>';
        
        if ( ! empty( $product_c_c ) ) {
        ?>
			<div class="section-product_description">
				<div class="section-product_description_wr all-width d-f-row-f_s-s_b">
					<div class="section-product_description_l d-f-column-f_s-f_s">
						<?php if ( ! empty( $product_c_c['title_one'] ) && ! empty( $product_c_c['desc_one'] ) ) { ?>
							<div class="s-pr-desc_item animation-opacity _anim-items">
								<button>
									<?php echo esc_html( $product_c_c['title_one'] ); ?>
									<?php echo $product_c_c_svd; ?>
								</button>

								<div class="s-pr-desc_item_show">
									<p><?php echo wp_kses_post( $product_c_c['desc_one'] ) ?></p>
								</div>
							</div>
						<?php } ?>

					   <?php if ( ! empty( $product_c_c['title_two'] ) && ! empty( $product_c_c['desc_two'] ) ) { ?>
							<div class="s-pr-desc_item animation-opacity _anim-items">
								<button>
									<?php echo esc_html( $product_c_c['title_two'] ); ?>
									<?php echo $product_c_c_svd; ?>
								</button>

								<div class="s-pr-desc_item_show">
									<p><?php echo wp_kses_post( $product_c_c['desc_two'] ) ?></p>
								</div>
							</div>
						<?php } ?>

						<?php if ( ! empty( $product_c_c['title_three'] ) && ! empty( $product_c_c['desc_three'] ) ) { ?>
							<div class="s-pr-desc_item animation-opacity _anim-items">
								<button>
									<?php echo esc_html( $product_c_c['title_three'] ); ?>
									<?php echo $product_c_c_svd; ?>
								</button>

								<div class="s-pr-desc_item_show">
									<p><?php echo wp_kses_post( $product_c_c['desc_three'] ) ?></p>
								</div>
							</div>
						<?php } ?>

						<?php if ( ! empty( $product_c_c['title_four'] ) && ! empty( $product_c_c['desc_four'] ) ) { ?>
							<div class="s-pr-desc_item animation-opacity _anim-items">
								<button>
									<?php echo esc_html( $product_c_c['title_four'] ); ?>
									<?php echo $product_c_c_svd; ?>
								</button>

								<div class="s-pr-desc_item_show">
									<p><?php echo wp_kses_post( $product_c_c['desc_four'] ) ?></p>
								</div>
							</div>
						<?php } ?>

						<?php if ( ! empty( $product_c_c['title_five'] ) && ! empty( $product_c_c['desc_five'] ) ) { ?>
							<div class="s-pr-desc_item animation-opacity _anim-items">
								<button>
									<?php echo esc_html( $product_c_c['title_five'] ); ?>
									<?php echo $product_c_c_svd; ?>
								</button>

								<div class="s-pr-desc_item_show">
									<p><?php echo wp_kses_post( $product_c_c['desc_five'] ) ?></p>
								</div>
							</div>
						<?php } ?>
					</div>

					<div class="section-product_description_r animation-opacity _anim-items">
						<?php if ( ! empty( $product_c_c['video'] ) ) { ?>
							<?php if ( ! empty( $product_c_c['poster'] ) ) { $poster = esc_url( $product_c_c['poster']['url'] ); } else { $poster = get_template_directory() . '/img/png/img_for_video.png'; } ?>
							<video controls poster="<?php echo $poster; ?>">
								<source
									src="<?php echo esc_url( $product_c_c['video'] ); ?>"
									type="video/mp4">
							</video>
						<?php } ?>	
					</div>
				</div>
			</div>
		<?php } ?>

        <!-- section beauty  -->
        <?php
        $beauty_set_ID = $product_c_c['beauty_set'];
		$beauty_set = get_field( 'single_beauty_set_one', $beauty_set_ID );
		
		if ( ! empty( $beauty_set ) ) {
        ?>
        <div class="section-product-beauty">
            <div class="section-product-beauty__wrapper">
                <!-- left -->
                <div class="section-product-beauty__left">
                    <div class="beauty-description all-width">
                        <div class="beauty-description_info">
                            <div class="beauty-description_info_-wrapper d-f-column-f_s-f_s">
                                <!-- title -->
                                <?php
								//global $post;                       
																													
								$beauty_extra_products = '';
								$beauty_extra_products_img = '';
								$beauty_extra_products_big_img = '';
								$beauty_extra_products_total = 0;
								$img_count = 1;
								$array_add_to_cart = array();
								
								//var_dump($beauty_set);
								foreach ( $beauty_set as $post ) {
									setup_postdata( $post );
									global $product;
									if ( empty( $product ) || ! $product->is_visible() || ! $product->is_purchasable() || ! $product->is_in_stock() ) { return; }
									$array_add_to_cart[] = $post->ID;
									
									$beauty_extra_products .= '<li>';
									$beauty_extra_products .= '<a href="'. esc_url( get_the_permalink() ) .'">';
									$beauty_extra_products .= get_the_title();
									$beauty_extra_products .= '</a></li>';
									
									// small
									$beauty_extra_products_img .= '<div class="beauty-min-slider-item" data-product-beauty-item="'. $img_count .'">';
									$beauty_extra_products_img .= '<a href="'. esc_url( get_the_permalink() ) .'">';
									$beauty_extra_products_img .= woocommerce_get_product_thumbnail('full');
									$beauty_extra_products_img .= '</a></div>';
									
									// big
									$beauty_extra_products_big_img .= '<div class="beauty-big-slider-item" data-product-beauty-item="'. $img_count .'">';
									$beauty_extra_products_big_img .= '<a href="'. esc_url( get_the_permalink() ) .'">';
									$beauty_extra_products_big_img .= '<div class="beauty-big-slider_img">';
									$beauty_extra_products_big_img .= woocommerce_get_product_thumbnail('full');
									$beauty_extra_products_big_img .= '</div>';
									$beauty_extra_products_big_img .= '<div class="beauty-big-slider_name">';
									$beauty_extra_products_big_img .= '<h4>'. get_the_title() .'</h4>';
									$beauty_extra_products_big_img .= '</div>';
									
									$pa_brand = $product->get_attribute( 'pa_brand' );
									if ( ! empty( $pa_brand ) ) {
										$pa_brand_arr = explode( ',', $pa_brand );
										$beauty_extra_products_big_img .= '<div class="beauty-big-slider_brand">';
										$beauty_extra_products_big_img .= '<h4>'. esc_html( $pa_brand_arr[0] ) .'</h4>';
										$beauty_extra_products_big_img .= '</div>';
									}
									$pa_product_type = $product->get_attribute( 'pa_product-type' );
									if ( ! empty( $pa_product_type ) ) {
										$pa_product_type_arr = explode( ',', $pa_product_type );
										$beauty_extra_products_big_img .= '<div class="beauty-big-slider_type">';
										$beauty_extra_products_big_img .= '<p>'. esc_html( $pa_product_type_arr[0] ) .'</p>';
										$beauty_extra_products_big_img .= '</div>';
									}
										
									$beauty_extra_products_big_img .= '</a></div>';
									
									$beauty_extra_products_total += $product->get_price();
									
									$img_count++;
									
								}
								wp_reset_postdata();
																
								$beauty_set_info = get_field( 'single_beauty_set_info', $beauty_set_ID );
								?>
                                
                                <div class="all-title-block animation-translateY _anim-items">
                                    <h2><?php echo get_the_title( $beauty_set_ID ); ?></h2>                                   
                                </div>

                                <!-- small info -->
                                <div class="beauty-small-info animation-opacity _anim-items">
                                    <p>
										<?php 
										if ( ! empty( $beauty_set_info['small_info'] ) ) {
											echo '<span class="small-info">';
											echo wp_kses_post( $beauty_set_info['small_info'] );
											echo '</span>';
										}
										if ( ! empty( $beauty_set_info['big_info'] ) ) {
											echo '<span class="big-info">';
											echo wp_kses_post( $beauty_set_info['big_info'] );
											echo '</span>';
										}
										?>
                                    </p>

                                    <button id="btn-product-more-info" class="is_mobile"><?php _e( 'Больше', 'la-fleur' ); ?></button>
                                </div>

                                <div class="block_beauty_buy-and-show animation-opacity _anim-items">
                                    <!-- first option always '' -->
                                    <div class="block-all-product-for-beauty">
                                        <div class="block-all-product-for-beauty_btn" id="btnn-show-all-product-beauty">
                                            <button><?php echo esc_html( $beauty_set_info['title_text'] ); ?></button>
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <line y1="-0.5" x2="20.7549" y2="-0.5"
                                                    transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 15.0332 0)"
                                                    stroke="black" />
                                                <line x1="0.5" y1="2.34912" x2="0.500001" y2="14.5695" stroke="black" />
                                                <line x1="12.5146" y1="15.0695" x2="0.000465348" y2="15.0695"
                                                    stroke="black" />
                                            </svg>
                                        </div>

                                        <div class="show-more-product-beauty show-more-product-beauty_desctop ">
                                            <ul>
                                                <?php echo $beauty_extra_products; ?>
                                            </ul>
                                        </div>

                                    </div>

                                    <div class="this_mobile_count_for-pr this_mobile_count_mobile-beauty is_mobile">
                                        <?php echo $beauty_extra_products_total . get_woocommerce_currency_symbol(); ?>
                                    </div>

                                    <!-- btn -->
                                    <div class="product_sm-inf-pr is_descktop">
                                        <p><?php echo $beauty_extra_products_total . get_woocommerce_currency_symbol(); ?></p>
                                        <div class="all_link hover_effect-for btn-beauty-buy">
                                            <button class="btn_array_add_to_cart">
                                                <p><?php echo esc_html( $beauty_set_info['button'] ); ?></p>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="all_link hover_effect-for btn-beauty-buy is_mobile">
                                        <button class="btn_array_add_to_cart">
                                            <p><?php echo esc_html( $beauty_set_info['button'] ); ?></p>
                                        </button>
                                    </div>

									<input id="array_add_to_cart" type="hidden" value="<?php echo implode( ',', $array_add_to_cart ); ?>">
                                </div>

                                <div class="show-more-product-beauty show-more-product-beauty_mobile">
                                    <ul>
                                        <?php echo $beauty_extra_products; ?>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="beauty-min-slider">
                        <div class="beauty-min-slider__wrapper all-width">
                            <div class="beauty-min-slider__container">
                                <div class="beauty-min-slider-items" id="product-small-slider">
                                    <?php echo $beauty_extra_products_img; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- right -->
                <div class="section-product-beauty__right">
                    <div class="section-product-beauty__right__wrapper">

                        <div class="beauty-big-slider-items">
                            <div class="beauty-big-slider-items_container" id="product-big-slider">
                                
                                <?php echo $beauty_extra_products_big_img; ?>
                                
                            </div>


                            <!-- btn -->
                            <div class="beauty-big-slider_btn">
                                <!-- left -->
                                <button class="beauty-big-slider_btn__arrow" id="btn-left-product-big-slider">
                                    <svg width="107" height="14" viewBox="0 0 107 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <line x1="106.219" y1="7.23242" x2="1.00139" y2="7.23244" stroke="black" />
                                        <line x1="7.28512" y1="13.086" x2="0.867469" y2="6.66832" stroke="black" />
                                        <line x1="0.86715" y1="7.33199" x2="7.2848" y2="0.914344" stroke="black" />
                                    </svg>
                                </button>

                                <!-- right -->
                                <button class="beauty-big-slider_btn__arrow" id="btn-right-product-big-slider">
                                    <svg width="107" height="14" viewBox="0 0 107 14" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <line x1="0.902344" y1="6.76782" x2="106.12" y2="6.76781" stroke="black" />
                                        <line x1="99.836" y1="0.914269" x2="106.254" y2="7.33192" stroke="black" />
                                        <line x1="106.254" y1="6.66825" x2="99.8363" y2="13.0859" stroke="black" />
                                    </svg>
                                </button>
                            </div>

                            <div class="all_link hover_effect-for link-for-mobile-beauty is_mobile">
                                <a href="<?php echo esc_url( home_url( '/catalog/' ) ); ?>">
                                    <p><?php echo esc_html( $beauty_set_info['show_all'] ); ?></p>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
		}
		?>

<?php do_action( 'woocommerce_after_single_product' ); ?>
