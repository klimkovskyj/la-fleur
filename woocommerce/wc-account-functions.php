<?php
/**
 * WooCommerce Account Functions
 *
 * Functions for account specific things.
 *
 * @package LaFleur
 */


/**
 * Get account menu item classes.
 *
 * @since 2.6.0
 * @param string $endpoint Endpoint.
 * @return string
 */
function la_fleur_get_account_menu_item_classes( $endpoint ) {
	global $wp;

	$classes = '';

	// Set current item class.
	$current = isset( $wp->query_vars[ $endpoint ] );
	if ( 'dashboard' === $endpoint && ( isset( $wp->query_vars['page'] ) || empty( $wp->query_vars ) ) ) {
		$current = true; // Dashboard is not an endpoint, so needs a custom check.
	} elseif ( 'orders' === $endpoint && isset( $wp->query_vars['view-order'] ) ) {
		$current = true; // When looking at individual order, highlight Orders list item (to signify where in the menu the user currently is).
	} elseif ( 'all-orders' === $endpoint && isset( $wp->query_vars['all-orders'] ) ) {
		$current = true; // When looking at individual order, highlight Orders list item (to signify where in the menu the user currently is).
	} elseif ( 'edit-address' === $endpoint && isset( $wp->query_vars['edit-address'] ) ) {
		$current = true;
	} elseif ( 'edit-account' === $endpoint && isset( $wp->query_vars['edit-account'] ) ) {
		$current = true;
	} elseif ( 'change-password' === $endpoint && isset( $wp->query_vars['change-password'] ) ) {
		$current = true;
	} elseif ( 'customer-logout' === $endpoint && isset( $wp->query_vars['customer-logout'] ) ) {
		$current = true;
	}
	
	if ( $current === true ) {
		$classes = 'active_link-cabinet';
	}

	return $classes;
}

function la_fleur_get_order_status( $status ) {
	$class = '';
	
	if ( $status == 'pending' ) {
		$class = 'yr-order-pending';
	} elseif ( $status == 'processing' ) {
		$class = 'yr-order-processing';
	} elseif ( $status == 'on-hold' ) {
		$class = 'yr-order-cancel';
	} elseif ( $status == 'completed' ) {
		$class = 'yr-order-done';
	} elseif ( $status == 'cancelled' ) {
		$class = 'yr-order-cancelled';
	}  elseif ( $status == 'refunded' ) {
		$class = 'yr-order-refunded';
	} elseif ( $status == 'failed' ) {
		$class = 'yr-order-failed';
	} else {
		$class = 'yr-order-yellow';
	}
	return $class;
}

// Menu of my account (navigation.php)
add_filter( 'woocommerce_account_menu_items', 'la_fleur_my_account_menu_items' );

function la_fleur_my_account_menu_items( $items ) {
    $page = get_page_by_path( '/my-account/' );
    $page_my_account = get_field( 'page_my_account', $page->ID );
    
    $items = array(
		'dashboard'       => esc_html( $page_my_account['data_title'] ),//__( 'Мои данные', 'la-fleur' ),
		'all-orders'      => esc_html( $page_my_account['orders_title'] ),//__( 'История покупок', 'la-fleur' ),	
		'edit-address'    => esc_html( $page_my_account['address_title'] ),//__( 'Адрес для доставки', 'la-fleur' ),
		'change-password' => esc_html( $page_my_account['change_password_title'] ),//__( 'Сменить пароль', 'la-fleur' ),		
		'customer-logout' => esc_html( $page_my_account['logout_title'] ),//__( 'Выйти', 'la-fleur' ),
	);
    
    return $items;
}

// Required fields remove
add_filter('woocommerce_save_account_details_required_fields', 'la_fleur_myaccount_required_fields');

function la_fleur_myaccount_required_fields( $account_fields ) { 
	unset( $account_fields['account_display_name'] );
 
	return $required_fields;
}

// woocommerce_save_account_details, city and phone at my account
add_filter('woocommerce_save_account_details', 'la_fleur_save_account_details');

function la_fleur_save_account_details( $curr_user_id ) {
	if ( is_page_template('tpl-my-account.php') && isset( $_POST['city-cabinet-data'] ) ) {
		$city = get_user_meta( $curr_user_id, 'city', true );
		if ( $_POST['city-cabinet-data'] != esc_html( $city ) ) {
			update_user_meta( $curr_user_id, 'city', esc_html( $_POST['city-cabinet-data'] ) );
		}
	}
	
	if ( is_page_template('tpl-my-account.php') && isset( $_POST['phone-cabinet-data'] ) ) {
		$phone = get_user_meta( $curr_user_id, 'phone', true );
		if ( $_POST['phone-cabinet-data'] != esc_html( $phone ) ) {
			update_user_meta( $curr_user_id, 'phone', esc_html( $_POST['phone-cabinet-data'] ) );
		}
	}
	//return $curr_user_id;
}

// endpoint all-orders, parse url, get 'm'
function la_fleur_parse_url_get_m() {
	$m = 0;
	if ( ! empty( get_query_var('all-orders') ) ) {
		$all_orders = get_query_var('all-orders');
		$arr = explode( '/', $all_orders );
		foreach ( $arr as $key => $value ) {
			if ( $value == 'm' ) {
				$key_m = (int) $key + 1;
				if ( ! empty( $arr[$key_m] ) ) {
					$m = $arr[$key_m];
				}
			}
		}
	}
	return $m;
}

// endpoint all-orders, parse url, get 'paged'
function la_fleur_parse_url_get_paged() {
	$paged = 1;
	if ( ! empty( get_query_var('all-orders') ) ) {
		$all_orders = get_query_var('all-orders');
		$arr = explode( '/', $all_orders );
		foreach ( $arr as $key => $value ) {
			if ( $value == 'paged' ) {
				$key_paged = (int) $key + 1;
				if ( ! empty( $arr[$key_paged] ) ) {
					$paged = $arr[$key_paged];
				}
			}
		}
	}
	return $paged;
}

// filter-by-date name="m"
function la_fleur_months_dropdown() {
	global $wpdb, $wp_locale;
	$post_type = 'shop_order';
	/**
	 * Filters whether to remove the 'Months' drop-down from the post list table.
	 *
	 * @since 4.2.0
	 *
	 * @param bool   $disable   Whether to disable the drop-down. Default false.
	 * @param string $post_type The post type.
	 */
	if ( apply_filters( 'disable_months_dropdown', false, $post_type ) ) {
		return;
	}

	$extra_checks = "AND post_status != 'auto-draft'";
	if ( ! isset( $_GET['post_status'] ) || 'trash' !== $_GET['post_status'] ) {
		$extra_checks .= " AND post_status != 'trash'";
	} elseif ( isset( $_GET['post_status'] ) ) {
		$extra_checks = $wpdb->prepare( ' AND post_status = %s', $_GET['post_status'] );
	}

	$months = $wpdb->get_results(
		$wpdb->prepare(
			"
		SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
		FROM $wpdb->posts
		WHERE post_type = %s
		$extra_checks
		ORDER BY post_date DESC
	",
			$post_type
		)
	);

	/**
	 * Filters the 'Months' drop-down results.
	 *
	 * @since 3.7.0
	 *
	 * @param object[] $months    Array of the months drop-down query results.
	 * @param string   $post_type The post type.
	 */
	$months = apply_filters( 'months_dropdown_results', $months, $post_type );

	$month_count = count( $months );

	if ( ! $month_count || ( 1 == $month_count && 0 == $months[0]->month ) ) {
		return;
	}

	$m = la_fleur_parse_url_get_m();
	
	//$m = isset( $_GET['m'] ) ? (int) $_GET['m'] : 0;
	$all_orders_url = esc_url( home_url( '/my-account/all-orders/' ) );
	?>
	<select class="small-select select2-hidden-accessible iv-filter-by-date"
		name="m" id="select-shop-sort" tabindex="-1"
		aria-hidden="true">	
		<option<?php selected( $m, 0 ); ?> value="<?php echo $all_orders_url; ?>"><?php _e( 'All dates' ); ?></option>
	<?php
	foreach ( $months as $arc_row ) {
		if ( 0 == $arc_row->year ) {
			continue;
		}

		$month = zeroise( $arc_row->month, 2 );
		$year  = $arc_row->year;

		printf(
			"<option %s value='%s'>%s</option>\n",
			selected( $m, $year . $month, false ),
			esc_attr( home_url( '/my-account/all-orders/m/' ) . $arc_row->year . $month ),
			/* translators: 1: Month name, 2: 4-digit year. */
			sprintf( __( '%1$s %2$d' ), $wp_locale->get_month( $month ), $year )
		);
	}
	?>
	</select>
	<?php
}

// Custom endpoints: all-orders, change-password
add_action( 'init', 'la_fleur_all_orders_endpoints' );

function la_fleur_all_orders_endpoints() {
    add_rewrite_endpoint( 'all-orders', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'change-password', EP_ROOT | EP_PAGES );
}

add_filter( 'query_vars', 'la_fleur_all_orders_query_vars', 0 );

function la_fleur_all_orders_query_vars( $vars ) {
    $vars[] = 'all-orders';
    $vars[] = 'change-password';
    return $vars;
}

// My Account > All-orders template.
add_action( 'woocommerce_account_all-orders_endpoint', 'la_fleur_all_orders_endpoint_content' );

function la_fleur_all_orders_endpoint_content() {
    $current_page = la_fleur_parse_url_get_paged();
	//$current_page = empty( $current_page ) ? 1 : absint( $current_page );
	//$all_orders = ! empty( get_query_var('all-orders') ) ? (int) get_query_var('all-orders') : '';
	$m = la_fleur_parse_url_get_m();
	
	$customer_orders = wc_get_orders(
		apply_filters(
			'woocommerce_my_account_my_orders_query',
			array(
				'customer' => get_current_user_id(),
				'page'     => $current_page,
				'paginate' => true,
				'm' => $m,
			)
		)
	);

	wc_get_template(
		'myaccount/all-orders.php',
		array(
			'current_page'    => absint( $current_page ),
			'customer_orders' => $customer_orders,
			'has_orders'      => 0 < $customer_orders->total,
		)
	);
}

// My Account > Change-password template.
add_action( 'woocommerce_account_change-password_endpoint', 'la_fleur_change_password_endpoint_content' );

function la_fleur_change_password_endpoint_content() {
    //echo '<h2>Test Change Password</h2>';
    wc_get_template(
		'myaccount/change-password.php',
		array(
			'customer' => get_current_user_id(),
		)
	);
}

// My Account > logout redirect
add_action('wp_logout','logout_redirect');

function logout_redirect(){
    wp_redirect( home_url() );    
    exit;
}

// Save the password details and redirect back to the my account page.
add_action( 'template_redirect', 'la_fleur_save_change_password_details' );

function la_fleur_save_change_password_details() {
	$nonce_value = wc_get_var( $_REQUEST['save-account-details-nonce'], wc_get_var( $_REQUEST['_wpnonce'], '' ) ); // @codingStandardsIgnoreLine.

	if ( ! wp_verify_nonce( $nonce_value, 'save_account_details' ) ) {
		return;
	}

	if ( empty( $_POST['action'] ) || 'la_fleur_save_changed_password' !== $_POST['action'] ) {
		return;
	}

	wc_nocache_headers();

	$user_id = get_current_user_id();

	if ( $user_id <= 0 ) {
		return;
	}

	$pass_cur             = ! empty( $_POST['password_current'] ) ? $_POST['password_current'] : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash
	$pass1                = ! empty( $_POST['password_1'] ) ? $_POST['password_1'] : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash
	//$pass2                = ! empty( $_POST['password_2'] ) ? $_POST['password_2'] : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash
	$save_pass            = true;

	// Current user data.
	$current_user       = get_user_by( 'id', $user_id );
	
	// New user data.
	$user               = new stdClass();
	$user->ID           = $user_id;

	if ( ! empty( $pass_cur ) && empty( $pass1 ) ) {
		wc_add_notice( __( 'Please fill out all password fields.', 'woocommerce' ), 'error' );
		$save_pass = false;
	} elseif ( ! empty( $pass1 ) && empty( $pass_cur ) ) {
		wc_add_notice( __( 'Please enter your current password.', 'woocommerce' ), 'error' );
		$save_pass = false;
	} elseif ( ! empty( $pass1 ) && ! wp_check_password( $pass_cur, $current_user->user_pass, $current_user->ID ) ) {
		wc_add_notice( __( 'Your current password is incorrect.', 'woocommerce' ), 'error' );
		$save_pass = false;
	}

	if ( $pass1 && $save_pass ) {
		$user->user_pass = $pass1;
	}

	// Allow plugins to return their own errors.
	$errors = new WP_Error();
	do_action_ref_array( 'woocommerce_save_account_details_errors', array( &$errors, &$user ) );

	if ( $errors->get_error_messages() ) {
		foreach ( $errors->get_error_messages() as $error ) {
			wc_add_notice( $error, 'error' );
		}
	}

	if ( wc_notice_count( 'error' ) === 0 ) {
		wp_update_user( $user );

	}

	wc_add_notice( __( 'Пароль изменён.', 'la-fleur' ) );

	//do_action( 'woocommerce_save_account_details', $user->ID );

	wp_safe_redirect( wc_get_endpoint_url( 'change-password', '', wc_get_page_permalink( 'myaccount' ) ) );
	exit;
}

// Save and edit address details and redirect back to the my address endpoint.
add_action( 'template_redirect', 'la_fleur_save_list_addresses' );

function la_fleur_save_list_addresses() {
	$nonce_value = wc_get_var( $_REQUEST['woocommerce-edit-address-nonce'], wc_get_var( $_REQUEST['_wpnonce'], '' ) ); // @codingStandardsIgnoreLine.

	if ( ! wp_verify_nonce( $nonce_value, 'woocommerce-edit_address' ) ) {
		return;
	}

	if ( empty( $_POST['action'] ) || 'la_fleur_edit_address' !== $_POST['action'] ) {
		return;
	}

	wc_nocache_headers();

	$user_id = get_current_user_id();

	if ( $user_id <= 0 ) {
		return;
	}
	
	// shipping_city
	$old_address = get_user_meta( $user_id, 'la_fleur_list_addresses', true );
	if ( ! empty( $_POST['city-personal-address'] ) && ! empty( $_POST['house-personal-address'] ) && ! empty( $_POST['street-personal-address'] ) && ! empty( $_POST['flat-personal-address'] ) ) {
		$new_address = array();
		$rand_id = ! empty( $_POST['address_id'] ) ? esc_html( $_POST['address_id'] ) : mt_rand();
		
		$new_address['active_shipping_address'] =  ! empty( $_POST['active_shipping_address'] ) ? esc_html( $_POST['active_shipping_address'] ) : 'not';		
		$new_address['city'] = esc_html( $_POST['city-personal-address'] );
		$new_address['street'] = esc_html( $_POST['street-personal-address'] );
		$new_address['house'] = esc_html( $_POST['house-personal-address'] );		
		$new_address['flat'] = esc_html( $_POST['flat-personal-address'] );
		
		if ( ! empty( $old_address ) ) {
			$old_address[$rand_id] = $new_address;
			$change_result = update_user_meta( $user_id, 'la_fleur_list_addresses', $old_address );
		} else {
			$old_address = array();
			$new_address['active_shipping_address'] = 'yes';
			$old_address[$rand_id] = $new_address;
			$change_result = update_user_meta( $user_id, 'la_fleur_list_addresses', $old_address );			
			
			//update_user_meta( $user_id, 'shipping_address_1', $new_address['address_1'] );
			//update_user_meta( $user_id, 'shipping_address_2', $new_address['address_2'] );
			$shipping_address_1 = $new_address['street'] .' '. $new_address['house'] .' '.  $new_address['flat'];
			update_user_meta( $user_id, 'shipping_address_1', $shipping_address_1 );
			
			update_user_meta( $user_id, 'city', $new_address['city'] );
			update_user_meta( $user_id, 'la_fleur_shipping_street', $new_address['street'] );
			update_user_meta( $user_id, 'la_fleur_shipping_house', $new_address['house'] );
			update_user_meta( $user_id, 'la_fleur_shipping_flat', $new_address['flat'] );
		}
		if ( $change_result ) {
			wc_add_notice( __( 'Адрес успешно добавлен.', 'la-fleur' ) );
		}
	} else {
		wc_add_notice( __( 'При изменении адреса возникла ошыбка.', 'la-fleur' ) );
	}

	wp_safe_redirect( wc_get_endpoint_url( 'edit-address', '', wc_get_page_permalink( 'myaccount' ) ) );
	exit;
}

// My account - remove address
add_action( 'wp_ajax_my_account_remove_address', 'callback_my_account_remove_address' );
add_action( 'wp_ajax_nopriv_my_account_remove_address', 'callback_my_account_remove_address' );

function callback_my_account_remove_address() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' );
	
	$return = array();
	if ( empty( $_POST['address_id'] ) ) {
		$return['text'] = __( 'Неизвестная ошыбка.', 'la-fleur' );
		echo json_encode( $return );
		wp_die();
	} else {
		$address_id = esc_html( $_POST['address_id'] );
		$user_id = get_current_user_id();

		if ( $user_id <= 0 ) {
			return;
		}
		
		$old_address = get_user_meta( $user_id, 'la_fleur_list_addresses', true );
		foreach ( $old_address as $key => $address_arr ) {
			if ( $key == (int) $address_id ) {			
				
				if ( $address_arr['active_shipping_address'] == 'yes' ) {
					delete_user_meta( $user_id, 'shipping_address_1' );
					
					delete_user_meta( $user_id, 'city' );
					delete_user_meta( $user_id, 'la_fleur_shipping_street' );
					delete_user_meta( $user_id, 'la_fleur_shipping_house' );
					delete_user_meta( $user_id, 'la_fleur_shipping_flat' );
				}
				
				unset( $old_address[$key] );
				
				$result = update_user_meta( $user_id, 'la_fleur_list_addresses', $old_address );
				
				if ( $result ) {
					$return['text'] = __( 'Адрес удалён.', 'la-fleur' );
					echo json_encode( $return );
					wp_die();
				}
			}
		}
		$return['text'] = __( 'Неизвестная ошыбка.', 'la-fleur' );	
		echo json_encode( $return );
		wp_die();
	}
	
}

// My account - active shipping address
add_action( 'wp_ajax_my_account_active_shipping_address', 'callback_my_account_active_shipping_address' );
add_action( 'wp_ajax_nopriv_my_account_active_shipping_address', 'callback_my_account_active_shipping_address' );

function callback_my_account_active_shipping_address() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' );
	
	$return = array();
	if ( empty( $_POST['address_id'] ) ) {
		$return['text'] = __( 'Неизвестная ошыбка.', 'la-fleur' );
		echo json_encode( $return );
		wp_die();
	} else {
		$address_id = esc_html( $_POST['address_id'] );
		$user_id = get_current_user_id();

		if ( $user_id <= 0 ) {
			return;
		}
		
		$old_address = get_user_meta( $user_id, 'la_fleur_list_addresses', true );
		
		foreach ( $old_address as $key => &$address_arr ) {
			$address_arr['active_shipping_address'] = 'not';	
		}
		
		$new_shipping_address = $old_address[ $address_id ];
		if ( ! empty( $new_shipping_address ) ) {
			$old_address[ $address_id ]['active_shipping_address'] = 'yes';			
			update_user_meta( $user_id, 'la_fleur_list_addresses', $old_address );		
			
			$shipping_address_1 = $new_shipping_address['street'] .' '. $new_shipping_address['house'] .' '.  $new_shipping_address['flat'];
			update_user_meta( $user_id, 'shipping_address_1', $shipping_address_1 );
			
			update_user_meta( $user_id, 'city', $new_shipping_address['city'] );
			update_user_meta( $user_id, 'la_fleur_shipping_street', $new_shipping_address['street'] );
			update_user_meta( $user_id, 'la_fleur_shipping_house', $new_shipping_address['house'] );
			update_user_meta( $user_id, 'la_fleur_shipping_flat', $new_shipping_address['flat'] );
			
			$return['text'] = __( 'Платёжный адрес изменён.', 'la-fleur' );
			echo json_encode( $return );
			wp_die();
		}
			
		$return['text'] = __( 'Неизвестная ошыбка.', 'la-fleur' );	
		echo json_encode( $return );
		wp_die();
	}
	
}

// My account - edit shipping address
add_action( 'wp_ajax_my_account_edit_address', 'callback_my_account_edit_address' );
add_action( 'wp_ajax_nopriv_my_account_edit_address', 'callback_my_account_edit_address' );

function callback_my_account_edit_address() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' );
	
	$return = array();
	if ( empty( $_POST['address_id'] ) ) {
		$return['text'] = __( 'Неизвестная ошыбка.', 'la-fleur' );
		echo json_encode( $return );
		wp_die();
	} else {
		$address_id = esc_html( $_POST['address_id'] );
		$user_id = get_current_user_id();

		if ( $user_id <= 0 ) {
			return;
		}
		
		$old_address = get_user_meta( $user_id, 'la_fleur_list_addresses', true );
		$return_address = $old_address[ $address_id ];
		$return_address['address_id'] = $address_id;
		echo json_encode( $return_address );
		wp_die();
	}
	
	$return['text'] = __( 'Неизвестная ошыбка.', 'la-fleur' );
	echo json_encode( $return );
	wp_die();
}

// order, disable required_fields
add_filter( 'woocommerce_default_address_fields', 'la_fleur_order_required_fields' );

function la_fleur_order_required_fields( $fields ) {
	unset( $fields[ 'state' ] );
	unset( $fields[ 'country' ] );
	unset( $fields[ 'postcode' ] );
	unset( $fields[ 'company' ] );
	
	return $fields;
}

add_filter( 'woocommerce_billing_fields', 'la_fleur_order_billing_fields' );
 
function la_fleur_order_billing_fields( $fields ) {
	unset( $fields[ 'billing_address_1' ] );
	unset( $fields[ 'billing_address_2' ] );
	unset( $fields[ 'billing_city' ] );
	
	return $fields;
}

add_filter( 'woocommerce_shipping_fields', 'la_fleur_order_shipping_fields' );
 
function la_fleur_order_shipping_fields( $fields ) {
	unset( $fields[ 'shipping_first_name' ] );
	unset( $fields[ 'shipping_last_name' ] );
	
	return $fields;
}
