<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

//do_action( 'woocommerce_before_cart' ); ?>

<div class="section-oreder">
	<div class="section-oreder_wr d-f-row-stre-s_b">

		<!-- left -->
		<div class="section-oreder_left">
			<div class="section-oreder_left_wr">

				<div class="order-mobile-title is_mobile">
					<h5><?php _e( 'ваша корзина', 'la-fleur' ); ?></h5>
				</div>

				<div class="line-in-order-step"></div>

				<!-- basket -->
				<div id="la_fleur_cart">
					<?php echo la_fleur_cart(); ?>
				</div>

			</div>
		</div>

		<?php
		$curr_user_id = get_current_user_id();
		$user = get_user_by( 'id', get_current_user_id() );	
		
		$phone = get_user_meta( $curr_user_id, 'phone', true );
		$city = get_user_meta( $curr_user_id, 'city', true );
		
		$first_name  = ! empty( $user->first_name  ) ? $user->first_name  : '';
		$last_name  = ! empty( $user->last_name  ) ? $user->last_name  : '';
		$user_email  = ! empty( $user->user_email  ) ? $user->user_email  : '';
		?>

		<!-- right -->
		<div class="section-oreder_right">
			<div class="section-oreder_right_wr">

				<div class="section-oreder_title is_descktop">
					<h3><?php _e( 'Оформление заказа', 'la-fleur' ); ?></h3>
				</div>

				<div class="section-oreder_our-data">
					<div class="section-oreder_our-data_wr d-f-column-f_s-f_s">
						<!-- <form action="" class="d-f-column-f_s-f_s"> -->
						<div class="section-oreder_our-data_title">
							<h5><?php _e( 'Контактные данные', 'la-fleur' ); ?></h5>
						</div>

						<form id="order-step-1" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" method="post">
							<div class="section-oreder_our-data_inputs d-f-row-stre-s_b">

								<!-- name  -->
								<div class="section-all-input">
									<label for="name-order-st-1"><?php esc_html_e( 'Имя', 'la-fleur' ); ?></label>
									<input id="name-order-st-1" name="name-order-st-1" type="text"
										placeholder="<?php esc_html_e( 'Имя', 'la-fleur' ); ?>" value="<?php echo esc_attr( $first_name ); ?>" autocomplete="given-name">
									<div class="input-mail-error__message">
										<p><?php esc_html_e( 'Введите корректное имя.', 'la-fleur' ); ?></p>
									</div>
								</div>
								
								<div class="section-all-input">
									<label for="surname-order-st-1"><?php esc_html_e( 'Фамилия', 'la-fleur' ); ?></label>
									<input id="surname-order-st-1" name="account_last_name" type="text"
										placeholder="<?php esc_html_e( 'Фамилия', 'la-fleur' ); ?>" value="<?php echo esc_attr( $last_name ); ?>"  autocomplete="family-name">
									<div class="input-mail-error__message">
										<p><?php esc_html_e( 'Введите корректную фамилию.', 'la-fleur' ); ?></p>
									</div>
								</div>

								<!-- phone  -->
								<div class="section-all-input">
									<label for="phone-order-st-1">Телефон</label>
									<input id="phone-order-st-1" class="mask-phone" name="phone-order-st-1"  type="tel"
										placeholder="+3__ ___ __ ___" value="<?php echo esc_html( $phone ); ?>" autocomplete="phone">
									<div class="input-mail-error__message">
										<p><?php esc_html_e( 'Введите корректное номер телефона.', 'la-fleur' ); ?></p>
									</div>
								</div>
								
								<!-- mail -->
								<div class="section-all-input">
									<label for="mail-order-st-1">Email</label>
									<input id="mail-order-st-1" name="mail-order-st-1" type="email"
										placeholder="<?php esc_html_e( 'Email', 'la-fleur' ); ?>" value="<?php echo esc_attr( $user_email ); ?>" autocomplete="email">
									<div class="input-mail-error__message">
										<p><?php esc_html_e( 'Введите корректный адрес электронной почты.', 'la-fleur' ); ?></p>
									</div>
								</div>

								<!-- btn -->
								<div class="all-link-order">
									<div class="all_link hover_effect-for">
										<button type="submit" id="link-to-order-step-2">
											<p><?php _e( 'Перейти к выбору доставки', 'la-fleur' ); ?></p>
										</button>
									</div>
								</div>
								<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>

							</div>
						</form>

						<!-- </form> -->
					</div>
				</div>
				
			</div>
		</div>
		

	</div>
</div>

<?php //do_action( 'woocommerce_before_cart_collaterals' ); ?>



<?php //do_action( 'woocommerce_after_cart' ); ?>
