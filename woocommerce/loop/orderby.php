<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! IS_MOBIL ) {
	?>
	<div class="section-main-shop-sort is_descktop d-f-row-c-f_end">
		<p><?php _e( 'Сортировать по:', 'la-fleur' ); ?></p>

		<div class="line-for-shop-sort"></div>

		<div class="sectio-small-select container_for_small-select select-for-sort">
			<form class="woocommerce-ordering" method="get">
				<select class="orderby small-select select2-hidden-accessible"
					name="orderby" id="select-shop-sort" tabindex="-1" aria-hidden="true">
					
					<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
						<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
					<?php endforeach; ?>
				</select>
				<input type="hidden" name="paged" value="1" />
				<?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
			</form>
		</div>

	</div>
	<?php
}
