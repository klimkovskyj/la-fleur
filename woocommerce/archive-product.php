<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

/**
 * Hook: woocommerce_archive_description.
 *
 * @hooked woocommerce_taxonomy_archive_description - 10
 * @hooked woocommerce_product_archive_description - 10
 */
do_action( 'woocommerce_archive_description' );

?>

        <!-- <section pagination start -->
        <?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
		<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
			<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
				<div class="section-pagination__wrapper all-width">
					<ul class="breadcrumb">
						<?php woocommerce_breadcrumb(); ?>
					</ul>
				</div>
			</div>
		<?php } ?>
        <!-- <section pagination end -->

        <div class="section-shop">
            <div class="section-shop__wrapper all-width d-f-row-stre-s_b">
			<?php
			
			include 'widget-section.php';
						
			if ( woocommerce_product_loop() ) {
				?>
				<div class="section-shop__product d-f-column-f_s-f_s">
					<div class="section-shop__product__wrapper d-f-column-f_s-f_s">
					<?php		
					/**
					 * Hook: woocommerce_before_shop_loop.
					 *
					 * @hooked woocommerce_output_all_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );
					?>

						<?php if ( ! IS_MOBIL ) { ?>
							<!-- choosed filter -->
							<?php echo la_fleur_shop_active_filters(); ?>
						<?php } ?>	

						<?php if ( IS_MOBIL ) { ?>
							<div class="section-mobile-choosed-filter is_mobile d-f-row-stre-f_s">
								<button id="show-mobile-filter-shop">
									<p><?php _e( 'Фильтры', 'la-fleur' ); ?>
								
										<span><?php echo la_fleur_shop_active_filters(); ?></span>
									</p>
									<svg width="10" height="10" viewBox="0 0 10 10" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<line y1="-0.5" x2="11.9493" y2="-0.5"
											transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 8.65625 0)"
											stroke="black" />
										<line x1="0.5" y1="1.35254" x2="0.5" y2="8.38822" stroke="black" />
										<line x1="7.20508" y1="8.88867" x2="0.000236466" y2="8.88867" stroke="black" />
									</svg>
								</button>

								<?php la_fleur_woocommerce_catalog_ordering_mobil(); ?>
								
							</div>
						<?php } ?>
							
						<?php						
						woocommerce_product_loop_start();
				
						if ( wc_get_loop_prop( 'total' ) ) {
							while ( have_posts() ) {
								the_post();

								/**
								 * Hook: woocommerce_shop_loop.
								 */
								do_action( 'woocommerce_shop_loop' );

								echo '<div class="shop-product-item">';
									wc_get_template_part( 'content', 'product' );
								echo '</div>';
							}
						}

						woocommerce_product_loop_end();
						
						/**
						 * Hook: woocommerce_after_shop_loop.
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
						?>

						<?php if ( isset( $wp_query->max_num_pages ) && $wp_query->max_num_pages > 1 ) : ?>
							<script>
							var true_posts = '<?php echo serialize($wp_query->query_vars); ?>';
							var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
							var max_pages = '<?php echo $wp_query->max_num_pages; ?>';
							</script>
							<div class="btn-show-more-product d-f-row-c-c">
								<a id="products_loadmore" href="#">
									<p><?php _e( 'смотреть все', 'la-fleur' ); ?></p>
								</a>
							</div>
						<?php endif; ?>
						
						
					</div>
				</div>
				<?php
				
			} else {
				?>
				<div class="section-shop__product d-f-column-f_s-f_s">
					<div class="section-shop__product__wrapper d-f-column-f_s-f_s">
						<?php
						/**
						 * Hook: woocommerce_no_products_found.
						 *
						 * @hooked wc_no_products_found - 10
						 */
						do_action( 'woocommerce_no_products_found' );
						?>
					</div>
				</div>
				<?php
			}
			?>
			</div>
		</div>
		
<?php
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

get_footer( 'shop' );
