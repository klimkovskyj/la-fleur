<?php

function la_fleur_woocommerce_setup() {
	add_theme_support(
		'woocommerce',
		array(
			'thumbnail_image_width' => 150,
			'single_image_width'    => 300,
			'product_grid'          => array(
				'default_rows'    => 3,
				'min_rows'        => 1,
				'default_columns' => 4,
				'min_columns'     => 1,
				'max_columns'     => 6,
			),
		)
	);
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'la_fleur_woocommerce_setup' );

/**
 * WooCommerce specific scripts & stylesheets.
 *
 * @return void
 */
function la_fleur_woocommerce_scripts() {
	wp_enqueue_style( 'la-fleur-woocommerce-style', get_template_directory_uri() . '/woocommerce.css', array(), _S_VERSION );

	$font_path   = WC()->plugin_url() . '/assets/fonts/';
	$inline_font = '@font-face {
			font-family: "star";
			src: url("' . $font_path . 'star.eot");
			src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
				url("' . $font_path . 'star.woff") format("woff"),
				url("' . $font_path . 'star.ttf") format("truetype"),
				url("' . $font_path . 'star.svg#star") format("svg");
			font-weight: normal;
			font-style: normal;
		}';

	wp_add_inline_style( 'la-fleur-woocommerce-style', $inline_font );
}
add_action( 'wp_enqueue_scripts', 'la_fleur_woocommerce_scripts' );

/**
 * Disable the default WooCommerce stylesheet.
 *
 * Removing the default WooCommerce stylesheet and enqueing your own will
 * protect you during WooCommerce core updates.
 *
 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param  array $classes CSS classes applied to the body tag.
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function la_fleur_woocommerce_active_body_class( $classes ) {
	$classes[] = 'woocommerce-active';

	return $classes;
}
add_filter( 'body_class', 'la_fleur_woocommerce_active_body_class' );

/**
 * Related Products Args.
 *
 * @param array $args related products args.
 * @return array $args related products args.
 */
function la_fleur_woocommerce_related_products_args( $args ) {
	$defaults = array(
		'posts_per_page' => 3,
		'columns'        => 3,
	);

	$args = wp_parse_args( $defaults, $args );

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'la_fleur_woocommerce_related_products_args' );

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

if ( ! function_exists( 'la_fleur_woocommerce_wrapper_before' ) ) {
	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	function la_fleur_woocommerce_wrapper_before() {
		?>
			<!-- main content start -->
			<div class="main-content d-f-column-f_c-f_s">
			<!-- this div in the header -->
		<?php
	}
}
add_action( 'woocommerce_before_main_content', 'la_fleur_woocommerce_wrapper_before' );

if ( ! function_exists( 'la_fleur_woocommerce_wrapper_after' ) ) {
	/**
	 * After Content.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	function la_fleur_woocommerce_wrapper_after() {
		?>
			</div>
		<?php
	}
}
add_action( 'woocommerce_after_main_content', 'la_fleur_woocommerce_wrapper_after' );

/**
 * Sample implementation of the WooCommerce Mini Cart.
 *
 * You can add the WooCommerce Mini Cart to header.php like so ...
 *
	<?php
		if ( function_exists( 'la_fleur_woocommerce_header_cart' ) ) {
			la_fleur_woocommerce_header_cart();
		}
	?>
 */

if ( ! function_exists( 'la_fleur_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	function la_fleur_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		la_fleur_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}
add_filter( 'woocommerce_add_to_cart_fragments', 'la_fleur_woocommerce_cart_link_fragment' );

if ( ! function_exists( 'la_fleur_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function la_fleur_woocommerce_cart_link() {
		?>
		<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'la-fleur' ); ?>">
			<?php
			$item_count_text = sprintf(
				/* translators: number of items in the mini cart. */
				_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'la-fleur' ),
				WC()->cart->get_cart_contents_count()
			);
			?>
			<span class="amount"><?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?></span> <span class="count"><?php echo esc_html( $item_count_text ); ?></span>
		</a>
		<?php
	}
}

if ( ! function_exists( 'la_fleur_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function la_fleur_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php la_fleur_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
				$instance = array(
					'title' => '',
				);

				the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}

// CUSTOM

/**
 * Breadcrumbs.
 *
 * @see woocommerce_breadcrumb()
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/**
 * For mobil
 * Output the product sorting options.
 */
function la_fleur_woocommerce_catalog_ordering_mobil() {
	if ( ! wc_get_loop_prop( 'is_paginated' ) || ! woocommerce_products_will_display() ) {
		return;
	}
	$show_default_orderby    = 'menu_order' === apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby', 'menu_order' ) );
	$catalog_orderby_options = apply_filters(
		'woocommerce_catalog_orderby',
		array(
			'menu_order' => __( 'Default sorting', 'woocommerce' ),
			'popularity' => __( 'Sort by popularity', 'woocommerce' ),
			'rating'     => __( 'Sort by average rating', 'woocommerce' ),
			'date'       => __( 'Sort by latest', 'woocommerce' ),
			'price'      => __( 'Sort by price: low to high', 'woocommerce' ),
			'price-desc' => __( 'Sort by price: high to low', 'woocommerce' ),
		)
	);

	$default_orderby = wc_get_loop_prop( 'is_search' ) ? 'relevance' : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby', '' ) );
	// phpcs:disable WordPress.Security.NonceVerification.Recommended
	$orderby = isset( $_GET['orderby'] ) ? wc_clean( wp_unslash( $_GET['orderby'] ) ) : $default_orderby;
	// phpcs:enable WordPress.Security.NonceVerification.Recommended

	if ( wc_get_loop_prop( 'is_search' ) ) {
		$catalog_orderby_options = array_merge( array( 'relevance' => __( 'Relevance', 'woocommerce' ) ), $catalog_orderby_options );

		unset( $catalog_orderby_options['menu_order'] );
	}

	if ( ! $show_default_orderby ) {
		unset( $catalog_orderby_options['menu_order'] );
	}

	if ( ! wc_review_ratings_enabled() ) {
		unset( $catalog_orderby_options['rating'] );
	}

	if ( ! array_key_exists( $orderby, $catalog_orderby_options ) ) {
		$orderby = current( array_keys( $catalog_orderby_options ) );
	}

	?>
	
	<form class="woocommerce-ordering" method="get">
		<div class="sectio-small-select container_for_small-select select-for-sort select-for-sort__mobile">
			<select name="orderby" class="orderby small-select select2-hidden-accessible"
				name="select-lang-descktop-header" id="select-shop-sort" tabindex="-1" aria-hidden="true">
				<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
					<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
				<?php endforeach; ?>
			</select>
			<input type="hidden" name="paged" value="1" />
			<?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
		</div>
	</form>
	
	<?php
}

// Sorting, custom ordering by alphabetical
function la_flour_alphabetical_shop_ordering( $sort_args ) {
	$orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
	if ( 'alphabetical' == $orderby_value ) {
		$sort_args['orderby'] = 'title';
		$sort_args['order'] = 'ASC';
		$sort_args['meta_key'] = '';
	}
	return $sort_args;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'la_flour_alphabetical_shop_ordering' );

function la_flour_wc_catalog_orderby( $sortby ) {
	//unset( $sortby['menu_order'] );
	unset( $sortby['date'] );
	unset( $sortby['rating'] );
	unset( $sortby['price-desc'] );
	
	$sortby['menu_order'] = __( 'по умолчанию', 'la-fleur' );
	$sortby['popularity'] = __( 'по популярности', 'la-fleur' );
	$sortby['price'] = __( 'по цене', 'la-fleur' );
	$sortby['alphabetical'] = __( 'по алфавиту', 'la-fleur' );
	
	return $sortby;
}
//add_filter( 'woocommerce_default_catalog_orderby_options', 'custom_wc_catalog_orderby' );
add_filter( 'woocommerce_catalog_orderby', 'la_flour_wc_catalog_orderby' );

/**
 * Output the start of a product loop. By default this is a UL.
 *
 * @param bool $echo Should echo?.
 * @return string
 */
function woocommerce_product_loop_start( $echo = true ) {
	ob_start();

	//wc_set_loop_prop( 'loop', 0 );
	//wc_get_template( 'loop/loop-start.php' );
	?>
	<div class="sectio-all-product">
	<?php

	$loop_start = apply_filters( 'woocommerce_product_loop_start', ob_get_clean() );

	if ( $echo ) {
		// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		echo $loop_start;
	} else {
		return $loop_start;
	}
}

/**
 * Output the end of a product loop. By default this is a UL.
 *
 * @param bool $echo Should echo?.
 * @return string
 */
function woocommerce_product_loop_end( $echo = true ) {
	ob_start();
	
	//wc_get_template( 'loop/loop-end.php' );
	?>
	</div>
	<?php

	$loop_end = apply_filters( 'woocommerce_product_loop_end', ob_get_clean() );

	if ( $echo ) {
		// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		echo $loop_end;
	} else {
		return $loop_end;
	}
}
	
/**
 * Output the pagination.
 */
function woocommerce_pagination() {
	if ( ! wc_get_loop_prop( 'is_paginated' ) || ! woocommerce_products_will_display() ) {
		return;
	}

	$args = array(
		'total'   => wc_get_loop_prop( 'total_pages' ),
		'current' => wc_get_loop_prop( 'current_page' ),
		'base'    => esc_url_raw( add_query_arg( 'product-page', '%#%', false ) ),
		'format'  => '?product-page=%#%',
	);

	if ( ! wc_get_loop_prop( 'is_shortcode' ) ) {
		$args['format'] = '';
		$args['base']   = esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) );
	}

	//wc_get_template( 'loop/pagination.php', $args );
	
	$total   = isset( $total ) ? $total : wc_get_loop_prop( 'total_pages' );
	$current = isset( $current ) ? $current : wc_get_loop_prop( 'current_page' );
	$base    = isset( $base ) ? $base : esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) );
	$format  = isset( $format ) ? $format : '';

	if ( $total <= 1 ) {
		return;
	}
	?>
	<div class="pagination-shop d-f-row-c-f_end">
		<?php
		echo paginate_links(
			apply_filters(
				'woocommerce_pagination_args',
				array( // WPCS: XSS ok.
					'base'      => $base,
					'format'    => $format,
					'add_args'  => false,
					'current'   => max( 1, $current ),
					'total'     => $total,
					'prev_text' => '&laquo;',//'&larr;',
					'next_text' => '&raquo;',//'&rarr;',
					'type'      => 'plain',//'list',
					'end_size'  => 3,
					'mid_size'  => 3,
				)
			)
		);
		?>
	</div>
	<?php
}

// params for widget, shop
function la_fleur_search_params( $index ) {
	$param = $_SERVER['QUERY_STRING'];
	$res   = '';
	if ( isset( $param ) ) {
		parse_str( $_SERVER['QUERY_STRING'], $vars );
		foreach ( $vars as $key => $val ) {
			
			if ( $key == $index ) continue;
							
			$res .= '<input type="hidden" name="'.esc_attr( $key ). '" value="'. esc_attr( $val ) .'" />';
		}
	}

	return $res;	
}

// Widget price
function la_fleur_widget_price_arg( $min, $max, $text ) {
	if ( isset( $_GET['min_price'] ) && $_GET['min_price'] == $min && isset( $_GET['max_price'] ) && $_GET['max_price'] == $max ) {
		$page_url = remove_query_arg( 
			array( 'min_price', 'max_price' )
		);		
		echo '<input type="checkbox" checked>';
		echo '<div class="block-for-input-filter"></div>';	
		echo '<p><a href="'. esc_url( $page_url ) .'">'. esc_html( $text ) .'</a></p>';		
	} else {
		$arr_params = array( 'min_price' => $min, 'max_price' => $max );
		echo '<input type="checkbox">';
		echo '<div class="block-for-input-filter"></div>';	
		echo '<p><a href="'. esc_url( add_query_arg( $arr_params ) ) .'">'. esc_html( $text ) .'</a></p>';	
	}	
}

// Shop active filters
function la_fleur_shop_active_filters() {
	if ( ! is_shop() && ! is_product_taxonomy() ) {
			return;
		}

		$_chosen_attributes = WC_Query::get_layered_nav_chosen_attributes();
		$min_price          = isset( $_GET['min_price'] ) ? wc_clean( wp_unslash( $_GET['min_price'] ) ) : 0; // WPCS: input var ok, CSRF ok.
		$max_price          = isset( $_GET['max_price'] ) ? wc_clean( wp_unslash( $_GET['max_price'] ) ) : 0; // WPCS: input var ok, CSRF ok.
		$rating_filter      = isset( $_GET['rating_filter'] ) ? array_filter( array_map( 'absint', explode( ',', wp_unslash( $_GET['rating_filter'] ) ) ) ) : array(); // WPCS: sanitization ok, input var ok, CSRF ok.
		$base_link          = home_url( $_SERVER['REQUEST_URI'] );

		if ( 0 < count( $_chosen_attributes ) || 0 < $min_price || 0 < $max_price || ! empty( $_GET['product_cat'] ) || ! empty( $rating_filter ) ) {
			echo '<div class="section-shop-choose-filter is_descktop  d-f-row-c-f_s">';
			echo '<p>'. __( 'Выбранные фильтры:', 'la-fleur' ) .'</p>';
			echo '<div class="line-shop-chosed-filter"></div>';
			
			// Attributes.
			if ( ! empty( $_chosen_attributes ) ) {
				foreach ( $_chosen_attributes as $taxonomy => $data ) {
					foreach ( $data['terms'] as $term_slug ) {
						$term = get_term_by( 'slug', $term_slug, $taxonomy );
						if ( ! $term ) {
							continue;
						}

						$filter_name    = 'filter_' . wc_attribute_taxonomy_slug( $taxonomy );
						$current_filter = isset( $_GET[ $filter_name ] ) ? explode( ',', wc_clean( wp_unslash( $_GET[ $filter_name ] ) ) ) : array(); // WPCS: input var ok, CSRF ok.
						$current_filter = array_map( 'sanitize_title', $current_filter );
						$new_filter     = array_diff( $current_filter, array( $term_slug ) );

						$link = remove_query_arg( array( 'add-to-cart', $filter_name ), $base_link );

						if ( count( $new_filter ) > 0 ) {
							$link = add_query_arg( $filter_name, implode( ',', $new_filter ), $link );
						}

						$filter_classes = array( 'chosen', 'chosen-' . sanitize_html_class( str_replace( 'pa_', '', $taxonomy ) ), 'chosen-' . sanitize_html_class( str_replace( 'pa_', '', $taxonomy ) . '-' . $term_slug ) );

						echo '<div class="' . esc_attr( implode( ' ', $filter_classes ) ) . ' this-shop-choose-filter"><a rel="nofollow" aria-label="' . esc_attr__( 'Remove filter', 'woocommerce' ) . '" href="' . esc_url( $link ) . '">' . esc_html( $term->name ) . '</a></div>';
					}
				}
			}

			if ( ! empty( $_GET['min_price'] ) && ! empty( $_GET['max_price'] ) ) {
				$link = remove_query_arg( array( 'min_price', 'max_price' ), $base_link );
				/* translators: %s: minimum price */
				echo '<div class="chosen this-shop-choose-filter"><a rel="nofollow" aria-label="' . esc_attr__( 'Remove filter', 'woocommerce' ) . '" href="' . esc_url( $link ) . '">' . sprintf( __( 'Цена %1$s - %2$s ', 'la-fleur' ), wc_price( $min_price ), wc_price( $max_price ) ) . '</a></div>'; // WPCS: XSS ok.
			} elseif ( ! empty( $min_price ) ) {
				$link = remove_query_arg( 'min_price', $base_link );
				/* translators: %s: minimum price */
				echo '<div class="chosen this-shop-choose-filter"><a rel="nofollow" aria-label="' . esc_attr__( 'Remove filter', 'woocommerce' ) . '" href="' . esc_url( $link ) . '">' . sprintf( __( 'Min %s', 'woocommerce' ), wc_price( $min_price ) ) . '</a></div>'; // WPCS: XSS ok.
			} elseif ( ! empty( $max_price ) ) {
				$link = remove_query_arg( 'max_price', $base_link );
				/* translators: %s: maximum price */
				echo '<div class="chosen this-shop-choose-filter"><a rel="nofollow" aria-label="' . esc_attr__( 'Remove filter', 'woocommerce' ) . '" href="' . esc_url( $link ) . '">' . sprintf( __( 'Max %s', 'woocommerce' ), wc_price( $max_price ) ) . '</a></div>'; // WPCS: XSS ok.
			}
			
			if ( ! empty( $_GET['product_cat'] ) ) {
				$product_cat = esc_html( $_GET['product_cat'] );
				$link = remove_query_arg( 'product_cat', $base_link );
				//$cat_link = get_term_link( $product_cat, 'product_cat' );
				$term = get_term_by('slug', $product_cat, 'product_cat');
				echo '<div class="chosen this-shop-choose-filter"><a rel="nofollow" aria-label="' . esc_attr__( 'Remove filter', 'woocommerce' ) . '" href="' . esc_url( $link ) . '">' . $term->name . '</a></div>'; // WPCS: XSS ok.
			}

			if ( ! empty( $rating_filter ) ) {
				foreach ( $rating_filter as $rating ) {
					$link_ratings = implode( ',', array_diff( $rating_filter, array( $rating ) ) );
					$link         = $link_ratings ? add_query_arg( 'rating_filter', $link_ratings ) : remove_query_arg( 'rating_filter', $base_link );

					/* translators: %s: rating */
					echo '<div class="chosen this-shop-choose-filter"><a rel="nofollow" aria-label="' . esc_attr__( 'Remove filter', 'woocommerce' ) . '" href="' . esc_url( $link ) . '">' . sprintf( esc_html__( 'Rated %s out of 5', 'woocommerce' ), esc_html( $rating ) ) . '</a></div>';
				}
			}
			global $wp;
			$current_url = home_url( add_query_arg( array(), $wp->request ) );
			echo '<a href="'. esc_url( $current_url ) .'" class="clean-all-shop-filter">'. __( 'очистить все', 'la-fleur' ) .'</a>'; 
			echo '</div>';
		} else {
			//_e( 'Нет выбраных фильтров', 'la-fleur' );
		}
}

function wc_dropdown_variation_attribute_options( $args = array() ) {
	$args = wp_parse_args(
		apply_filters( 'woocommerce_dropdown_variation_attribute_options_args', $args ),
		array(
			'options'          => false,
			'attribute'        => false,
			'product'          => false,
			'selected'         => false,
			'name'             => '',
			'id'               => '',
			'class'            => '',
			'show_option_none' => __( 'Choose an option', 'woocommerce' ),
		)
	);

	// Get selected value.
	if ( false === $args['selected'] && $args['attribute'] && $args['product'] instanceof WC_Product ) {
		$selected_key = 'attribute_' . sanitize_title( $args['attribute'] );
		// phpcs:disable WordPress.Security.NonceVerification.Recommended
		$args['selected'] = isset( $_REQUEST[ $selected_key ] ) ? wc_clean( wp_unslash( $_REQUEST[ $selected_key ] ) ) : $args['product']->get_variation_default_attribute( $args['attribute'] );
		// phpcs:enable WordPress.Security.NonceVerification.Recommended
	}

	$options               = $args['options'];
	$product               = $args['product'];
	$attribute             = $args['attribute'];
	$name                  = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );
	$id                    = $args['id'] ? $args['id'] : sanitize_title( $attribute );
	$class                 = $args['class'];
	$show_option_none      = (bool) $args['show_option_none'];
	$show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' ); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

	if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
		$attributes = $product->get_variation_attributes();
		$options    = $attributes[ $attribute ];
	}

	$html  = '<select id="select-choose-volume" class="' . esc_attr( $class ) . ' small-select" name="' . esc_attr( $name ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute ) ) . '" data-show_option_none="' . ( $show_option_none ? 'yes' : 'no' ) . '">';
	//$html .= '<option value="">' . esc_html( $show_option_none_text ) . '</option>';

	if ( ! empty( $options ) ) {
		if ( $product && taxonomy_exists( $attribute ) ) {
			// Get terms if this is a taxonomy - ordered. We need the names too.
			$terms = wc_get_product_terms(
				$product->get_id(),
				$attribute,
				array(
					'fields' => 'all',
				)
			);

			foreach ( $terms as $term ) {
				if ( in_array( $term->slug, $options, true ) ) {
					$html .= '<option value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $args['selected'] ), $term->slug, false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name, $term, $attribute, $product ) ) . '</option>';
				}
			}
		} else {
			foreach ( $options as $option ) {
				// This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
				$selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false );
				$html    .= '<option value="' . esc_attr( $option ) . '" ' . $selected . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option, null, $attribute, $product ) ) . '</option>';
			}
		}
	}

	$html .= '</select>';

	// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	echo apply_filters( 'woocommerce_dropdown_variation_attribute_options_html', $html, $args );
}

function woocommerce_breadcrumb( $args = array() ) {
	$args = wp_parse_args(
		$args,
		apply_filters(
			'woocommerce_breadcrumb_defaults',
			array(
				'delimiter'   => '',//'&nbsp;&#47;&nbsp;',
				'wrap_before' => '<ul class="breadcrumb">',//'<nav class="woocommerce-breadcrumb">',
				'wrap_after'  => '</ul>',//'</nav>',
				'before'      => '<li>',
				'after'       => '</li>',
				'home'        => _x( 'LaFleur', 'breadcrumb', 'la-fleur' ),
			)
		)
	);

	$breadcrumbs = new WC_Breadcrumb();

	if ( ! empty( $args['home'] ) ) {
		$breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
	}

	$args['breadcrumb'] = $breadcrumbs->generate();

	/**
	 * WooCommerce Breadcrumb hook
	 *
	 * @hooked WC_Structured_Data::generate_breadcrumblist_data() - 10
	 */
	do_action( 'woocommerce_breadcrumb', $breadcrumbs, $args );

	wc_get_template( 'global/breadcrumb.php', $args );
}

// Custom get price
function la_fleur_get_price( $product ) {
	$regular_price = $product->get_regular_price();
	$sale_price = $product->get_sale_price();
	if ( $regular_price || $sale_price ) {
		if ( $regular_price ) {
			echo '<h3>'. $regular_price . get_woocommerce_currency_symbol() .'</h3>';
		}
		if ( $sale_price ) {
			echo '<h3 class="price_sale">'. $sale_price . get_woocommerce_currency_symbol() .'</h3>';
		}
	} else {
		echo '<h3>'. $product->get_price_html() .'</h3>';
	}
}

// custom_variation_price
add_filter('woocommerce_variable_price_html', 'custom_variation_price', 10, 2); 

function custom_variation_price( $price, $product ) { 

     $price = '';

     $price .= wc_price($product->get_price()); 

     return $price;
}

// add custom shipping opion
add_filter( 'woocommerce_checkout_create_order', 'mbm_alter_shipping', 10, 1 );

function mbm_alter_shipping ($order) {
	$city = '';
	$address_1 = '';
	if ( isset( $_POST['ship'] ) && $_POST['ship'] == 'local_pickup:2' ) { $address_1 = __( 'Самовывоз. ', 'la-fleur' ); }
	if ( isset( $_POST['ship'] ) && $_POST['ship'] == 'flat_rate:4' ) { $city = __( 'Город ', 'la-fleur' ) . esc_html( $_POST['shipping_city'] ) .'.'; }
	if ( isset( $_POST['ship'] ) && $_POST['ship'] == 'flat_rate:4' ) {
		$address_1 = __( 'Улица ', 'la-fleur' ) . esc_html( $_POST['street-order-st-2'] ) . __( '. Дом ', 'la-fleur' ) . esc_html( $_POST['number-house-order-st-2'] ) . __( '. Квартира', 'la-fleur' ) . esc_html( $_POST['number-flat-order-st-2'] ) .'.';
	}
	if ( isset( $_POST['ship'] ) && $_POST['ship'] == 'flat_rate:5' ) { $address_1 = __( 'Отделение новой почты. ', 'la-fleur' ) . esc_html( $_POST['order-branch'] ) .'.'; }
	
  if ($something == $condition) {
    $address = array(
      'city'       => $city,
      'address_1'  => $address_1,
      //'address_2'  => $address_2,
    );

    $order->set_address( $address, 'shipping' );

  }

  return $order;

}

remove_action( 'woocommerce_before_checkout_form_cart_notices', 'woocommerce_output_all_notices', 10 );

// this is not used
//function la_fleur_get_cart_shipping_total() {
	//// Default total assumes Free shipping.
	//$total = __( 'Free!', 'woocommerce' );

	//if ( 0 < WC()->cart->get_shipping_total() ) {

		//if ( WC()->cart->display_prices_including_tax() ) {
			//$total = WC()->cart->shipping_total + WC()->cart->shipping_tax_total;

			//if ( WC()->cart->shipping_tax_total > 0 && ! wc_prices_include_tax() ) {
				//$total .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
			//}
		//} else {
			//$total = WC()->cart->shipping_total;

			//if ( WC()->cart->shipping_tax_total > 0 && wc_prices_include_tax() ) {
				//$total .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
			//}
		//}
	//}
	//return apply_filters( 'woocommerce_cart_shipping_total', $total, WC()->cart );
//}

//add_filter( 'wc_add_to_cart_message_html', '__return_null' );
