<?php
/**
 * All-orders
 * Custom endpoint
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_account_orders', $has_orders );

$page_my_account = get_field( 'page_my_account' );
?>



<!-- mobile btn 2 -->
<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo esc_url( home_url( '/my-account/' ) ); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['data_title'] ); ?></p>
			<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<line y1="-0.5" x2="20.7549" y2="-0.5"
					transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
					stroke="black" />
				<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
				<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
			</svg>
		</div>
	</a>
</button>
<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active-cabinet-mob-btn" data-cabinet-tab='2'>
	<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
		<p><?php esc_html_e( $page_my_account['orders_title'] ); ?></p>
		<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
			xmlns="http://www.w3.org/2000/svg">
			<line y1="-0.5" x2="20.7549" y2="-0.5"
				transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
				stroke="black" />
			<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
			<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
		</svg>
	</div>
</button>
	<!-- tab 2  -->
	<div class="cabinet-show-tab cabinet-show-tab-2 active_cabinet-show-tab" data-cabinet-tab='2'>
		<div class="cabinet-show-tab_wr d-f-column-f_s-f_s">

			<!-- all title -->
			<div class="cabinet_title is_descktop">
				<h2><?php esc_html_e( $page_my_account['orders_title'] ); ?></h2>
			</div>

			<!-- sort for -->
			<!-- is descktop -->
			<!-- is mobile -->
			<div
				class="section-main-shop-sort section-main-shop-sort_cabinet-order d-f-row-c-f_end">

				<p>
					<?php _e( 'Период:', 'la-fleur' ); ?>
				</p>

				<div class="line-for-shop-sort"></div>


				<div class="sectio-small-select container_for_small-select select-for-sort mobil-p-l">
					<?php la_fleur_months_dropdown(); ?>
				</div>
			</div>

			<div class="cabinet-all-order w_100">
								
				<?php
				if ( $has_orders ) {
				
					foreach ( $customer_orders->orders as $customer_order ) {
						$order      = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
						$item_count = $order->get_item_count() - $order->get_item_count_refunded();
						$order_data = $order->get_data()
						?>
						<!-- item 1 -->
						<div class="cabinet-all-order_itm w_100">
							<div class="cabinet-all-order_itm_btn d-f-row-stre-s_b w_100">
														
								<!-- left -->
								<div class="cabinet-all-order_itm_btn_l d-f-row-c-f_s">
									<h4><?php _e( 'Заказ № ', 'la-fleur' ); ?><?php echo $order->get_order_number(); ?></h4>
								</div>

								<!-- right -->
								<div class="cabinet-all-order_itm_btn_r d-f-row-c-f_end">
									<!-- if delivery is done use class .yr-order-done -->
									<!-- if delivery is cancel or error use class .yr-order-cancel -->
									<p class="<?php echo la_fleur_get_order_status( $order->get_status() ); ?>"><?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?></p>

									<svg width="16" height="16" viewBox="0 0 16 16" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<line y1="-0.5" x2="20.7549" y2="-0.5"
											transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 15.0332 0)"
											stroke="black" />
										<line x1="0.5" y1="2.34912" x2="0.500001" y2="14.5695"
											stroke="black" />
										<line x1="12.5146" y1="15.0693" x2="0.000465348" y2="15.0693"
											stroke="black" />
									</svg>
								</div>
							</div>

							<!-- show -->
							<div class="cabinet-all-order_itm_content d-f-column-f_s-f_s w_100">
								
								<!-- this product -->
								<div class="all-pay-your-order w_100">
									<?php
									//$order = wc_get_order($order->get_ID());

									// Iterating through each WC_Order_Item_Product objects
									foreach ( $order->get_items() as $item_key => $item ) {
										//global $product;
										//$item_name    = $item->get_name(); // Name of the product
										$item_id    = $item->get_ID();
										$product      = $item->get_product();
										$product_id      = $item->get_product_id();
										$product_link = esc_url( get_the_permalink( $product_id ) );//$product ? admin_url( 'post.php?post=' . $item->get_product_id() . '&action=edit' ) : '';
										$thumbnail    = $product ? apply_filters( 'woocommerce_admin_order_item_thumbnail', $product->get_image( 'thumbnail', array( 'title' => '' ), false ), $item_id, $item ) : '';
									
									?>
									<!-- item 1 -->
									<div class="order-you-pr d-f-row-stre-s_b w_100">
										<div class="order-you-pr__img">
											<a href="<?php echo esc_url( get_the_permalink( $product_id ) ); ?>">
												<?php echo wp_kses_post( $thumbnail ); ?>
											</a>
										</div>

										<div class="order-you-pr__info order-you-pr__info_cabinet">
											<div class="order-you-pr__info_top d-f-row-stre-s_b">
												<div
													class="order-you-pr__info_left order-you-pr__info_left-cabinet">
													<a href="<?php echo esc_url( get_the_permalink( $product_id ) ); ?>">

														<!-- name  -->
														<div class="order-you-pr__info_name">
															<h4><?php echo get_the_title( $product_id ); ?></h4>
														</div>

														<!-- brand -->
														<div
															class="order-you-pr__info_brand-volume cabinet_volume d-f-row-stre-f_s">
															<p>
																<?php
																$pa_brand = $product->get_attribute( 'pa_brand' );
																if ( ! empty( $pa_brand ) ) {
																	$pa_brand_arr = explode( ',', $pa_brand );
																	echo esc_html( $pa_brand_arr[0] );
																}
																?>
															</p>

															<div class="order-you-pr__info_line"></div>

															<p>
																<?php
																$pa_volume = $product->get_attribute( 'pa_volume' );
																if ( ! empty( $pa_volume ) ) {
																	$pa_volume_arr = explode( ',', $pa_volume );
																	echo esc_html( $pa_volume_arr[0] );
																}
																?>
															</p>
														</div>
													</a>
												</div>

												<!-- price -->
												<div
													class="order-you-pr__info_bottom_price cabinet-your-order-weight">
													<div><?php esc_html_e( $item->get_quantity() ); ?> <?php esc_html_e( 'шт.', 'la-fleur' ) ?></div>
													<p>
														<?php
														echo wc_price( $order->get_item_subtotal( $item, false, true ), array( 'currency' => $order->get_currency() ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
														?>
													</p>
												</div>
											</div>

											<div class="order-you-pr__info_bottom d-f-row-stre-s_b">

												<!-- articul -->
												<div class="show-articul">
													<p><?php esc_html_e( 'АРТ:', 'la-fleur' ) ?> <?php esc_html_e( $product->get_sku() ); ?></p>
												</div>

											</div>
										</div>
									</div>
									<?php
									} // end foreach items
									?>
								</div>	

								<!-- info about your order -->
								<div class="cabinet-info-order w_100 d-f-row-stre-s_b">
									<div class="cabinet-info-order_l d-f-column-f_s-f_s">
										<p><?php esc_html_e( 'Дата доставки:  ', 'la-fleur' ) ?><?php echo $order_data['date_created']->date('d.m.Y'); ?></p>
										<p><?php esc_html_e( 'Тел. ', 'la-fleur' ) ?><?php echo esc_html( $order_data['billing']['phone'] ); ?></p>
										<p>
											<?php if ( $order_data['shipping']['city'] ) { esc_html_e( $order_data['shipping']['city'] ); } ?> <?php if ( $order_data['shipping']['address_1'] ) { esc_html_e( $order_data['shipping']['address_1'] ); } ?>
											<?php if ( $order_data['shipping']['address_2'] ) { esc_html_e( $order_data['shipping']['address_2'] ); } ?>
										</p>
									</div>

									<div class="cabinet-info-order_r d-f-column-f_s-f_s">
										<div>
											<p><?php _e( 'Всего:', 'la-fleur' ); ?></p> <span><?php echo wc_price( $order->get_subtotal(), array( 'currency' => $order->get_currency() ) ); ?></span>
										</div>
										<div>
											<p><?php _e( 'Доставка:', 'la-fleur' ); ?></p> <span><?php echo wc_price( $order->get_shipping_total(), array( 'currency' => $order->get_currency() ) ); ?></span>
										</div>
										<div>
											<p><?php _e( 'Итого:', 'la-fleur' ); ?></p> <span><?php echo wc_price( $order->get_total(), array( 'currency' => $order->get_currency() ) ); ?></span>
										</div>
									</div>							
								</div>
								
							</div>
						</div>	
					<?php
					} // end foreach orders
					?>
					<?php if ( 1 < $customer_orders->max_num_pages ) : ?>
						<?php		
						$pagination_url = esc_url( home_url( '/my-account/all-orders/' ) );
						$m = la_fleur_parse_url_get_m();
						if ( ! empty( $m ) ) {
							$pagination_url = $pagination_url . 'm/' . $m . '/';
						}
						?>
						<div class="">
							<?php if ( 1 !== $current_page ) { ?>
								<?php $prev_paged = (int) $current_page - 1; ?>
								<a class="iv-button" href="<?php echo $pagination_url . 'paged/' . $prev_paged . '/'; ?>"><?php esc_html_e( 'Previous', 'woocommerce' ); ?></a>
							<?php } ?>

							<?php if ( intval( $customer_orders->max_num_pages ) !== $current_page ) : ?>
								<?php $next_paged = (int) $current_page + 1; ?>
								<a class="iv-button" href="<?php echo $pagination_url . 'paged/' . $next_paged . '/'; ?>"><?php esc_html_e( 'Next', 'woocommerce' ); ?></a>
							<?php endif; ?>
						</div>
					<?php endif;
					
				} else {
				?>					
					<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
						<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>"><?php esc_html_e( 'Browse products', 'woocommerce' ); ?></a>
						<?php esc_html_e( 'No order has been made yet.', 'woocommerce' ); ?>
					</div>
				<?php		
				}	
				?>	
			</div>
		</div>
	</div>
	
	<?php do_action( 'woocommerce_before_account_orders_pagination' ); ?>

<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo esc_url( home_url( '/my-account/edit-address/' ) ); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['address_title'] ); ?></p>
			<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<line y1="-0.5" x2="20.7549" y2="-0.5"
					transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
					stroke="black" />
				<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
				<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
			</svg>
		</div>
	</a>
</button>
<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo esc_url( home_url( '/my-account/change-password/' ) ); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['change_password_title'] ); ?></p>
			<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<line y1="-0.5" x2="20.7549" y2="-0.5"
					transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
					stroke="black" />
				<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
				<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
			</svg>
		</div>
	</a>
</button>
<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo wc_logout_url(); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['logout_title'] ); ?></p>
		</div>
	</a>
</button>

<?php do_action( 'woocommerce_after_account_orders', $has_orders ); ?>
