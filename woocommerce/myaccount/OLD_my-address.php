<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

defined( 'ABSPATH' ) || exit;

$user_id = get_current_user_id();

?>
	<!-- mobile btn 3 -->
	<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active-cabinet-mob-btn" data-cabinet-tab='3'>
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php _e( 'Адрес для доставки', 'la-fleur' ); ?></p>
			<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<line y1="-0.5" x2="20.7549" y2="-0.5"
					transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
					stroke="black" />
				<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
				<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
			</svg>
		</div>
	</button>
	<!-- tab 3  -->
	<div class="cabinet-show-tab cabinet-show-tab-2 active_cabinet-show-tab" data-cabinet-tab='3'>
		<div class="cabinet-show-tab_wr d-f-column-f_s-f_s">

			<!-- all title -->
			<div class="cabinet_title is_descktop">
				<h2><?php _e( 'Адрес для доставки', 'la-fleur' ); ?></h2>
			</div>

			<!-- this content -->
			<form id="personal-address" method="POST">
				<div class="cabinet-tab_content d-f-row-stre-s_b">
					<!-- item 1 -->
					<div class="tab_content-1">
						<p><?php _e( 'Персональная информация нужна для оформления заказа', 'la-fleur' ); ?></p>
					</div>

					<!-- item 2 -->
					<div class="tab_content-r">

						<div class="cabinet-small-title">
							<p><?php _e( 'Добавление нового адреса', 'la-fleur' ); ?></p>
						</div>

						<div class="tab_content-r_itm">

							<!-- city  -->
							<div class="section-all-input">
								<label for="city-personal-address"><?php _e( 'Город', 'la-fleur' ); ?></label>
								<input id="city-personal-address" name="city-personal-address"
									type="text" placeholder="<?php _e( 'Город', 'la-fleur' ); ?>" value="">
								<div class="input-mail-error__message">
									<p><?php _e( 'Введите корректный город.', 'la-fleur' ); ?></p>
								</div>
							</div>

							<!-- house  -->
							<div class="section-all-input">
								<label for="house-personal-address"><?php _e( 'Дом', 'la-fleur' ); ?></label>
								<input id="house-personal-address" name="house-personal-address"
									type="text" placeholder="<?php _e( 'Дом', 'la-fleur' ); ?>" value="">
								<div class="input-mail-error__message">
									<p><?php _e( 'Введите корректный номер дома.', 'la-fleur' ); ?></p>
								</div>
							</div>

						</div>

						<div class="tab_content-r_itm">

							<!-- street  -->
							<div class="section-all-input">
								<label for="street-personal-address"><?php _e( 'Улица', 'la-fleur' ); ?></label>
								<input id="street-personal-address" name="street-personal-address"
									type="text" placeholder="<?php _e( 'Улица', 'la-fleur' ); ?>" value="">
								<div class="input-mail-error__message">
									<p><?php _e( 'Введите корректную улицу.', 'la-fleur' ); ?></p>
								</div>
							</div>

							<!-- name  -->
							<div class="section-all-input">
								<label for="flat-personal-address"><?php _e( 'Квартира', 'la-fleur' ); ?></label>
								<input id="flat-personal-address" name="flat-personal-address"
									type="text" placeholder="<?php _e( 'Квартира', 'la-fleur' ); ?>" value="">
								<div class="input-mail-error__message">
									<p><?php _e( 'Введите корректный номер квартиры.', 'la-fleur' ); ?></p>
								</div>
							</div>
						</div>

						<div class="tab_content-r_sbm">
							<div class="all_link hover_effect-for link-shop-card-modal">
								<label>
									<input type="submit" name="la_fleur_save_address">
									<p><?php _e( 'Сохранить', 'la-fleur' ); ?></p>
									
									<?php wp_nonce_field( 'woocommerce-edit_address', 'woocommerce-edit-address-nonce' ); ?>
									<input type="hidden" name="action" value="la_fleur_edit_address" />
									<input type="hidden" name="active_shipping_address" value="" />
									<input type="hidden" name="address_id" value="" />
								</label>
							</div>
						</div>
					</div>
				</div>
			</form>
			
			<p id="response_text"></p>

			<!-- show user all adress -->
			<div class="cabinet-show-all-adress d-f-column-f_c-f_s">

				<?php
				$old_address = get_user_meta( $user_id, 'la_fleur_list_addresses', true );
				if ( ! empty( $old_address ) ) {
					foreach ( $old_address as $key => $address_arr ) {
						
						if ( $address_arr['active_shipping_address'] == 'yes' ) {
							$checked = 'checked';
						} else {
							$checked = '';
						}
						?>
						<!-- item 1 -->
						<div class="cabinet-show-all-adress_itm d-f-row-stre-s_b" data-address-id="<?php esc_html_e( $key ); ?>">
							<!-- left -->
							<div class="cabinet-show-all-adress_itm_l d-f-row-c-f_s">
								<div class="section-shop-filter_item animation-this-duration">
									<label class="d-f-row-c-f_s">
										<input type="radio" name="cabinet-this-adress" <?php echo $checked; ?>>
										<div class="block-for-input-filter"></div>
										<div class="cabinet-your-adrss d-f-column-f_s-f_s">
											<p>
												<?php
												if ( $address_arr['city'] ) {
													echo __( 'г. ', 'la-fleur' ) . esc_html( $address_arr['city'] );
												}
												?>
											</p>	
											<p>
												<?php
												if ( $address_arr['street'] ) {
													echo __( 'ул. ', 'la-fleur' ) . esc_html( $address_arr['street'] );
												}
												if ( $address_arr['house'] ) {
													echo ', ' . esc_html( $address_arr['house'] );
												}
												if ( $address_arr['flat'] ) {
													echo __( ' кв. ', 'la-fleur' ) . esc_html( $address_arr['flat'] );
												}
												?>
											</p>
										</div>
									</label>
								</div>
							</div>

							<!-- right -->
							<div class="cabinet-show-all-adress_itm_r d-f-row-c-f_end">
								<a href="#" class="cabinet-adress-edit"><?php _e( 'Изменить адрес', 'la-fleur' ); ?></a>
								<a href="#" class="cabinet-adress-remove">
									<svg width="15" height="19" viewBox="0 0 15 19" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<path
											d="M4.89528 6.91504C5.04561 6.91504 5.17383 7.0402 5.17383 7.19665V15.6451C5.17383 15.7971 5.05003 15.9267 4.89528 15.9267C4.74054 15.9267 4.61674 15.8015 4.61674 15.6451V7.19665C4.61674 7.0402 4.74054 6.91504 4.89528 6.91504Z"
											fill="black" />
										<path
											d="M10.1082 6.91504C10.2585 6.91504 10.3867 7.0402 10.3867 7.19665V15.6451C10.3867 15.7971 10.2629 15.9267 10.1082 15.9267C9.95343 15.9267 9.82963 15.8015 9.82963 15.6451V7.19665C9.82963 7.0402 9.95343 6.91504 10.1082 6.91504Z"
											fill="black" />
										<path
											d="M0.0125389 3.66545C0.109808 2.90107 0.759743 2.32443 1.52463 2.32443H3.95194V1.60028C3.94751 1.17563 4.11995 0.764381 4.41175 0.464887C4.70798 0.165392 5.1059 0 5.5215 0H5.53477H9.47858C9.89861 0 10.2921 0.165392 10.5883 0.464887C10.8801 0.759911 11.0526 1.17563 11.0482 1.59581V2.32443H13.4799C14.2403 2.32443 14.8903 2.90107 14.9875 3.66545C15.0848 4.42983 14.6029 5.15398 13.869 5.35514L13.7452 5.38643V16.5303C13.7452 17.1293 13.5241 17.7104 13.1439 18.1172C12.768 18.5239 12.2331 18.7564 11.6848 18.7564H3.31968C2.77144 18.7564 2.23646 18.5239 1.86065 18.1172C1.476 17.7104 1.25935 17.1293 1.25935 16.5303V5.38643L1.13555 5.35514C0.392773 5.15398 -0.0847301 4.42983 0.0125389 3.66545ZM3.31526 18.1932H11.6848C12.5293 18.1932 13.1925 17.4601 13.1925 16.5258V5.38643H1.8076V16.5303C1.8076 17.4601 2.47079 18.1932 3.31526 18.1932ZM9.47858 0.558758H9.4609H5.5215C5.2518 0.558758 4.99537 0.66604 4.80525 0.858252C4.61071 1.05494 4.50018 1.32314 4.5046 1.60028V2.32443H10.4955V1.59581C10.4999 1.32314 10.3894 1.05047 10.1948 0.858252C10.0047 0.66604 9.74828 0.558758 9.47858 0.558758ZM13.4799 2.88319H1.52463C0.994073 2.88319 0.560782 3.32126 0.560782 3.85767C0.560782 4.39407 0.994073 4.83214 1.52463 4.83214H13.4799C14.0104 4.83214 14.4437 4.39407 14.4437 3.85767C14.4437 3.32126 14.006 2.88319 13.4799 2.88319Z"
											fill="black" />
										<path
											d="M7.50271 6.91504C7.65303 6.91504 7.78125 7.0402 7.78125 7.19665V15.6451C7.78125 15.7971 7.65745 15.9267 7.50271 15.9267C7.34796 15.9267 7.22417 15.8015 7.22417 15.6451V7.19665C7.22417 7.0402 7.34796 6.91504 7.50271 6.91504Z"
											fill="black" />
									</svg>
								</a>
							</div>
						</div>
					<?php
					} 
				} else {
					echo '<p>'. __( 'Нет добавленых адресов', 'la-fleur' ) .'</p>';
				}
				?>
				<?php wp_nonce_field( 'iv-ajax-nonce', 'security' ); ?>
				
			</div>

		</div>
	</div>

<?php
