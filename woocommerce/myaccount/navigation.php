<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>

	<ul>
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
		
			<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>" class="link-menu-for-cabinet <?php echo la_fleur_get_account_menu_item_classes( $endpoint ); ?>" >
					<svg width="18" height="19" viewBox="0 0 18 19" fill="none"
						xmlns="http://www.w3.org/2000/svg">
						<path
							d="M15.4502 9.20528L18 8.98919L15.4502 8.77311C12.1224 8.4922 9.48619 5.85594 9.20528 2.52821L9.0108 0L8.79472 2.54982C8.51381 5.87755 5.87755 8.51381 2.54982 8.79472L0 9.0108L2.54982 9.22689C5.87755 9.5078 8.51381 12.1441 8.79472 15.4718L9.0108 18.0216L9.22689 15.4718C9.48619 12.1224 12.1224 9.48619 15.4502 9.20528Z"
							fill="#7F5106" />
					</svg>
					<?php echo esc_html( $label ); ?>
				</a>
			</li>
			
		<?php endforeach; ?>
	</ul>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
