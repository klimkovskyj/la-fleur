<?php
/**
 * Change-password
 * Custom endpoint
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' );

$page_my_account = get_field( 'page_my_account' );
?>

<!-- mobile btn 4 -->

<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo esc_url( home_url( '/my-account/' ) ); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['data_title'] ); ?></p>
			<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<line y1="-0.5" x2="20.7549" y2="-0.5"
					transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
					stroke="black" />
				<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
				<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
			</svg>
		</div>
	</a>
</button>
<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo esc_url( home_url( '/my-account/all-orders/' ) ); ?>">	
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['orders_title'] ); ?></p>
			<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<line y1="-0.5" x2="20.7549" y2="-0.5"
					transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
					stroke="black" />
				<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
				<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
			</svg>
		</div>
	</a>
</button>
<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo esc_url( home_url( '/my-account/edit-address/' ) ); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['address_title'] ); ?></p>
			<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<line y1="-0.5" x2="20.7549" y2="-0.5"
					transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
					stroke="black" />
				<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
				<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
			</svg>
		</div>
	</a>
</button>
<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active-cabinet-mob-btn" data-cabinet-tab='4'>
	<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
		<p><?php esc_html_e( $page_my_account['change_password_title'] ); ?></p>
		<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
			xmlns="http://www.w3.org/2000/svg">
			<line y1="-0.5" x2="20.7549" y2="-0.5"
				transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
				stroke="black" />
			<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
			<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
		</svg>
	</div>
</button>

<!-- tab 4  -->
<div class="cabinet-show-tab cabinet-show-tab-2 active_cabinet-show-tab" data-cabinet-tab='4'>
	<div class="cabinet-show-tab_wr d-f-column-f_s-f_s">

		<!-- all title -->
		<div class="cabinet_title is_descktop">
			<h2><?php esc_html_e( $page_my_account['change_password_title'] ); ?></h2>
		</div>

		<?php //do_action( 'woocommerce_edit_account_form_start' ); ?>

		<!-- this content -->
		<form id="cabinet-add-new-password" action="" method="post">
			<div class="cabinet-tab_content d-f-row-stre-s_b">
				<!-- item 1 -->
				<div class="tab_content-1">
					<p><?php echo wp_kses_post( $page_my_account['change_password_desk'] ); ?></p>
				</div>

				<!-- item 2 -->
				<div class="tab_content-r">
					<div class="tab_content-r_itm">

						<!-- password  -->
						<div class="section-all-input">
							<label for="cabinet-pass"><?php _e( 'Старый пароль', 'la-fleur' ); ?></label>
							<input id="cabinet-pass" name="password_current" type="password"
								placeholder="<?php _e( 'Старый пароль', 'la-fleur' ); ?>" autocomplete="off" />
							<div class="input-mail-error__message">
								<p><?php _e( 'Введите корректный пароль.', 'la-fleur' ); ?></p>
							</div>
						</div>

					</div>

					<div class="tab_content-r_itm">

						<!-- repeat password  -->
						<div class="section-all-input">
							<label for="cabinet-repeat-pass"><?php _e( 'Новый пароль', 'la-fleur' ); ?></label>
							<input id="cabinet-repeat-pass" name="password_1"
								type="password" placeholder="<?php _e( 'Новый пароль', 'la-fleur' ); ?>" autocomplete="off" />
							<div class="input-mail-error__message">
								<p><?php _e( 'Пароли не свовпадают', 'la-fleur' ); ?></p>
							</div>
						</div>

					</div>

					<div class="tab_content-r_sbm">
						<div class="all_link hover_effect-for link-shop-card-modal">
							<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
							<input type="hidden" name="action" value="la_fleur_save_changed_password" />
							<label>
								<input type="submit"  name="save_account_details" >
								<p><?php _e( 'Сохранить', 'la-fleur' ); // ?></p>
							</label>
						</div>
					</div>
				</div>
			</div>

			<?php //do_action( 'woocommerce_edit_account_form_end' ); ?>
		</form>

	</div>
</div>

<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo wc_logout_url(); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['logout_title'] ); ?></p>
		</div>
	</a>	
</button>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
