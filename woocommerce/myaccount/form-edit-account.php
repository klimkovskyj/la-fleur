<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' );

$page_my_account = get_field( 'page_my_account' );
?>

<!-- mobile btn 1 -->
<button class="cabinet-mobile-btn is_mobile w_100 active-cabinet-mob-btn" data-cabinet-tab='1'>
	<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
		<p><?php esc_html_e( $page_my_account['data_title'] ); ?></p>
		<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
			xmlns="http://www.w3.org/2000/svg">
			<line y1="-0.5" x2="20.7549" y2="-0.5"
				transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
				stroke="black" />
			<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
			<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
		</svg>
	</div>
</button>

<!-- tab 1  -->
<div class="cabinet-show-tab cabinet-show-tab-1 active_cabinet-show-tab" data-cabinet-tab='1'>
	<div class="cabinet-show-tab_wr d-f-column-f_s-f_s">
		<?php
		$curr_user_id = get_current_user_id();

		$phone = get_user_meta( $curr_user_id, 'phone', true );
		$city = get_user_meta( $curr_user_id, 'city', true );
		$phone = ! empty( $phone ) ? $phone : '';
		$city  = ! empty( $city  ) ? $city  : '';

		$first_name  = ! empty( $user->first_name  ) ? $user->first_name  : '';
		$last_name  = ! empty( $user->last_name  ) ? $user->last_name  : '';
		$user_email  = ! empty( $user->user_email  ) ? $user->user_email  : '';
		?>
		<!-- all title -->
		<div class="cabinet_title is_descktop">
			<h2><?php esc_html_e( $page_my_account['data_title'] ); ?></h2>
		</div>

		<!-- this content  id="personal-data"-->
		<form class="woocommerce-EditAccountForm edit-account test" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?>>
			
			<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
			
			<div class="cabinet-tab_content d-f-row-stre-s_b">
				<!-- item 1 -->
				<div class="tab_content-1">
					<p><?php echo wp_kses_post( $page_my_account['data_desc'] ); ?></p>
				</div>

				<!-- item 2 -->
				<div class="tab_content-r">
					<div class="tab_content-r_itm">
					
						<!-- name  -->
						<div class="section-all-input">
							<label for="account_first_name"><?php esc_html_e( 'Имя', 'la-fleur' ); ?></label>
							<input id="account_first_name" name="account_first_name" type="text"
								placeholder="<?php esc_html_e( 'Имя', 'la-fleur' ); ?>" value="<?php echo esc_attr( $first_name ); ?>" autocomplete="given-name">
							<div class="input-mail-error__message">
								<p><?php esc_html_e( 'Введите корректное имя.', 'la-fleur' ); ?></p>
							</div>
						</div>

						<!-- city  -->
						<div class="section-all-input">
							<label for="city-cabinet-data"><?php esc_html_e( 'Город', 'la-fleur' ); ?></label>
							<input id="city-cabinet-data" name="city-cabinet-data" type="text"
								placeholder="<?php esc_html_e( 'Город', 'la-fleur' ); ?>" value="<?php echo esc_attr( $city ); ?>" autocomplete="city">
							<div class="input-mail-error__message">
								<p><?php esc_html_e( 'Введите корректный город', 'la-fleur' ); ?></p>
							</div>
						</div>

						<!-- phone  -->
						<div class="section-all-input">
							<label for="phone-cabinet-data"><?php esc_html_e( 'Телефон', 'la-fleur' ); ?></label>
							<input id="phone-cabinet-data" class="mask-phone"
								name="phone-cabinet-data" type="tel"
								placeholder="+3__ ___ __ ___" value="<?php echo esc_html( $phone ); ?>" autocomplete="phone">
							<div class="input-mail-error__message">
								<p><?php esc_html_e( 'Введите корректное номер телефона.', 'la-fleur' ); ?></p>
							</div>
						</div>
					</div>

					<div class="tab_content-r_itm">

						<!-- surname  -->
						<div class="section-all-input">
							<label for="account_last_name"><?php esc_html_e( 'Фамилия', 'la-fleur' ); ?></label>
							<input id="account_last_name" name="account_last_name" type="text"
								placeholder="<?php esc_html_e( 'Фамилия', 'la-fleur' ); ?>" value="<?php echo esc_attr( $last_name ); ?>"  autocomplete="family-name">
							<div class="input-mail-error__message">
								<p><?php esc_html_e( 'Введите корректную фамилию.', 'la-fleur' ); ?></p>
							</div>
						</div>

						<!-- mail -->
						<div class="section-all-input">
							<label for="account_email"><?php esc_html_e( 'Email', 'la-fleur' ); ?></label>
							<input id="account_email" name="account_email" type="email"
								placeholder="<?php esc_html_e( 'Email', 'la-fleur' ); ?>" value="<?php echo esc_attr( $user_email ); ?>" autocomplete="email">
							<div class="input-mail-error__message">
								<p><?php esc_html_e( 'Введите корректный адрес электронной почты.', 'la-fleur' ); ?></p>
							</div>
						</div>
					</div>

					<div class="tab_content-r_sbm">
						<div class="all_link hover_effect-for link-shop-card-modal">
							<label>
								<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
								<input type="submit" name="save_account_details" />
								<input type="hidden" name="action" value="save_account_details" />
								<p><?php esc_html_e( 'Сохранить', 'la-fleur' ); ?></p>
							</label>
						</div>
					</div>
				</div>
			</div>
			<?php do_action( 'woocommerce_edit_account_form' ); ?>
				
			<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
		</form>
	</div>
</div>

<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo esc_url( home_url( '/my-account/all-orders/' ) ); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['orders_title'] ); ?></p>
			<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<line y1="-0.5" x2="20.7549" y2="-0.5"
					transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
					stroke="black" />
				<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
				<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
			</svg>
		</div>
	</a>
</button>
<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo esc_url( home_url( '/my-account/edit-address/' ) ); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['address_title'] ); ?></p>
			<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<line y1="-0.5" x2="20.7549" y2="-0.5"
					transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
					stroke="black" />
				<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
				<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
			</svg>
		</div>
	</a>
</button>
<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo esc_url( home_url( '/my-account/change-password/' ) ); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['change_password_title'] ); ?></p>
			<svg width="17" height="16" viewBox="0 0 17 16" fill="none"
				xmlns="http://www.w3.org/2000/svg">
				<line y1="-0.5" x2="20.7549" y2="-0.5"
					transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 16 0.430664)"
					stroke="black" />
				<line x1="1.4668" y1="2.77979" x2="1.4668" y2="15.0001" stroke="black" />
				<line x1="13.4814" y1="15.5" x2="0.967262" y2="15.5" stroke="black" />
			</svg>
		</div>
	</a>
</button>
<button class="cabinet-mobile-btn is_mobile d-f-row-c-s_b w_100 active_cabinet-show-tab">
	<a class="mob-links" href="<?php echo wc_logout_url(); ?>">
		<div class="cabinet-mobile-btn_wr d-f-row-c-s_b w_100">
			<p><?php esc_html_e( $page_my_account['logout_title'] ); ?></p>
		</div>
	</a>
</button>


<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
