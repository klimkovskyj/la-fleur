<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>

<div class="section-oreder">
	<div class="section-oreder_wr d-f-row-stre-s_b">

		<!-- left -->
		<div class="section-oreder_left">
			<div class="section-oreder_left_wr">

				<div class="order-mobile-title is_mobile">
					<h5><?php _e( 'ваша корзина', 'la-fleur' ); ?></h5>
				</div>
				<div class="line-in-order-step"></div>
				<!-- basket -->
				<div id="la_fleur_checkout">
					<?php echo la_fleur_checkout(); ?>
				</div>
			 
			</div>
		</div>

		<!-- right -->
		<div class="section-oreder_right">
			<div class="section-oreder_right_wr">

				<div class="section-oreder_title is_descktop">
					<h3><?php _e( 'Оформление заказа', 'la-fleur' ); ?></h3>
				</div>

				<?php
				do_action( 'woocommerce_before_checkout_form', $checkout );
				
				echo '<p class="checkout-p-required"><a href="'. esc_url( home_url( '/cart/' ) ) .'">'. __( 'Вернутся в корзину для добавления данных', 'la-fleur' ) .'</a></p>';
				
				$user_id = get_current_user_id();
				if ( $user_id ) {
					$user = get_user_by( 'id', $user_id );
					
					$old_address = get_user_meta( $user_id, 'la_fleur_list_addresses', true );
					if ( ! empty( $old_address ) ) {
						foreach ( $old_address as $address_arr ) {				
							if ( $address_arr['active_shipping_address'] == 'yes' ) {
								$address_1 = get_user_meta( $user_id, 'shipping_address_1', true );
								
								$city = get_user_meta( $user_id, 'city', true );
								$street = get_user_meta( $user_id, 'la_fleur_shipping_street', true );
								$house = get_user_meta( $user_id, 'la_fleur_shipping_house', true );
								$flat = get_user_meta( $user_id, 'la_fleur_shipping_flat', true );		
							}
						}
					}
				
					$user_phone = get_user_meta( $user_id, 'phone', true );
				}
				
				$city = ! empty( $city ) ? $city : '';
				$street = ! empty( $street ) ? $street : '';
				$house = ! empty( $house ) ? $house : '';
				$flat = ! empty( $flat ) ? $flat : '';
				?>
				<div class="section-oreder_our-data">
					<div class="section-oreder_our-data_wr d-f-column-f_s-f_s">
						<form id="form-order-step-2" name="checkout" method="post" class="d-f-column-f_s-f_s" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
							
							
							
							<div class="section-oreder_our-data_title">
								<h5><?php _e( 'Информация о доставке', 'la-fleur' ); ?></h5>
							</div>

							<div class="section-oreder_our-data_inputs d-f-row-stre-s_b">
								<!-- left -->
								<div class="section-oreder_our-data_inputs_l">

									<!-- choose city  -->
									<div  id="by_courier_city" class="section-all-input">
										<label for="city-order-st-2"><?php _e( 'Город', 'la-fleur' ); ?></label>
										<input id="city-order-st-2" name="shipping_city" type="text"
											placeholder="<?php _e( 'Город', 'la-fleur' ); ?>" value="<?php echo $city; ?>" />
										<div class="input-mail-error__message">
											<p><?php _e( 'Введите корректный город', 'la-fleur' ); ?></p>
										</div>
									</div>
									
									<?php
										$packages = WC()->shipping()->get_packages();
										$chosen_method = isset( WC()->session->chosen_shipping_methods[ 0 ] ) ? WC()->session->chosen_shipping_methods[ 0 ] : 'No method';
									
										$flat_rate_courier = '';
										$delivery_label = '';
										$page_checkout_delivery = get_field( 'page_checkout_delivery' );
									?>
									<!-- select -->
									<div
										class="select-with-arrow_b-b container-with-border-into select-with-terms">
										<select class="select-with-border-into" name="ship"
											id="order-delivery" data-placeholder="Варианты доставки">
											<option value=""><?php _e( 'Выберите вариант доставки', 'la-fleur' ); ?></option>
											<?php foreach ( $packages[0]['rates'] as $key => $value ) { ?>
												<?php
												if ( $key == 'local_pickup:2' ) {
													$delivery_label = $page_checkout_delivery['local'];
												} elseif ( $key == 'flat_rate:4' ) {
													$delivery_label = $page_checkout_delivery['courier'];
												} elseif ( $key == 'flat_rate:5' ) {
													$delivery_label = $page_checkout_delivery['nova_poshta'];
												}
												?>
												<option data-shippinf-cost="<?php echo $value->cost; ?>" value="<?php echo $key ?>" <?php if ( $value->id == $chosen_method ) echo 'selected'; ?>><?php echo $delivery_label; ?></option>
												<?php
												if ( $key == 'flat_rate:4' )
													$flat_rate_courier = $value->cost . get_woocommerce_currency_symbol();
												?>
											<?php } ?>
											
										</select>
									</div>

									<!-- section terms for method delivery -->
									<div class="order-terms">
										<div class="order-terms_txt" id="conditions-delivery"><?php _e( 'Условия', 'la-fleur' ); ?> <span
												id="delivery-tooltiptext"><?php _e( 'Условия доставки', 'la-fleur' ); ?></span></div>
									</div>

									<!-- Select branch when user choose delivery new post or ukrpost -->
									<div id="nova_poshta_option" class="select-with-arrow_b-b container-with-border-into">
										
										<select class="select-with-border-into" name="order-branch"
											id="order-branch" data-placeholder="<?php _e( 'Выберете отделение', 'la-fleur' ); ?>">
											<option value=""></option>
											<?php
											$nova_poshta_sections = get_field( 'nova_poshta_sections', 'option' );
											foreach ( $nova_poshta_sections as $section ) {
												echo '<option value="'. $section['section'] .'">'. $section['section'] .'</option>';
											}
											?>
											
										</select>
									</div>

									<!-- choose street when user choose delivery to the home. No pick up at the store.  -->
									<div id="by_courier_street" class="section-all-input">
										<label for="street-order-st-2"><?php _e( 'Улица', 'la-fleur' ); ?></label>
										<input id="street-order-st-2" name="street-order-st-2" type="text"
											placeholder="<?php _e( 'Улица', 'la-fleur' ); ?>" value="<?php echo $street; ?>" />
									</div>

									<!-- choose is number your home and number flat. When user choose delivery to the home. No pick up at the store.  -->
									<div id="by_courier_house" class="order-step-2_block-number_fl_house">
										<!-- number user house -->
										<div class="section-all-input">
											<label for="number-house-order-st-2"><?php _e( 'Дом', 'la-fleur' ); ?></label>
											<input id="number-house-order-st-2" name="number-house-order-st-2"
												type="text" placeholder="<?php _e( 'Дом', 'la-fleur' ); ?>" value="<?php echo $house; ?>" />
										</div>

										<!-- number user flat -->
										<div class="section-all-input">
											<label for="number-flat-order-st-2"><?php _e( 'Квартира', 'la-fleur' ); ?></label>
											<input id="number-flat-order-st-2" name="number-flat-order-st-2"
												type="text" placeholder="<?php _e( 'Квартира', 'la-fleur' ); ?>" value="<?php echo $flat; ?>" />
										</div>
										
										<input type="hidden" name="shipping_address_1" value="<?php echo $address_1; ?>" />
	
									</div>
								</div>
								<!-- right -->
								<div id="checkout_right" class="section-oreder_our-data_inputs_r">
									<div class="section-oreder_our-data_inputs_r_wr">
									<?php
									$available_gateways = WC()->payment_gateways()->get_available_payment_gateways();
									WC()->payment_gateways()->set_current_gateway( $available_gateways );
									
									$payment_label = '';
									$page_checkout_payment = get_field( 'page_checkout_payment' );							
									?>
										<!-- select choose paymend method -->
										<div
											class="select-with-arrow_b-b container-with-border-into select-with-terms">
											<select class="select-with-border-into" name="paymend_method"
												id="order-paymend-method" data-placeholder="Вариант оплаты">
												<option value=""><?php _e( 'Выберите вариант оплаты', 'la-fleur' ); ?></option>
												<?php foreach ( $available_gateways as $key => $gateway ) {
													if ( $key == 'bacs' ) {
														$payment_label = $page_checkout_payment['bacs'];
													} elseif ( $key == 'cod' ) {
														$payment_label = $page_checkout_payment['cod'];
													}
													?>
													
													<option value="<?php echo esc_attr( $gateway->id ); ?>" <?php if ( $gateway->id == $gateway->chosen ) echo 'selected'; ?>><?php echo $payment_label; ?></option>
												<?php } ?>
											</select>
										</div>	
										
										<!-- section terms for method payment -->
										<div class="order-terms">
											<div class="order-terms_txt" id='payment-method-select-btn'>
												<?php _e( 'Условия', 'la-fleur' ); ?><span id="payment-method_txt"></span></div>
										</div>

										<div class="secttion-textarea">
											<textarea name="order_comments" id="order-textarea" cols="30"
												rows="10" placeholder="<?php _e( 'Коментарий к заказу', 'la-fleur' ); ?>"></textarea>
										</div>
									</div>

								</div>

								<!-- btn -->
								<div class="all-link-order all-link-sbm">
									<div class="all_link hover_effect-for">
										<label>
											<input type="submit" name="woocommerce_checkout_place_order" />
											<p><?php _e( 'Оформить заказ', 'la-fleur' ); ?></p>
										</label>
									</div>
								</div>
								
								<?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
								
								<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php //do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php //do_action( 'woocommerce_checkout_billing' );
				
				//$customer = new WC_Customer( $user_id );
				$first_name  = ! empty( $_POST['name-order-st-1'] ) ? sanitize_user( $_POST['name-order-st-1'] ) : $user->first_name;
				$last_name  = ! empty( $_POST['account_last_name']  ) ? sanitize_user( $_POST['account_last_name'] ) : $user->last_name;
				$user_email  = ! empty( $_POST['mail-order-st-1'] ) ? sanitize_email( $_POST['mail-order-st-1'] ) : $user->user_email;
				$user_phone  = ! empty( $_POST['phone-order-st-1'] ) ? $_POST['phone-order-st-1'] : $user_phone;
				?>
				
				<input type="hidden" name="billing_first_name" value="<?php echo $first_name; ?>" />
				<input type="hidden" name="billing_last_name" value="<?php echo $last_name; ?>" />
				<input type="hidden" name="billing_phone" value="<?php echo $user_phone; ?>" />
				<input type="hidden" name="billing_email" value="<?php echo $user_email; ?>" />
				
			</div>

			<div class="col-2">
				<?php //do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php //do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>
	
	<?php //do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
	
	
	
	<?php //do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php //do_action( 'woocommerce_checkout_after_order_review' ); ?>
								
							</div>
						</form>
						
					</div>
				</div>
				<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
			</div>
		</div>

	</div>
</div>

<?php $page_checkout = get_field( 'page_checkout' ); ?>
<!-- script for terms when choose method -->
<script>
	document.addEventListener('DOMContentLoaded', function () {

		let selectOrderDelivery;
		if (document.getElementById('order-delivery') != null && document.getElementById('order-delivery') != undefined) {
			selectOrderDelivery = document.getElementById('order-delivery');
			selectOrderDelivery.onchange = function () {
				seeValueDelivery(selectOrderDelivery);
			}

			seeValueDelivery(selectOrderDelivery);
		}

		let selectOrderPayment;
		if (document.getElementById('order-paymend-method') != null && document.getElementById('order-paymend-method') != undefined) {
			selectOrderPayment = document.getElementById('order-paymend-method');
			selectOrderPayment.onchange = function () {
				seeValuePayment(selectOrderPayment);
			}

			seeValuePayment(selectOrderPayment);
		}
	});

	function seeValueDelivery(this_v) {
		if (this_v.value == '') {
			document.getElementById('delivery-tooltiptext').innerHTML = '<?php esc_html_e( $page_checkout["title_payment"] ) ?>';
		} else if (this_v.value == 'flat_rate:5') {// delivery-np
			document.getElementById('delivery-tooltiptext').innerHTML = '<?php echo wp_kses_post( $page_checkout["nova_poshta"] ) ?>';
		} else if (this_v.value == 'flat_rate:4') {// delivery-kurier
			document.getElementById('delivery-tooltiptext').innerHTML = '<?php echo wp_kses_post( $page_checkout["courier"] ) . ' ' . $flat_rate_courier . '.' ?>';
		} else if (this_v.value == 'local_pickup:2') {// delivery-personal
			document.getElementById('delivery-tooltiptext').innerHTML = '<?php echo wp_kses_post( $page_checkout["local"] ) ?>';
		}
	}

	function seeValuePayment(this_v) {
		if (this_v.value == '') {
			document.getElementById('payment-method_txt').innerHTML = '<?php esc_html_e( $page_checkout["title_shipping"] ) ?>';
		} else if (this_v.value == 'bacs') {// card
			document.getElementById('payment-method_txt').innerHTML = '<?php echo wp_kses_post( $page_checkout["bacs"] ) ?>';
		} else if (this_v.value == 'cod') {// money
			document.getElementById('payment-method_txt').innerHTML = '<?php echo wp_kses_post( $page_checkout["cod"] ) ?>';
		}
	}
</script>

