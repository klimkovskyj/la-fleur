<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

	<!-- item -->
	
		<a href="<?php echo esc_url( get_the_permalink() ); ?>">
			<div class="perfumency-with-product_top d-f-column-f_s-f_s">
				<div class="perfumency-with-product_top__price">
					<?php la_fleur_get_price( $product ); ?>				
				</div>


				<div class="perfumency-product_img animation-img-bottom _anim-items">
					<?php
						echo woocommerce_get_product_thumbnail( 'full' );
					?>
				</div>

				<?php
					$pa_product_kind = $product->get_attribute( 'pa_product-kind' );
					if ( ! empty( $pa_product_kind ) ) {
						$pa_product_kind_arr = explode( ',', $pa_product_kind );
						echo '<div class="label_for_type-product">';
						echo '<p>'. esc_html( $pa_product_kind_arr[0] ) .'</p>';
						echo '</div>';
					}
				?>
				
			</div>

			<div class="perfumency-with-product_bottom d-f-column-f_s-f_s">
				<div class="perfumency-with-product_name">
					<h4><?php echo get_the_title(); ?></h4>
				</div>

				<div class="perfumency-with-product_info">
					<h5>
						<?php
						$pa_brand = $product->get_attribute( 'pa_brand' );
						if ( ! empty( $pa_brand ) ) {
							$pa_brand_arr = explode( ',', $pa_brand );
							echo esc_html( $pa_brand_arr[0] );
						}
						?>
					</h5>

					<p>
						<?php
						$pa_volume = $product->get_attribute( 'pa_volume' );
						if ( ! empty( $pa_volume ) ) {
							$pa_volume_arr = explode( ',', $pa_volume );
							echo esc_html( $pa_volume_arr[0] );
						}
						?>
					</p>
				</div>
			</div>
		</a>

		<div class="product_btn_to_pay">	
			<a href="<?php echo esc_url( $product->add_to_cart_url() ); ?>"
				class="btn-add-to-cart add_to_cart_button ajax_add_to_cart"
				data-product_id="<?php echo $product->get_id(); ?>"
				data-product_sku="<?php echo $product->get_sku(); ?>"
				rel="nofollow">
				<?php _e( 'Купить', 'la-fleur' ); ?>		
			</a>
		</div>
	

<?php
