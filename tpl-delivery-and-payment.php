<?php
/**
 * Template name: Template Delivery and Payment
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package  LaFleur
 */

get_header();
?>
	
		<!-- <section pagination start -->
		<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
		<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
			<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
				<div class="section-pagination__wrapper all-width">
					<ul class="breadcrumb">
						<?php woocommerce_breadcrumb(); ?>
					</ul>
				</div>
			</div>
		<?php } ?>
		<!-- <section pagination end -->
	
		<?php $page_d_and_p = get_field( 'page_delivery_and_payment' ); ?>
		<div class="page-info-method w_100 bg-PDP">
            <div class="page-info-method_wr all-width d-f-column-f_s-f_s">
				<?php if ( ! empty( $page_d_and_p['delivery_title'] ) ) { ?>
					<div class="page-info-method_title w_100">
						<h2><?php echo esc_html( $page_d_and_p['delivery_title'] ); ?></h2>
					</div>
				<?php } ?>

                <div class="page-info-method_items w_100">
					<?php
					if ( ! empty( $page_d_and_p['delivery_section'] ) ) {
						$counter = 1;
						foreach ( $page_d_and_p['delivery_section'] as $section ) {
							if ( $counter < 10 ) { $counter_text = '0' . $counter; } else { $counter_text = $counter; }
							?>
							<div class="page-info-method_item w_100">
								<div class="page-info-method_item_head d-f-row-c-s_b">
									<div class="page-info-method_item_head_l d-f-row-c-s_b">
										<p class="is_descktop"><?php echo $counter_text; ?></p>
										<h4><?php echo esc_html( $section['name'] ); ?></h4>
										<h5 class="is_descktop">
											<?php echo wp_kses_post( $section['short_desk'] ); ?>
										</h5>
									</div>

									<div class="page-info-method_item_head_r">
										<p class="is_mobile"><?php echo $counter_text; ?></p>
										<svg width="43" height="43" viewBox="0 0 43 43" fill="none"
											xmlns="http://www.w3.org/2000/svg">
											<line x1="42.1504" y1="0.819374" x2="1.06821" y2="41.9016" stroke="black" />
											<line x1="0.703125" y1="7.12109" x2="0.703126" y2="41.7431" stroke="black" />
											<line x1="34.8242" y1="42.2432" x2="0.202167" y2="42.2432" stroke="black" />
										</svg>
									</div>
								</div>

								<div class="page-info-method_item_show w_100">
									<p><?php echo wp_kses_post( $section['long_desk'] ); ?></p>
								</div>
							</div>
							<?php
						$counter++;
						}
                    }
                    ?>
				</div>
            </div>
        </div>


        <div class="page-info-method w_100">
            <div class="page-info-method_wr all-width d-f-column-f_s-f_s">
				<?php if ( ! empty( $page_d_and_p['payment_title'] ) ) { ?>
					<div class="page-info-method_title w_100">
						<h2><?php echo esc_html( $page_d_and_p['payment_title'] ); ?></h2>
					</div>
				<?php } ?>
                

                <div class="page-info-method_items w_100">
                    <?php
					if ( ! empty( $page_d_and_p['payment_section'] ) ) {
						$counter = 1;
						foreach ( $page_d_and_p['payment_section'] as $section ) {
							if ( $counter < 10 ) { $counter_text = '0' . $counter; } else { $counter_text = $counter; }
							?>
							<div class="page-info-method_item w_100">
								<div class="page-info-method_item_head d-f-row-c-s_b">
									<div class="page-info-method_item_head_l d-f-row-c-s_b">
										<p class="is_descktop"><?php echo $counter_text; ?></p>
										<h4><?php echo esc_html( $section['name'] ); ?></h4>
										<h5 class="is_descktop">
											<?php echo wp_kses_post( $section['short_desk'] ); ?>
										</h5>
									</div>

									<div class="page-info-method_item_head_r">
										<p class="is_mobile"><?php echo $counter_text; ?></p>
										<svg width="43" height="43" viewBox="0 0 43 43" fill="none"
											xmlns="http://www.w3.org/2000/svg">
											<line x1="42.1504" y1="0.819374" x2="1.06821" y2="41.9016" stroke="black" />
											<line x1="0.703125" y1="7.12109" x2="0.703126" y2="41.7431" stroke="black" />
											<line x1="34.8242" y1="42.2432" x2="0.202167" y2="42.2432" stroke="black" />
										</svg>
									</div>
								</div>

								<div class="page-info-method_item_show w_100">
									<p><?php echo wp_kses_post( $section['long_desk'] ); ?></p>
								</div>
							</div>
							<?php
						$counter++;
						}
                    }
                    ?>
                </div>
            </div>
        </div>

<?php
get_footer();
