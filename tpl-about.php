<?php
/**
 * Template name: Template About
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package  LaFleur
 */

get_header();
?>

       <!-- <section pagination start -->
		<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
		<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
			<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
				<div class="section-pagination__wrapper all-width">
					<ul class="breadcrumb">
						<?php woocommerce_breadcrumb(); ?>
					</ul>
				</div>
			</div>
		<?php } ?>
		<!-- <section pagination end -->
        
        <!-- about title -->
        <?php $page_about = get_field( 'page_about' ); ?>
        <div class="page-about_title w_100">
            <div class="page-about_title_wr all-width d-f-column-f_c-f_s">
                <div class="all_big_title abt-title_this">

                    <h2 class="animation-opacity _anim-items _active-scroll-animation w_100">

                        <?php if ( ! empty( $page_about['title_one'] ) ) { ?>
							<div class="abt-title">
								<span></span>
								<p><?php echo esc_html( $page_about['title_one'] ); ?></p>
							</div>
						<?php } ?>

                        <?php if ( ! empty( $page_about['title_two'] ) ) { ?>
							<div class="abt-title">							
								<p><?php echo esc_html( $page_about['title_two'] ); ?></p>
								<span></span>
							</div>
						<?php } ?>

                        <?php if ( ! empty( $page_about['title_three'] ) ) { ?>
							<div class="abt-title">
								<span></span>
								<p><?php echo esc_html( $page_about['title_three'] ); ?></p>
							</div>
						<?php } ?>
                    </h2>                 

                </div>
            </div>
        </div>

        <!-- about info -->
        <div class="page-about-info w_100">
            <div class="page-about-info_wr all-width d-f-row-stre-s_b">
                <!-- left -->
                <div class="page-about-info_l animation-img-left _anim-items bg-light">
					<?php if ( ! empty( $page_about['img_one'] ) ) { ?>
						<img src="<?php echo esc_url( $page_about['img_one']['url'] ); ?>" alt="<?php echo esc_html( $page_about['img_one']['alt'] ); ?>">
					<?php } else { ?>
						<img src="<?php echo LA_FLEUR_THEME_URI; ?>/img/png/about-big.png" alt="about">
					<?php } ?>                  
                </div>
                <!-- right -->
                <div class="page-about-info_r">
                    <div class="page-about-info_r_description w_100">
						<?php if ( ! empty( $page_about['p_one'] ) ) { ?>
							<p><?php echo wp_kses_post( $page_about['p_one'] ); ?></p>
                        <?php } ?>
                        <?php if ( ! empty( $page_about['p_two'] ) ) { ?>
							<p><?php echo wp_kses_post( $page_about['p_two'] ); ?></p>
                        <?php } ?>
                        <?php if ( ! empty( $page_about['p_three'] ) ) { ?>
							<p><?php echo wp_kses_post( $page_about['p_three'] ); ?></p>
                        <?php } ?>
                    </div>

                    <div class="page-about-info_r_img d-f-row-stre-s_b w_100">
                        <div class="page-about-info_r_img_itm animation-img-left _anim-items bg-light">
                            <?php if ( ! empty( $page_about['img_two'] ) ) { ?>
								<img src="<?php echo esc_url( $page_about['img_two']['url'] ); ?>" alt="<?php echo esc_html( $page_about['img_two']['alt'] ); ?>">
							<?php } else { ?>
								<img src="<?php echo LA_FLEUR_THEME_URI; ?>/img/png/about-1.png" alt="img-1">
							<?php } ?>                          
                        </div>

                        <div class="page-about-info_r_img_itm animation-img-right _anim-items bg-light">
                            <?php if ( ! empty( $page_about['img_three'] ) ) { ?>
								<img src="<?php echo esc_url( $page_about['img_three']['url'] ); ?>" alt="<?php echo esc_html( $page_about['img_three']['alt'] ); ?>">
							<?php } else { ?>
								<img src="<?php echo LA_FLEUR_THEME_URI; ?>/img/png/about-2.png" alt="img-2">
							<?php } ?>
                        </div>
                    </div>

                    <div class="page-about-info_r_description page-about-info_r_description_btm w_100">
                        <?php if ( ! empty( $page_about['p_four'] ) ) { ?>
							<p><?php echo wp_kses_post( $page_about['p_four'] ); ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        
<?php
get_footer();
        
