<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package LaFleur
 */

if ( ! function_exists( 'la_fleur_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function la_fleur_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'la-fleur' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'la_fleur_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function la_fleur_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'la-fleur' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'la_fleur_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function la_fleur_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'la-fleur' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'la-fleur' ) . '</span>', $categories_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'la-fleur' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'la-fleur' ) . '</span>', $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'la-fleur' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'la-fleur' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'la_fleur_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function la_fleur_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

			<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
				<?php
					the_post_thumbnail(
						'post-thumbnail',
						array(
							'alt' => the_title_attribute(
								array(
									'echo' => false,
								)
							),
						)
					);
				?>
			</a>

			<?php
		endif; // End is_singular().
	}
endif;

/**
 * Displaying menus of 'primary-menu' and 'footer-menu'
 */
 
function la_fleur_get_primary_menu() {
	$menu_name = 'primary-menu';
	$locations = get_nav_menu_locations();

	if ( $locations && isset( $locations[ $menu_name ] ) ) {
		$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

		$menu_items = wp_get_nav_menu_items( $menu );
		
		$menu = array();
		foreach ( $menu_items as $item ) {
			if ( empty( $item->menu_item_parent ) ) {
				$menu[$item->ID] = array();
				$menu[$item->ID]['ID'] = $item->ID;
				$menu[$item->ID]['title'] = $item->title;
				$menu[$item->ID]['url'] = $item->url;
				$menu[$item->ID]['children'] = array();
			}
		}
		foreach ( $menu as $m ) {
			foreach ( $menu_items as $item ) {
				if ( $m['ID'] == $item->menu_item_parent ) {
					$submenu = array();
					$submenu['ID'] = $item->ID;
            		$submenu['title'] = $item->title;
            		$submenu['url'] = $item->url;
            		$submenu['children'] = array();
            		$menu[$m['ID']]['children'][$item->ID] = $submenu;
            		
            		foreach ( $menu_items as $i ) {
						if ( $item->ID == $i->menu_item_parent ) {
							$subsubmenu = array();
							$subsubmenu['ID'] = $i->ID;
							$subsubmenu['title'] = $i->title;
							$subsubmenu['url'] = $i->url;
							$menu[$m['ID']]['children'][$item->ID]['children'][$i->ID] = $subsubmenu;
						}
					}
				}
			}
		}
		
		$menu_list = '<ul class="d-f-row-c-f_s" id="menu-'. $menu_name .'">';

		foreach ( $menu as $key => $one_level ) {
			
			if ( ! empty( $one_level['children'] ) ) {
				$menu_list .= '<li>';// one level
				
					$menu_list .= '<button id="header_catalog" class="header_catalog d-f-row-c-f_s">';
						if ( $one_level['url'] == site_url() . $_SERVER['REQUEST_URI'] ) { $active_page = 'active-page'; } else { $active_page = ''; }
						$menu_list .= '<p class='. $active_page .'>'. esc_html( $one_level['title'] ) .'</p>';

						$menu_list .= '<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">';
							$menu_list .= '<line y1="-0.5" x2="20.1147" y2="-0.5" transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 14.9336 0)" stroke="black" />';
							$menu_list .= '<line x1="0.863281" y1="2.27734" x2="0.863282" y2="14.1207" stroke="black" />';
							$menu_list .= '<line x1="12.4922" y1="14.6211" x2="0.364043" y2="14.6211" stroke="black" />';
						$menu_list .= '</svg>';
					$menu_list .= '</button>';
					
					$menu_list .= '<ul id="show_menu-catalog" class="show_menu-catalog">';						
										
						$menu_list .= '<li>';// two level
							$menu_list .= '<div class="show_menu-catalog_wrapper d-f-column-f_c-f_s">';
								$menu_list .= '<div class="show_menu-catalog_container all-width d-f-column-f_s-f_s">';
									
									$count_two_level = 1;
									foreach ( $one_level['children'] as $two_level ) {
										// item 1
										$menu_list .= '<div class="show_menu-catalog_item d-f-column-f_s-f_s">';
											// main block
											$menu_list .= '<div class="show_menu-catalog_item_btn d-f-row-c-s_b">';
												// count + title
												$menu_list .= '<div class="show_menu-catalog_item_btn_title d-f-row-c-f_s">';
													if ( $count_two_level < 10 ) {
														$menu_list .= '<p>0'. $count_two_level .'</p>';
													} else {
														$menu_list .= '<p>'. $count_two_level .'</p>';
													}
													$menu_list .= '<h3>'. esc_html( $two_level['title'] ) .'</h3>';// <a href="'. esc_url( $two_level['url'] ) .'"></a>
												$menu_list .= '</div>';

												// arrow
												$menu_list .= '<div class="show_menu-catalog_item_btn_arrow">';
													$menu_list .= '<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">';
														$menu_list .= '<line y1="-0.5" x2="20.1147" y2="-0.5" transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 14.9336 0)" stroke="black" />';
														$menu_list .= '<line x1="0.863281" y1="2.27734" x2="0.863282" y2="14.1207" stroke="black" />';
														$menu_list .= '<line x1="12.4922" y1="14.6211" x2="0.364043" y2="14.6211" stroke="black" />';
													$menu_list .= '</svg>';
												$menu_list .= '</div>';
											$menu_list .= '</div>';
											
											// show block
											if ( ! empty( $two_level['children']) ) {
												$menu_list .= '<div class="show_menu-catalog_item_description d-f-row-f_s-s_b">';
													$menu_list .= '<div class="section_menu-catalog_link">';
														$menu_list .= '<ul class="d-f-row-stre-f_s">';
															
															foreach ( $two_level['children'] as $three_level ) {
																$post_ID = url_to_postid( $three_level['url'] );
																if( has_post_thumbnail( $post_ID ) ){
																	$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id( $post_ID ) );
																} else {
																	$feat_image_url = '';
																}
																$menu_list .= '<li>';// $three_level
																	$menu_list .= '<a href="'. esc_url( $two_level['url'] ) .'" data-menu-src="'. $feat_image_url .'" class="link_with_img_in_sub">'. esc_html( $three_level['title'] ) .'</a>';
																$menu_list .= '</li>';										
															}
															
														$menu_list .= '</ul>';
													$menu_list .= '</div>';
													$menu_list .= '<div id="section_menu-catalog_img_for-link" class="section_menu-catalog_img_for-link">';											
														$menu_list .= '<img src="'. LA_FLEUR_THEME_URI .'/img/png/menu-item-1.png" alt="img">';
													$menu_list .= '</div>';
												$menu_list .= '</div>';
											} // here maybe need 'else'
										$menu_list .= '</div>';
										$count_two_level++;									
									}
									
								$menu_list .= '</div>';
							$menu_list .= '</div>';		
						$menu_list .= '</li>';				
						
					$menu_list .= '</ul>';//</div>
				$menu_list .= '</li>';
			} else {
				if ( $one_level['url'] == site_url() . $_SERVER['REQUEST_URI'] ) { $active_page = 'active-page'; } else { $active_page = ''; }
				$menu_list .= '<li><a class="'. $active_page .'" href="'. esc_url( $one_level['url'] ) .'">'. esc_html( $one_level['title'] ) .'</a></li>';
			}			
		}

		$menu_list .= '</ul>';
	} else {
		$menu_list = '<ul class="d-f-row-c-f_s"><li>'. __( 'Primary menu is not defined.' ) .'</li></ul>';
	}
	return $menu_list;
}

function la_fleur_get_primary_mobil_menu( $part_menu ) {
	$menu_name = 'primary-menu';
	$locations = get_nav_menu_locations();

	if ( $locations && isset( $locations[ $menu_name ] ) ) {
		$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

		$menu_items = wp_get_nav_menu_items( $menu );
		
		$menu = array();
		foreach ( $menu_items as $item ) {
			if ( empty( $item->menu_item_parent ) ) {
				$menu[$item->ID] = array();
				$menu[$item->ID]['ID'] = $item->ID;
				$menu[$item->ID]['title'] = $item->title;
				$menu[$item->ID]['url'] = $item->url;
				$menu[$item->ID]['children'] = array();
			}
		}
		foreach ( $menu as $m ) {
			foreach ( $menu_items as $item ) {
				if ( $m['ID'] == $item->menu_item_parent ) {
					$submenu = array();
					$submenu['ID'] = $item->ID;
            		$submenu['title'] = $item->title;
            		$submenu['url'] = $item->url;
            		$submenu['children'] = array();
            		$menu[$m['ID']]['children'][$item->ID] = $submenu;
            		
            		foreach ( $menu_items as $i ) {
						if ( $item->ID == $i->menu_item_parent ) {
							$subsubmenu = array();
							$subsubmenu['ID'] = $i->ID;
							$subsubmenu['title'] = $i->title;
							$subsubmenu['url'] = $i->url;
							$menu[$m['ID']]['children'][$item->ID]['children'][$i->ID] = $subsubmenu;
						}
					}
				}
			}
		}

		$check_catalog = true;
		// get first level of menu
		if ( $part_menu == 'first-level' ) {
			$menu_list = '<ul class="" id="menu-' . $menu_name . '-first-level">';
			foreach ( $menu as $key => $one_level ) {
				if ( ! empty( $one_level['children'] ) ) {
					$menu_list .= '<li><button id="btn-for-show-menu-catalog">'. esc_html( $one_level['title'] ) .'</button>';
				} else {
					$menu_list .= '<li><a href="' . esc_url( $one_level['url'] ) . '">' . esc_html( $one_level['title'] ) . '</a></li>';
				}
				$check_catalog = false;
			}
			$menu_list .= '</ul>';
		}
		// get secons and third levels of menu
		if ( $part_menu == 'catalog' ) {
				
			foreach ( $menu as $key => $one_level ) {			
				
				if ( $one_level['url'] == esc_url( home_url( '/catalog/' ) ) ) { // $one_level['title'] == 'Каталог' || $one_level['title'] == 'Catalog'
					$menu_list = '';
					$menu_list .= '<button id="btn-back-to-mobile-menu">';
						$menu_list .= '<svg width="22" height="14" viewBox="0 0 22 14" fill="none" xmlns="http://www.w3.org/2000/svg">';
							$menu_list .= '<line x1="21.9756" y1="7.21973" x2="1.07314" y2="7.21973" stroke="black" />';
							$menu_list .= '<line x1="7.35738" y1="13.3536" x2="0.939735" y2="6.9359" stroke="black" />';
							$menu_list .= '<line x1="0.939415" y1="7.59957" x2="7.35707" y2="1.18192" stroke="black" />';
						$menu_list .= '</svg>';
						$menu_list .= esc_html( $one_level['title'] );
					$menu_list .= '</button>';
					
					if ( ! empty( $one_level['children'] ) ) {
						$menu_list .= '<div class="this-section-mobile-sub-menu this-section-mobile-sub-menu-items" id="menu-' . $menu_name . '-catalog">';				
							$count_two_level = 1;
							foreach ( $one_level['children'] as $two_level ) {
								// item 1
								$menu_list .= '<div class="section-mobile-sub-menu">';
									$menu_list .= '<button class="btn-for-section-mobile-sub-menu">';
										$menu_list .= '<div class="section-mobile-sub-menu_title">';
											$menu_list .= '<h4>'. esc_html( $two_level['title'] ) .'</h4>';
										$menu_list .= '</div>';

										$menu_list .= '<div class="section-mobile-sub-menu_icon">';
											if ( $count_two_level < 10 ) {
													$menu_list .= '<p>0'. $count_two_level .'</p>';
												} else {
													$menu_list .= '<p>'. $count_two_level .'</p>';
												}

											$menu_list .= '<svg width="23" height="24" viewBox="0 0 23 24" fill="none" xmlns="http://www.w3.org/2000/svg">';
												$menu_list .= '<line x1="22.2784" y1="1.3057" x2="1.14666" y2="22.4374" stroke="black" />';
												$menu_list .= '<line x1="1.03076" y1="4.37598" x2="1.03076" y2="22.1847" stroke="black" />';
												$menu_list .= '<line x1="18.3394" y1="22.6846" x2="0.530611" y2="22.6846" stroke="black" />';
											$menu_list .= '</svg>';

										$menu_list .= '</div>';
									$menu_list .= '</button>';
									
									if ( ! empty( $two_level['children'] ) ) {	
										$menu_list .= '<div class="this-mobile-sub-menu_show">';
											$menu_list .= '<ul>';
											
												foreach ( $two_level['children'] as $three_level ) {
													$menu_list .= '<li>';// $three_level
														$menu_list .= '<a href="'. esc_url( $two_level['url'] ) .'">'. esc_html( $three_level['title'] ) .'</a>';
													$menu_list .= '</li>';
												}
												
											$menu_list .= '</ul>';
										$menu_list .= '</div>';
									
									}
								$menu_list .= '</div>';
								$count_two_level++;
							}
						$menu_list .= '</div>';
					}
				}
			}
		}
		
	} else {
		$menu_list = '<ul class="d-f-row-c-f_s"><li>'. __( 'Primary menu is not defined.' ) .'</li></ul>';
	}
	return $menu_list;
}

function la_fleur_get_footer_menu() {
	$menu_name = 'footer-menu';
	$locations = get_nav_menu_locations();

	if ( $locations && isset($locations[ $menu_name ]) ) {
		$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

		$menu_items = wp_get_nav_menu_items( $menu );

		$menu_list = '<ul class="footer-menu f-s-btn" id="menu-' . $menu_name . '">';

		foreach ( (array) $menu_items as $key => $menu_item ) {
			$menu_list .= '<li><a href="' . esc_url( $menu_item->url ) . '">' . esc_html( $menu_item->title ) . '</a></li>';
		}

		$menu_list .= '</ul>';
	} else {
		$menu_list = '<ul class="footer-menu f-s-btn"><li>' . __( 'Footer menu is not defined.' ) . '</li></ul>';
	}
	return $menu_list;
}
