<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package LaFleur
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function la_fleur_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'la_fleur_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function la_fleur_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'la_fleur_pingback_header' );

// Language switcher
function la_fleur_language_switcher( $mobil = false ) {
	global $q_config;
	if ( is_404() ) {
        $url = get_option( 'home' );
    } else {
        $url = '';
    }	
	?>
	<div class="section-filter-itm section-filter-itm_lang" id="this-filter-3">
		<button class="shop-filter-btn mobile-lang_dropdown-this d-f-row-c-s_b">
			<p><?php echo $q_config['language']; ?></p>
			<?php if ( $mobil ) { ?>
				<svg width="6" height="4" viewBox="0 0 6 4" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M3.31738 3.89111L0.719306 0.891113L5.91546 0.891114L3.31738 3.89111Z"
						fill="#1A1919" />
				</svg>
			<?php } else { ?>	
				<svg width="11" height="11" viewBox="0 0 11 11" fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<line y1="-0.5" x2="11.9493" y2="-0.5"
						transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 9.56641 0.805664)"
						stroke="black" />
					<line x1="1.41016" y1="2.1582" x2="1.41016" y2="9.19388" stroke="black" />
					<line x1="8.11523" y1="9.69434" x2="0.910393" y2="9.69433" stroke="black" />
				</svg>
			<?php } ?>	
		</button>

		<div class="section-filter-itm_show">
			<div class="section-filter-itm_show_wr">
				<form>
					<ul>
						<?php
						foreach ( qtranxf_getSortedLanguages() as $language ) {
							if ( $language == $q_config['language'] ) {
								continue;
							} else {
								echo '<li><a href="' . qtranxf_convertURL( $url, $language, false, true ) . '">'. $language .'</a></li>';
							}
						}	
						?>
					</ul>
				</form>
			</div>
		</div>
	</div>
	<?php
}

// Currency switcher
function la_fleur_currency_switcher( $mobil = false ) {
	// alg_currency_select_link_list( $atts = array() )
	$function_currencies = alg_get_enabled_currencies();
	$currencies          = get_woocommerce_currencies();
	$selected_currency   = alg_get_current_currency_code();
	$links               = '';
	$first_link          = '';
	foreach ( $function_currencies as $currency_code ) {
		if ( isset( $currencies[ $currency_code ] ) ) {
			$the_text = alg_format_currency_switcher( $currencies[ $currency_code ], $currency_code, false );
			$the_symbol = get_woocommerce_currency_symbol( $currency_code );
			if ( $currency_code != $selected_currency ) {
				$the_link = '<li><a id="alg_currency_' . $currency_code . '" href="' . add_query_arg( 'alg_currency', $currency_code ) . '">' . $the_symbol . '</a></li>';
				$links .= $the_link;
			} else {
				if ( $mobil ) {
					$first_link .= '<button class="shop-filter-btn mobile-lang_dropdown-this d-f-row-c-s_b">';
				} else {
					$first_link .= '<button class="shop-filter-btn d-f-row-c-s_b">';
				}				
				$first_link .= '<p>'. $the_symbol .'</p>';
				$first_link .= '<svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">';
				$first_link .= '<line y1="-0.5" x2="11.9493" y2="-0.5" transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 9.56641 0.805664)" stroke="black" />';
				$first_link .= '<line x1="1.41016" y1="2.1582" x2="1.41016" y2="9.19388" stroke="black" />';
				$first_link .= '<line x1="8.11523" y1="9.69434" x2="0.910393" y2="9.69433" stroke="black" />';
				$first_link .= '</svg>';
				$first_link .= '</button>';
			}
		}
	}

	$html = '';
	$html .= $first_link;
	$html .= '<div class="section-filter-itm_show"><div class="section-filter-itm_show_wr"><form><ul>';
	$html .= $links;
	$html .= '</ul></form></div></div>';
	return $html;
}

// Get page url or title
function la_fleur_get_page( $path, $part ) {
	$page = get_page_by_path( $path );
	if ( $part == 'title' && $page->post_title ) {
		return esc_html( $page->post_title );
	} elseif ( $part == 'url' && $page->ID ) {
		return esc_url( get_permalink( $page->ID ) );
	} else {
		return '';
	}	
}

// Get end time for sale
function la_fleur_get_end_time( $seconds, $get_seconds = false ) {
	date_default_timezone_set( 'Europe/Kiev' );
	$one = array(1, 21, 31, 41, 51, 61, 71, 81, 91);
	$two = array(2, 3, 4, 22, 32, 42, 52, 62, 72, 82, 92, 23, 33, 43, 53, 63, 73, 83, 93, 24, 34, 44, 54, 64, 74, 84, 94);
	if( $seconds < time() ) return '';
	$seconds = $seconds - time();
	if ( $seconds > 86400 ) :
		$days = floor( $seconds / 86400 );
		$seconds = $seconds - ( $days * 86400 );
	endif;

	$out = '';	
	if( ! empty( $days ) ) {
		$out .= $days;
		if( in_array( $days, $one ) ){
			$out .= __( ' день ' , 'la-fleur' );
		} elseif( in_array( $days, $two ) ){
			$out .= __( ' дня ' , 'la-fleur' );
		} else {
			$out .= __( ' дней ' , 'la-fleur' );
		}
	};
	if ( $get_seconds == true ) {
		$hours = date( " H:i:s", $seconds );
		$out .= $hours;
	}			
	return $out;
}

//  Page sales - load more
function la_fleur_callback_page_sales_loadmore() {
 
	$args = unserialize( stripslashes( $_POST['query'] ) );
	$args['paged'] = $_POST['page'] + 1;
	
	query_posts( $args );

	if( have_posts() ) {
		while ( have_posts() ) {
			the_post();
 
			$sale_content = get_field( 'singl_sale_content' );
			$singl_sale_end = get_field( 'singl_sale_end' );
			?>
			<div class="page-sales-other_item">
				<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="d-f-column-f_s-f_s">
					<div class="page-sales-other_item_img w_100 animation-img-bottom bg-light _anim-items">
						<?php
						if ( $sale_content['image'] ) {
							echo '<img src="'. esc_url( $sale_content['image']['url'] ) .'" alt="'. esc_html( $sale_content['image']['alt'] ) .'">';
						} else {
							echo '<img src="'. LA_FLEUR_THEME_URI .'/img/png/sale-item.png" alt="sale">';
						}
						?>
					</div>
				</a>
				<div class="page-sales-other_item_info d-f-row-c-s_b w_100">
					<div class="page-sales-other_item_info_l d-f-row-c-f_s">
						<?php
						if ( ! empty( $singl_sale_end ) && $singl_sale_end > time() ) {
							echo '<h4>'. la_fleur_get_end_time( $singl_sale_end ) .'</h4>';
							echo '<p>'. __( 'до конца акции', 'la-fleur' ) .'</p>';
						}
						?>
					</div>

					<div class="page-sales-other_item_info_r d-f-row-c-f_end">
						<a href="<?php echo esc_url( get_the_permalink() ); ?>"><?php _e( 'Читать больше', 'la-fleur' ); ?></a>
					</div>
				</div>
			</div>
			<?php
		}
	}
	die();
}
 
add_action('wp_ajax_page_sales_loadmore', 'la_fleur_callback_page_sales_loadmore');
add_action('wp_ajax_nopriv_page_sales_loadmore', 'la_fleur_callback_page_sales_loadmore');

// Products (shop) - load more
function la_fleur_callback_products_loadmore(){
 
	$args = unserialize( stripslashes( $_POST['query'] ) );
	$args['paged'] = $_POST['page'] + 1; // следующая страница
	$args['post_status'] = 'publish';
 
	query_posts( $args );
	if( have_posts() ) :
 
		// запускаем цикл
		while( have_posts() ): the_post();
 
			echo '<div class="shop-product-item">';
			wc_get_template_part( 'content', 'product' );
			echo '</div>';
		endwhile;
 
	endif;
	die();
}
 
 
add_action('wp_ajax_products_loadmore', 'la_fleur_callback_products_loadmore');
add_action('wp_ajax_nopriv_products_loadmore', 'la_fleur_callback_products_loadmore');

// Mini cart
add_filter( 'woocommerce_add_to_cart_fragments', 'la_fleur_mini_cart_count_fragments', 30, 1 );

function la_fleur_mini_cart_count_fragments( $fragments ) {
	ob_start();
	echo '<p class="la_fleur_mini_cart_count">'. WC()->cart->cart_contents_count .'</p>';
	//echo la_fleur_mini_cart();
	$fragments['p.la_fleur_mini_cart_count'] = ob_get_clean();
    
    return $fragments;
}

add_filter( 'woocommerce_add_to_cart_fragments', 'la_fleur_mini_cart_fragments', 30, 1 );

function la_fleur_mini_cart_fragments( $fragments ) {
	ob_start();
	echo la_fleur_mini_cart();
	$fragments['div#iv_mini_cart'] = ob_get_clean();
    
    return $fragments;
}

function la_fleur_mini_cart() { // Display
	global $woocommerce;
	//$items_n = WC()->cart->get_cart();
	
	if ( ! WC()->cart->is_empty() ) { // woocommerce
		$custom_cart_total = 0;
		?>
		<!-- this blok show when shop card has products start -->
		<div id="iv_mini_cart">
			<div id="modal-shop-card-with-product" class="block-product-card_with-product">
				<div id="container_mini_cart" class="block-product-card_with-product__wrapper d-f-column-f_c-f_s">	
					<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
						<div id="modal-shop-card-basket" class="modal-shop-card-basket d-f-column-f_s-f_s">
							<?php
							foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
								$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
								$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
								$product_brand = get_post_meta( $cart_item['product_id'], 'product_brand', true );

								if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
									$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
									?>
									<div class="modal-shop-card-item d-f-row-stre-s_b">
										<div class="modal-shop-card-item__img">
											<?php
											$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image( 'full' ), $cart_item, $cart_item_key );

											if ( ! $product_permalink ) {
												echo $thumbnail; // PHPCS: XSS ok.
											} else {
												printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
											}
											?>
										</div>

										<div class="modal-shop-card-item__info">
											<div class="modal-shop-card-item__info_top d-f-row-stre-s_b">
												<div class="modal-shop-card-item__info_left">
													<a href="<?php echo esc_url( $product_permalink ); ?>">

														<!-- name  -->
														<div class="modal-shop-card-item__info_name">
															<?php  $product_name = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ); ?>
															<h4><?php echo wp_kses_post( $product_name ); ?></h4>
														</div>

														<!-- brand -->
														<div class="modal-shop-card-item__info_brand-volume d-f-row-stre-f_s">
															<?php
																if ( $product_brand ) {
																	echo '<p>';
																	echo esc_html( $product_brand );
																	echo  '</p>';
																}
															?>
																													
															<?php
																$pa_volume = $_product->get_attribute( 'pa_volume' );
																if ( ! empty( $pa_volume ) ) {
																	$pa_volume_arr = explode( ',', $pa_volume );
																	echo '<div class="modal-shop-card-item__info_line"></div>';
																	echo  '<p>';
																	echo $pa_volume_arr[0] . __( ' мл', 'la-fleur' );
																	echo  '</p>';
																}
															?>															
														</div>
													</a>
												</div>

												<div class="modal-shop-card-item__info_right">
													<?php
													$remove_link_svg = '<svg width="10" height="14" viewBox="0 0 10 14" fill="none" xmlns="http://www.w3.org/2000/svg">';
													$remove_link_svg .= '<path d="M3.26303 5.21762C3.36325 5.21762 3.44873 5.30106 3.44873 5.40536V11.0376C3.44873 11.139 3.3662 11.2254 3.26303 11.2254C3.15987 11.2254 3.07734 11.142 3.07734 11.0376V5.40536C3.07734 5.30106 3.15987 5.21762 3.26303 5.21762Z" fill="black" />';
													$remove_link_svg .= '<path d="M6.73911 5.21762C6.83933 5.21762 6.9248 5.30106 6.9248 5.40536V11.0377C6.9248 11.139 6.84227 11.2254 6.73911 11.2254C6.63594 11.2254 6.55341 11.142 6.55341 11.0377V5.40536C6.55341 5.30106 6.63594 5.21762 6.73911 5.21762Z" fill="black" />';
													$remove_link_svg .= '<path d="M0.00835896 3.05106C0.073205 2.54147 0.506495 2.15704 1.01642 2.15704H2.63462V1.67428C2.63168 1.39117 2.74663 1.11701 2.94117 0.917346C3.13865 0.717683 3.40393 0.607422 3.681 0.607422H3.68984H6.31906C6.59907 0.607422 6.8614 0.717683 7.05889 0.917346C7.25343 1.11403 7.36838 1.39117 7.36543 1.6713V2.15704H8.98659C9.49356 2.15704 9.92685 2.54147 9.9917 3.05106C10.0565 3.56064 9.73526 4.04341 9.24597 4.17751L9.16344 4.19837V11.6276C9.16344 12.0269 9.01606 12.4144 8.76257 12.6855C8.51203 12.9567 8.15538 13.1117 7.78988 13.1117H2.21312C1.84763 13.1117 1.49097 12.9567 1.24043 12.6855C0.983996 12.4144 0.839567 12.0269 0.839567 11.6276V4.19837L0.757035 4.17751C0.261847 4.04341 -0.0564871 3.56064 0.00835896 3.05106ZM2.21018 12.7362H7.78988C8.35286 12.7362 8.79499 12.2475 8.79499 11.6246V4.19837H1.20506V11.6276C1.20506 12.2475 1.64719 12.7362 2.21018 12.7362ZM6.31906 0.979927H6.30727H3.681C3.5012 0.979927 3.33024 1.05145 3.2035 1.17959C3.07381 1.31071 3.00012 1.48951 3.00307 1.67428V2.15704H6.99699V1.6713C6.99994 1.48951 6.92625 1.30773 6.79656 1.17959C6.66981 1.05145 6.49886 0.979927 6.31906 0.979927ZM8.98659 2.52955H1.01642C0.662715 2.52955 0.373855 2.82159 0.373855 3.1792C0.373855 3.5368 0.662715 3.82885 1.01642 3.82885H8.98659C9.34029 3.82885 9.62915 3.5368 9.62915 3.1792C9.62915 2.82159 9.33734 2.52955 8.98659 2.52955Z" fill="black" />';
													$remove_link_svg .= '<path d="M5.00132 5.21762C5.10153 5.21762 5.18701 5.30106 5.18701 5.40536V11.0376C5.18701 11.139 5.10448 11.2254 5.00132 11.2254C4.89815 11.2254 4.81562 11.142 4.81562 11.0376V5.40536C4.81562 5.30106 4.89815 5.21762 5.00132 5.21762Z" fill="black" />';
													$remove_link_svg .= '</svg>';
													
													echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
														'woocommerce_cart_item_remove_link',
														sprintf(
															'<a href="%s" class="remove remove_from_cart_button" onclick="removeFromMiniCart();" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s">'. $remove_link_svg .'</a>',
															esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
															esc_attr__( 'Remove this item', 'woocommerce' ),
															esc_attr( $product_id ),
															esc_attr( $cart_item_key ),
															esc_attr( $_product->get_sku() )
														),
														$cart_item_key
													);
													?>
												</div>
											</div>

											<div class="modal-shop-card-item__info_bottom d-f-row-stre-s_b">
												<!-- count -->
												<div class="modal-shop-card-item__info_bottom_left">
													<button class="btn-modal-item-minus" onclick="changeCountProduct(this, event, 'minus');">-</button>

													<label>
														<input type="number" class="input-text qty text" step="1" id="this-product-count"  
														min="1" max="" name="cart[<?php echo $cart_item_key; ?>][qty]"
														value="<?php echo $cart_item['quantity']; ?>" 
														title="Кол-во" size="4" placeholder="" inputmode="numeric">											
														
														<input type="hidden" name="iv_product_key" value="<?php echo $cart_item_key; ?>">
													</label>
													<!--  -->
													<button class="btn-modal-item-plus" onclick="changeCountProduct(this, event, 'plus');">+</button>
													
												</div>

												<!-- price -->
												<div class="modal-shop-card-item__info_bottom_price">
													<?php								
													$row_price = $_product->get_price() * $cart_item['quantity'];
													$custom_cart_total += $row_price;
													?>
													<p><?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );// echo $price; ?></p>
												</div>
											</div>

										</div>
									</div>
							
								<?php
								}
							}
							?>
						</div>
														
						<?php wp_nonce_field( 'iv-ajax-nonce', 'security' ); ?>
										
					</form>
					<input type="hidden" id="custom_cart_count" value="<?php echo WC()->cart->cart_contents_count; ?>" >
					<!-- total sum -->
					<div class="modal-shop-card-total-sum">
						<div class="modal-shop-card-total-sum__wrapper d-f-row-stre-s_b">
							<p><?php _e( 'Общая сумма:', 'la-fleur' ); ?></p>
							<h4><?php echo $custom_cart_total . get_woocommerce_currency_symbol(); ?></h4>
						</div>
					</div>
					<!-- other link -->
					<div class="modal-shop-card-total-other-link d-f-row-stre-s_b">
						<button class="btn-link-this-page d-f-row-c-c btn-shop-card-modal" onclick="closeMiniCart();">
							<?php _e( 'Продолжить покупки', 'la-fleur' ); ?>
						</button>

						<div class="all_link hover_effect-for link-shop-card-modal">
							<a href="<?php echo wc_get_cart_url(); ?>">
								<p><?php _e( 'заказать', 'la-fleur' ); ?></p>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	} else {
		?>
		<div id="iv_mini_cart">
			<div id="modal-shop-card-no-product" class="block-product-card_no-product">
				<div class="block-product-card_no-product__wrapper d-f-column-f_c-f_s">

					<!-- img -->
					<div class="modal-block-no-product__img d-f-row-c-c">
						<svg width="80" height="90" viewBox="0 0 80 90" fill="none"
							xmlns="http://www.w3.org/2000/svg">
							<g clip-path="url(#clip0)">
								<path
									d="M68.8236 20.837C67.7622 19.6639 66.3098 18.9935 64.6897 18.9935H56.3102V14.8596C56.3102 6.7036 49.4949 0 41.1713 0H29.0489C20.6694 0 13.91 6.7036 13.8541 14.9155V19.0494H5.47461C4.02216 19.0494 2.62558 19.608 1.56417 20.6136C0.558633 21.6191 0 22.9598 0 24.4123C0 24.6357 0 24.8592 0.0558633 25.0826L4.4132 61.3379C5.0277 66.4215 9.4409 70.2202 14.6362 70.2761H55.584C60.7793 70.2761 65.1925 66.4215 65.8628 61.3379L70.2202 25.0826C70.3878 23.5185 69.885 22.0101 68.8236 20.837ZM12.7927 33.2945V29.7751H16.3121V33.2945H12.7927ZM53.9081 33.2945V29.7751H57.4275V33.2945H53.9081ZM13.91 28.5462H12.1223C11.7872 28.5462 11.452 28.8255 11.452 29.1607V33.909C11.452 34.2442 11.7313 34.5235 12.1223 34.5235H16.9824C17.3176 34.5235 17.6528 34.2442 17.6528 33.909V29.1607C17.6528 28.8255 17.3735 28.5462 16.9824 28.5462H15.1948V20.2784H55.0254V28.5462H53.2377C52.9026 28.5462 52.5674 28.8255 52.5674 29.1607V33.909C52.5674 34.2442 52.8467 34.5235 53.2377 34.5235H58.0978C58.433 34.5235 58.7682 34.2442 58.7682 33.909V29.1607C58.7682 28.8255 58.4889 28.5462 58.0978 28.5462H56.3102V20.2784H64.6897C65.807 20.2784 66.8684 20.7253 67.6505 21.5074C68.4326 22.2895 68.8795 23.3509 68.8795 24.4123C68.8795 24.5799 68.8795 24.7474 68.8236 24.915L64.4663 61.1703C63.9076 65.6394 60.0531 68.9912 55.5281 68.9912H14.6362C10.1113 68.9912 6.25669 65.6394 5.69806 61.1703L1.34072 24.915C1.22899 23.8536 1.50831 22.7364 2.17867 21.8984C2.84903 21.0046 3.85457 20.446 4.97183 20.3342C5.13942 20.3342 5.30701 20.2784 5.47461 20.2784H13.8541V28.5462H13.91ZM29.0489 1.28486H41.1713C48.8245 1.28486 55.0254 7.42982 55.0254 14.9155V19.0494H15.1948V14.9155C15.1948 7.37396 21.4515 1.28486 29.0489 1.28486Z"
									fill="black" />
							</g>
							<circle cx="53.074" cy="62.8716" r="26.1284" fill="#FEFCF5" stroke="black"
								stroke-width="1.3" />
							<path
								d="M52.958 69.0229C51.986 69.0229 51.116 68.7649 50.348 68.2489C49.592 67.7329 48.998 66.9949 48.566 66.0349C48.146 65.0629 47.936 63.9229 47.936 62.6149C47.936 61.3069 48.146 60.1729 48.566 59.2129C48.998 58.2409 49.592 57.4969 50.348 56.9809C51.116 56.4649 51.986 56.2069 52.958 56.2069C53.93 56.2069 54.794 56.4649 55.55 56.9809C56.318 57.4969 56.918 58.2409 57.35 59.2129C57.782 60.1729 57.998 61.3069 57.998 62.6149C57.998 63.9229 57.782 65.0629 57.35 66.0349C56.918 66.9949 56.318 67.7329 55.55 68.2489C54.794 68.7649 53.93 69.0229 52.958 69.0229ZM52.958 67.8349C53.69 67.8349 54.332 67.6309 54.884 67.2229C55.448 66.8149 55.886 66.2209 56.198 65.4409C56.51 64.6609 56.666 63.7189 56.666 62.6149C56.666 61.5109 56.51 60.5689 56.198 59.7889C55.886 59.0089 55.448 58.4149 54.884 58.0069C54.332 57.5989 53.69 57.3949 52.958 57.3949C52.226 57.3949 51.578 57.5989 51.014 58.0069C50.462 58.4149 50.03 59.0089 49.718 59.7889C49.406 60.5689 49.25 61.5109 49.25 62.6149C49.25 63.7189 49.406 64.6609 49.718 65.4409C50.03 66.2209 50.462 66.8149 51.014 67.2229C51.578 67.6309 52.226 67.8349 52.958 67.8349Z"
								fill="black" />
							<defs>
								<clipPath id="clip0">
									<rect width="70.2202" height="70.2202" fill="white" />
								</clipPath>
							</defs>
						</svg>
					</div>

					<!-- link -->
					<div class="modal-product-card-link">
						<a href="<?php echo esc_url( home_url( '/catalog/' ) ); ?>" class="btn-link-this-page d-f-row-c-c">
							<?php _e( 'Вернуться в каталог?', 'la-fleur' ); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}

// iv_mini_cart_quanity_update
add_action( 'wp_ajax_iv_mini_cart_quanity_update', 'callback_iv_mini_cart_quanity_update' );
add_action( 'wp_ajax_nopriv_iv_mini_cart_quanity_update', 'callback_iv_mini_cart_quanity_update' );

function callback_iv_mini_cart_quanity_update() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' ); 
	
	$number = $_POST['number'];
	$key = $_POST['product_key'];
	
	WC()->cart->set_quantity( $key, $number );

	echo la_fleur_mini_cart();
	
	wp_die();
}

// iv_cart
function la_fleur_cart() { // Display
	global $woocommerce;
	
	if ( ! WC()->cart->is_empty() ) {
		$custom_cart_total = 0;
		?>
		<!-- this blok show when shop card has products start -->
		<div id="iv_cart">
			<div id="order-show-you-pr" class="order-show-you-pr">
					
				<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
					<?php
					$custom_cart_total = 0;
					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
						$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
						$product_brand = get_post_meta( $cart_item['product_id'], 'product_brand', true );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
							?>
							<div class="order-you-pr d-f-row-stre-s_b">
								<div class="order-you-pr__img">
									<?php
									$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image( 'full' ), $cart_item, $cart_item_key );

									if ( ! $product_permalink ) {
										echo $thumbnail; // PHPCS: XSS ok.
									} else {
										printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
									}
									?>
								</div>

								<div class="order-you-pr__info">
									<div class="order-you-pr__info_top d-f-row-stre-s_b">
										<div class="order-you-pr__info_left order-you-pr__info_left">
											<a href="<?php echo esc_url( $product_permalink ); ?>">

												<!-- name  -->
												<div class="order-you-pr__info_name">
													<?php  $product_name = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ); ?>
													<h4><?php echo wp_kses_post( $product_name ); ?></h4>
												</div>

												<!-- brand -->
												<div class="order-you-pr__info_brand-volume d-f-row-stre-f_s">
													<?php
														if ( $product_brand ) {
															echo '<p>';
															echo esc_html( $product_brand );
															echo  '</p>';
														}
													?>
																											
													<?php
														$pa_volume = $_product->get_attribute( 'pa_volume' );
														if ( ! empty( $pa_volume ) ) {
															$pa_volume_arr = explode( ',', $pa_volume );
															echo '<div class="order-you-pr__info_line"></div>';
															echo  '<p>';
															echo $pa_volume_arr[0] . __( ' мл', 'la-fleur' );
															echo  '</p>';
														}
													?>															
												</div>
											</a>
										</div>

										<div class="order-you-pr__info_right">
											<?php
											$remove_link_svg = '<svg width="10" height="14" viewBox="0 0 10 14" fill="none" xmlns="http://www.w3.org/2000/svg">';
											$remove_link_svg .= '<path d="M3.26303 5.21762C3.36325 5.21762 3.44873 5.30106 3.44873 5.40536V11.0376C3.44873 11.139 3.3662 11.2254 3.26303 11.2254C3.15987 11.2254 3.07734 11.142 3.07734 11.0376V5.40536C3.07734 5.30106 3.15987 5.21762 3.26303 5.21762Z" fill="black" />';
											$remove_link_svg .= '<path d="M6.73911 5.21762C6.83933 5.21762 6.9248 5.30106 6.9248 5.40536V11.0377C6.9248 11.139 6.84227 11.2254 6.73911 11.2254C6.63594 11.2254 6.55341 11.142 6.55341 11.0377V5.40536C6.55341 5.30106 6.63594 5.21762 6.73911 5.21762Z" fill="black" />';
											$remove_link_svg .= '<path d="M0.00835896 3.05106C0.073205 2.54147 0.506495 2.15704 1.01642 2.15704H2.63462V1.67428C2.63168 1.39117 2.74663 1.11701 2.94117 0.917346C3.13865 0.717683 3.40393 0.607422 3.681 0.607422H3.68984H6.31906C6.59907 0.607422 6.8614 0.717683 7.05889 0.917346C7.25343 1.11403 7.36838 1.39117 7.36543 1.6713V2.15704H8.98659C9.49356 2.15704 9.92685 2.54147 9.9917 3.05106C10.0565 3.56064 9.73526 4.04341 9.24597 4.17751L9.16344 4.19837V11.6276C9.16344 12.0269 9.01606 12.4144 8.76257 12.6855C8.51203 12.9567 8.15538 13.1117 7.78988 13.1117H2.21312C1.84763 13.1117 1.49097 12.9567 1.24043 12.6855C0.983996 12.4144 0.839567 12.0269 0.839567 11.6276V4.19837L0.757035 4.17751C0.261847 4.04341 -0.0564871 3.56064 0.00835896 3.05106ZM2.21018 12.7362H7.78988C8.35286 12.7362 8.79499 12.2475 8.79499 11.6246V4.19837H1.20506V11.6276C1.20506 12.2475 1.64719 12.7362 2.21018 12.7362ZM6.31906 0.979927H6.30727H3.681C3.5012 0.979927 3.33024 1.05145 3.2035 1.17959C3.07381 1.31071 3.00012 1.48951 3.00307 1.67428V2.15704H6.99699V1.6713C6.99994 1.48951 6.92625 1.30773 6.79656 1.17959C6.66981 1.05145 6.49886 0.979927 6.31906 0.979927ZM8.98659 2.52955H1.01642C0.662715 2.52955 0.373855 2.82159 0.373855 3.1792C0.373855 3.5368 0.662715 3.82885 1.01642 3.82885H8.98659C9.34029 3.82885 9.62915 3.5368 9.62915 3.1792C9.62915 2.82159 9.33734 2.52955 8.98659 2.52955Z" fill="black" />';
											$remove_link_svg .= '<path d="M5.00132 5.21762C5.10153 5.21762 5.18701 5.30106 5.18701 5.40536V11.0376C5.18701 11.139 5.10448 11.2254 5.00132 11.2254C4.89815 11.2254 4.81562 11.142 4.81562 11.0376V5.40536C4.81562 5.30106 4.89815 5.21762 5.00132 5.21762Z" fill="black" />';
											$remove_link_svg .= '</svg>';
											
											echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
												'woocommerce_cart_item_remove_link',
												sprintf(
													'<a href="%s" class="remove remove_from_cart_button" onclick="removeFromCart();" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s">'. $remove_link_svg .'</a>',
													esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
													esc_attr__( 'Remove this item', 'woocommerce' ),
													esc_attr( $product_id ),
													esc_attr( $cart_item_key ),
													esc_attr( $_product->get_sku() )
												),
												$cart_item_key
											);
											?>
										</div>
									</div>

									<div class="order-you-pr__info_bottom d-f-row-stre-s_b">
										<!-- count -->
										<div class="order-you-pr_info_bottom_left">
											<button class="" onclick="changeCountProductCart(this, event, 'minus');">-</button>

											<label>
												<input type="number" class="input-text qty text" step="1" id="this-product-count"  
												min="1" max="" name="cart[<?php echo $cart_item_key; ?>][qty]"
												value="<?php echo $cart_item['quantity']; ?>" 
												title="Кол-во" size="4" placeholder="" inputmode="numeric">											
												
												<input type="hidden" name="iv_product_key" value="<?php echo $cart_item_key; ?>">
											</label>
											<!--  -->
											<button class="" onclick="changeCountProductCart(this, event, 'plus');">+</button>
											
										</div>

										<!-- price -->
										<div class="order-you-pr__info_bottom_price">
											<?php								
											$row_price = $_product->get_price() * $cart_item['quantity'];
											$custom_cart_total += $row_price;
											?>
											<p><?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );// echo $price; ?></p>
										</div>
									</div>

								</div>
							</div>
					
						<?php
						}
					}
					?>
													
					<?php wp_nonce_field( 'iv-ajax-nonce', 'security' ); ?>
									
				</form>
				<input type="hidden" id="cart_count" value="<?php echo WC()->cart->cart_contents_count; ?>" >	
			</div>

			<!-- total sum -->
			<div class="order-total-sum">
				<div class="order-total-sum__wrapper d-f-row-stre-s_b">
					<div class="order_total-summ">
						<p><?php _e( 'Общая сумма:', 'la-fleur' ); ?></p>

						<h4><span id="checkout_sum"><?php echo $custom_cart_total; ?></span><?php echo get_woocommerce_currency_symbol(); ?></h4>
					</div>
					
				</div>
			</div>
			
		</div>
		<?php
	} else {
		?>
		<div id="iv_cart">
			<div id="modal-shop-card-no-product" class="block-product-card_no-product">
				<div class="block-product-card_no-product__wrapper d-f-column-f_c-f_s">

					<input type="hidden" id="cart_count" value="<?php echo WC()->cart->cart_contents_count; ?>" >
					<!-- img -->
					<div class="modal-block-no-product__img d-f-row-c-c">
						<svg width="80" height="90" viewBox="0 0 80 90" fill="none"
							xmlns="http://www.w3.org/2000/svg">
							<g clip-path="url(#clip0)">
								<path
									d="M68.8236 20.837C67.7622 19.6639 66.3098 18.9935 64.6897 18.9935H56.3102V14.8596C56.3102 6.7036 49.4949 0 41.1713 0H29.0489C20.6694 0 13.91 6.7036 13.8541 14.9155V19.0494H5.47461C4.02216 19.0494 2.62558 19.608 1.56417 20.6136C0.558633 21.6191 0 22.9598 0 24.4123C0 24.6357 0 24.8592 0.0558633 25.0826L4.4132 61.3379C5.0277 66.4215 9.4409 70.2202 14.6362 70.2761H55.584C60.7793 70.2761 65.1925 66.4215 65.8628 61.3379L70.2202 25.0826C70.3878 23.5185 69.885 22.0101 68.8236 20.837ZM12.7927 33.2945V29.7751H16.3121V33.2945H12.7927ZM53.9081 33.2945V29.7751H57.4275V33.2945H53.9081ZM13.91 28.5462H12.1223C11.7872 28.5462 11.452 28.8255 11.452 29.1607V33.909C11.452 34.2442 11.7313 34.5235 12.1223 34.5235H16.9824C17.3176 34.5235 17.6528 34.2442 17.6528 33.909V29.1607C17.6528 28.8255 17.3735 28.5462 16.9824 28.5462H15.1948V20.2784H55.0254V28.5462H53.2377C52.9026 28.5462 52.5674 28.8255 52.5674 29.1607V33.909C52.5674 34.2442 52.8467 34.5235 53.2377 34.5235H58.0978C58.433 34.5235 58.7682 34.2442 58.7682 33.909V29.1607C58.7682 28.8255 58.4889 28.5462 58.0978 28.5462H56.3102V20.2784H64.6897C65.807 20.2784 66.8684 20.7253 67.6505 21.5074C68.4326 22.2895 68.8795 23.3509 68.8795 24.4123C68.8795 24.5799 68.8795 24.7474 68.8236 24.915L64.4663 61.1703C63.9076 65.6394 60.0531 68.9912 55.5281 68.9912H14.6362C10.1113 68.9912 6.25669 65.6394 5.69806 61.1703L1.34072 24.915C1.22899 23.8536 1.50831 22.7364 2.17867 21.8984C2.84903 21.0046 3.85457 20.446 4.97183 20.3342C5.13942 20.3342 5.30701 20.2784 5.47461 20.2784H13.8541V28.5462H13.91ZM29.0489 1.28486H41.1713C48.8245 1.28486 55.0254 7.42982 55.0254 14.9155V19.0494H15.1948V14.9155C15.1948 7.37396 21.4515 1.28486 29.0489 1.28486Z"
									fill="black" />
							</g>
							<circle cx="53.074" cy="62.8716" r="26.1284" fill="#FEFCF5" stroke="black"
								stroke-width="1.3" />
							<path
								d="M52.958 69.0229C51.986 69.0229 51.116 68.7649 50.348 68.2489C49.592 67.7329 48.998 66.9949 48.566 66.0349C48.146 65.0629 47.936 63.9229 47.936 62.6149C47.936 61.3069 48.146 60.1729 48.566 59.2129C48.998 58.2409 49.592 57.4969 50.348 56.9809C51.116 56.4649 51.986 56.2069 52.958 56.2069C53.93 56.2069 54.794 56.4649 55.55 56.9809C56.318 57.4969 56.918 58.2409 57.35 59.2129C57.782 60.1729 57.998 61.3069 57.998 62.6149C57.998 63.9229 57.782 65.0629 57.35 66.0349C56.918 66.9949 56.318 67.7329 55.55 68.2489C54.794 68.7649 53.93 69.0229 52.958 69.0229ZM52.958 67.8349C53.69 67.8349 54.332 67.6309 54.884 67.2229C55.448 66.8149 55.886 66.2209 56.198 65.4409C56.51 64.6609 56.666 63.7189 56.666 62.6149C56.666 61.5109 56.51 60.5689 56.198 59.7889C55.886 59.0089 55.448 58.4149 54.884 58.0069C54.332 57.5989 53.69 57.3949 52.958 57.3949C52.226 57.3949 51.578 57.5989 51.014 58.0069C50.462 58.4149 50.03 59.0089 49.718 59.7889C49.406 60.5689 49.25 61.5109 49.25 62.6149C49.25 63.7189 49.406 64.6609 49.718 65.4409C50.03 66.2209 50.462 66.8149 51.014 67.2229C51.578 67.6309 52.226 67.8349 52.958 67.8349Z"
								fill="black" />
							<defs>
								<clipPath id="clip0">
									<rect width="70.2202" height="70.2202" fill="white" />
								</clipPath>
							</defs>
						</svg>
					</div>

					<!-- link -->
					<div class="modal-product-card-link">
						<a href="<?php echo esc_url( home_url( '/catalog/' ) ); ?>" class="btn-link-this-page d-f-row-c-c">
							<?php _e( 'Вернуться в каталог?', 'la-fleur' ); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}

// iv_cart_quanity_update
add_action( 'wp_ajax_iv_cart_quanity_update', 'callback_iv_cart_quanity_update' );
add_action( 'wp_ajax_nopriv_iv_cart_quanity_update', 'callback_iv_cart_quanity_update' );

function callback_iv_cart_quanity_update() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' ); 
	
	$number = $_POST['number'];
	$key = $_POST['product_key'];
	
	WC()->cart->set_quantity( $key, $number );

	echo la_fleur_cart();
	
	wp_die();
}

// ajax_remove_from_cart
add_action( 'wp_ajax_ajax_remove_from_cart', 'callback_ajax_remove_from_cart' );
add_action( 'wp_ajax_nopriv_ajax_remove_from_cart', 'callback_ajax_remove_from_cart' );

function callback_ajax_remove_from_cart() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' );

	echo la_fleur_cart();
	
	wp_die();
}


// iv_checkout
function la_fleur_checkout() { // Display
	global $woocommerce;
	
	if ( ! WC()->cart->is_empty() ) { // woocommerce
		$custom_cart_total = 0;
		?>
		<!-- this blok show when shop card has products start -->
		<div id="iv_checkout">
			<div id="order-show-you-pr" class="order-show-you-pr">
					
				<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" method="post">
					<?php
					$custom_cart_total = 0;
					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
						$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
						$product_brand = get_post_meta( $cart_item['product_id'], 'product_brand', true );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
							?>
							<div class="order-you-pr d-f-row-stre-s_b">
								<div class="order-you-pr__img">
									<?php
									$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image( 'full' ), $cart_item, $cart_item_key );

									if ( ! $product_permalink ) {
										echo $thumbnail; // PHPCS: XSS ok.
									} else {
										printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
									}
									?>
								</div>

								<div class="order-you-pr__info">
									<div class="order-you-pr__info_top d-f-row-stre-s_b">
										<div class="order-you-pr__info_left order-you-pr__info_left">
											<a href="<?php echo esc_url( $product_permalink ); ?>">

												<!-- name  -->
												<div class="order-you-pr__info_name">
													<?php  $product_name = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ); ?>
													<h4><?php echo wp_kses_post( $product_name ); ?></h4>
												</div>

												<!-- brand -->
												<div class="order-you-pr__info_brand-volume d-f-row-stre-f_s">
													<?php
														if ( $product_brand ) {
															echo '<p>';
															echo esc_html( $product_brand );
															echo  '</p>';
														}
													?>
																											
													<?php
														$pa_volume = $_product->get_attribute( 'pa_volume' );
														if ( ! empty( $pa_volume ) ) {
															$pa_volume_arr = explode( ',', $pa_volume );
															echo '<div class="order-you-pr__info_line"></div>';
															echo  '<p>';
															echo $pa_volume_arr[0] . __( ' мл', 'la-fleur' );
															echo  '</p>';
														}
													?>															
												</div>
											</a>
										</div>

										<div class="order-you-pr__info_right">
											<?php
											$remove_link_svg = '<svg width="10" height="14" viewBox="0 0 10 14" fill="none" xmlns="http://www.w3.org/2000/svg">';
											$remove_link_svg .= '<path d="M3.26303 5.21762C3.36325 5.21762 3.44873 5.30106 3.44873 5.40536V11.0376C3.44873 11.139 3.3662 11.2254 3.26303 11.2254C3.15987 11.2254 3.07734 11.142 3.07734 11.0376V5.40536C3.07734 5.30106 3.15987 5.21762 3.26303 5.21762Z" fill="black" />';
											$remove_link_svg .= '<path d="M6.73911 5.21762C6.83933 5.21762 6.9248 5.30106 6.9248 5.40536V11.0377C6.9248 11.139 6.84227 11.2254 6.73911 11.2254C6.63594 11.2254 6.55341 11.142 6.55341 11.0377V5.40536C6.55341 5.30106 6.63594 5.21762 6.73911 5.21762Z" fill="black" />';
											$remove_link_svg .= '<path d="M0.00835896 3.05106C0.073205 2.54147 0.506495 2.15704 1.01642 2.15704H2.63462V1.67428C2.63168 1.39117 2.74663 1.11701 2.94117 0.917346C3.13865 0.717683 3.40393 0.607422 3.681 0.607422H3.68984H6.31906C6.59907 0.607422 6.8614 0.717683 7.05889 0.917346C7.25343 1.11403 7.36838 1.39117 7.36543 1.6713V2.15704H8.98659C9.49356 2.15704 9.92685 2.54147 9.9917 3.05106C10.0565 3.56064 9.73526 4.04341 9.24597 4.17751L9.16344 4.19837V11.6276C9.16344 12.0269 9.01606 12.4144 8.76257 12.6855C8.51203 12.9567 8.15538 13.1117 7.78988 13.1117H2.21312C1.84763 13.1117 1.49097 12.9567 1.24043 12.6855C0.983996 12.4144 0.839567 12.0269 0.839567 11.6276V4.19837L0.757035 4.17751C0.261847 4.04341 -0.0564871 3.56064 0.00835896 3.05106ZM2.21018 12.7362H7.78988C8.35286 12.7362 8.79499 12.2475 8.79499 11.6246V4.19837H1.20506V11.6276C1.20506 12.2475 1.64719 12.7362 2.21018 12.7362ZM6.31906 0.979927H6.30727H3.681C3.5012 0.979927 3.33024 1.05145 3.2035 1.17959C3.07381 1.31071 3.00012 1.48951 3.00307 1.67428V2.15704H6.99699V1.6713C6.99994 1.48951 6.92625 1.30773 6.79656 1.17959C6.66981 1.05145 6.49886 0.979927 6.31906 0.979927ZM8.98659 2.52955H1.01642C0.662715 2.52955 0.373855 2.82159 0.373855 3.1792C0.373855 3.5368 0.662715 3.82885 1.01642 3.82885H8.98659C9.34029 3.82885 9.62915 3.5368 9.62915 3.1792C9.62915 2.82159 9.33734 2.52955 8.98659 2.52955Z" fill="black" />';
											$remove_link_svg .= '<path d="M5.00132 5.21762C5.10153 5.21762 5.18701 5.30106 5.18701 5.40536V11.0376C5.18701 11.139 5.10448 11.2254 5.00132 11.2254C4.89815 11.2254 4.81562 11.142 4.81562 11.0376V5.40536C4.81562 5.30106 4.89815 5.21762 5.00132 5.21762Z" fill="black" />';
											$remove_link_svg .= '</svg>';
											
											echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
												'woocommerce_cart_item_remove_link',
												sprintf(
													'<a href="%s" class="remove remove_from_cart_button" onclick="removeFromCheckout();" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s">'. $remove_link_svg .'</a>',
													esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
													esc_attr__( 'Remove this item', 'woocommerce' ),
													esc_attr( $product_id ),
													esc_attr( $cart_item_key ),
													esc_attr( $_product->get_sku() )
												),
												$cart_item_key
											);
											?>
										</div>
									</div>

									<div class="order-you-pr__info_bottom d-f-row-stre-s_b">
										<!-- count -->
										<div class="order-you-pr_info_bottom_left">
											<button class="" onclick="changeCountProductCheckout(this, event, 'minus');">-</button>

											<label>
												<input type="number" class="input-text qty text" step="1" id="this-product-count"  
												min="1" max="" name="cart[<?php echo $cart_item_key; ?>][qty]"
												value="<?php echo $cart_item['quantity']; ?>" 
												title="Кол-во" size="4" placeholder="" inputmode="numeric">											
												
												<input type="hidden" name="iv_product_key" value="<?php echo $cart_item_key; ?>">
											</label>
											<!--  -->
											<button class="" onclick="changeCountProductCheckout(this, event, 'plus');">+</button>
											
										</div>

										<!-- price -->
										<div class="order-you-pr__info_bottom_price">
											<?php								
											$row_price = $_product->get_price() * $cart_item['quantity'];
											$custom_cart_total += $row_price;
											?>
											<p><?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );// echo $price; ?></p>
										</div>
									</div>

								</div>
							</div>
					
						<?php
						}
					}
					?>
													
					<?php wp_nonce_field( 'iv-ajax-nonce', 'security' ); ?>
									
				</form>
				<input type="hidden" id="cart_count" value="<?php echo WC()->cart->cart_contents_count; ?>" >	
			</div>

			<!-- total sum -->
			<div class="order-total-sum">
				<div class="order-total-sum__wrapper d-f-row-stre-s_b">
					<div class="order_total-summ">
						<p><?php _e( 'Общая сумма:', 'la-fleur' ); ?></p>

						<h4><span id="checkout_sum"><?php echo $custom_cart_total; ?></span><?php echo get_woocommerce_currency_symbol(); ?></h4>
					</div>
					
					<!-- count delivery -->
					<div class="order_total-summ">
						<p><?php _e( 'Доставка:', 'la-fleur' ); ?></p>							
						<h4><span id="checkout_shipping"><?php echo WC()->cart->shipping_total; ?></span><?php echo get_woocommerce_currency_symbol(); ?></h4>
					</div>

					<!-- all count -->
					<div class="order_total-summ">
						<p><?php _e( 'ИТОГО:', 'la-fleur' ); ?></p>							
						<h4><span id="checkout_total"><?php echo ( $custom_cart_total + WC()->cart->shipping_total ); ?></span><?php echo get_woocommerce_currency_symbol(); ?></h4>
					</div>
					
				</div>
			</div>
			
		</div>
		<?php
	} else {
		?>
		<div id="iv_checkout">
			<div id="modal-shop-card-no-product" class="block-product-card_no-product">
				<div class="block-product-card_no-product__wrapper d-f-column-f_c-f_s">

					<input type="hidden" id="cart_count" value="<?php echo WC()->cart->cart_contents_count; ?>" >
					<!-- img -->
					<div class="modal-block-no-product__img d-f-row-c-c">
						<svg width="80" height="90" viewBox="0 0 80 90" fill="none"
							xmlns="http://www.w3.org/2000/svg">
							<g clip-path="url(#clip0)">
								<path
									d="M68.8236 20.837C67.7622 19.6639 66.3098 18.9935 64.6897 18.9935H56.3102V14.8596C56.3102 6.7036 49.4949 0 41.1713 0H29.0489C20.6694 0 13.91 6.7036 13.8541 14.9155V19.0494H5.47461C4.02216 19.0494 2.62558 19.608 1.56417 20.6136C0.558633 21.6191 0 22.9598 0 24.4123C0 24.6357 0 24.8592 0.0558633 25.0826L4.4132 61.3379C5.0277 66.4215 9.4409 70.2202 14.6362 70.2761H55.584C60.7793 70.2761 65.1925 66.4215 65.8628 61.3379L70.2202 25.0826C70.3878 23.5185 69.885 22.0101 68.8236 20.837ZM12.7927 33.2945V29.7751H16.3121V33.2945H12.7927ZM53.9081 33.2945V29.7751H57.4275V33.2945H53.9081ZM13.91 28.5462H12.1223C11.7872 28.5462 11.452 28.8255 11.452 29.1607V33.909C11.452 34.2442 11.7313 34.5235 12.1223 34.5235H16.9824C17.3176 34.5235 17.6528 34.2442 17.6528 33.909V29.1607C17.6528 28.8255 17.3735 28.5462 16.9824 28.5462H15.1948V20.2784H55.0254V28.5462H53.2377C52.9026 28.5462 52.5674 28.8255 52.5674 29.1607V33.909C52.5674 34.2442 52.8467 34.5235 53.2377 34.5235H58.0978C58.433 34.5235 58.7682 34.2442 58.7682 33.909V29.1607C58.7682 28.8255 58.4889 28.5462 58.0978 28.5462H56.3102V20.2784H64.6897C65.807 20.2784 66.8684 20.7253 67.6505 21.5074C68.4326 22.2895 68.8795 23.3509 68.8795 24.4123C68.8795 24.5799 68.8795 24.7474 68.8236 24.915L64.4663 61.1703C63.9076 65.6394 60.0531 68.9912 55.5281 68.9912H14.6362C10.1113 68.9912 6.25669 65.6394 5.69806 61.1703L1.34072 24.915C1.22899 23.8536 1.50831 22.7364 2.17867 21.8984C2.84903 21.0046 3.85457 20.446 4.97183 20.3342C5.13942 20.3342 5.30701 20.2784 5.47461 20.2784H13.8541V28.5462H13.91ZM29.0489 1.28486H41.1713C48.8245 1.28486 55.0254 7.42982 55.0254 14.9155V19.0494H15.1948V14.9155C15.1948 7.37396 21.4515 1.28486 29.0489 1.28486Z"
									fill="black" />
							</g>
							<circle cx="53.074" cy="62.8716" r="26.1284" fill="#FEFCF5" stroke="black"
								stroke-width="1.3" />
							<path
								d="M52.958 69.0229C51.986 69.0229 51.116 68.7649 50.348 68.2489C49.592 67.7329 48.998 66.9949 48.566 66.0349C48.146 65.0629 47.936 63.9229 47.936 62.6149C47.936 61.3069 48.146 60.1729 48.566 59.2129C48.998 58.2409 49.592 57.4969 50.348 56.9809C51.116 56.4649 51.986 56.2069 52.958 56.2069C53.93 56.2069 54.794 56.4649 55.55 56.9809C56.318 57.4969 56.918 58.2409 57.35 59.2129C57.782 60.1729 57.998 61.3069 57.998 62.6149C57.998 63.9229 57.782 65.0629 57.35 66.0349C56.918 66.9949 56.318 67.7329 55.55 68.2489C54.794 68.7649 53.93 69.0229 52.958 69.0229ZM52.958 67.8349C53.69 67.8349 54.332 67.6309 54.884 67.2229C55.448 66.8149 55.886 66.2209 56.198 65.4409C56.51 64.6609 56.666 63.7189 56.666 62.6149C56.666 61.5109 56.51 60.5689 56.198 59.7889C55.886 59.0089 55.448 58.4149 54.884 58.0069C54.332 57.5989 53.69 57.3949 52.958 57.3949C52.226 57.3949 51.578 57.5989 51.014 58.0069C50.462 58.4149 50.03 59.0089 49.718 59.7889C49.406 60.5689 49.25 61.5109 49.25 62.6149C49.25 63.7189 49.406 64.6609 49.718 65.4409C50.03 66.2209 50.462 66.8149 51.014 67.2229C51.578 67.6309 52.226 67.8349 52.958 67.8349Z"
								fill="black" />
							<defs>
								<clipPath id="clip0">
									<rect width="70.2202" height="70.2202" fill="white" />
								</clipPath>
							</defs>
						</svg>
					</div>

					<!-- link -->
					<div class="modal-product-card-link">
						<a href="<?php echo esc_url( home_url( '/catalog/' ) ); ?>" class="btn-link-this-page d-f-row-c-c">
							<?php _e( 'Вернуться в каталог?', 'la-fleur' ); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}

// iv_checkout_quanity_update
add_action( 'wp_ajax_iv_checkout_quanity_update', 'callback_iv_checkout_quanity_update' );
add_action( 'wp_ajax_nopriv_iv_checkout_quanity_update', 'callback_iv_checkout_quanity_update' );

function callback_iv_checkout_quanity_update() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' ); 
	
	$number = $_POST['number'];
	$key = $_POST['product_key'];
	
	WC()->cart->set_quantity( $key, $number );

	echo la_fleur_checkout();
	
	wp_die();
}

// ajax_remove_from_checkout
add_action( 'wp_ajax_ajax_remove_from_checkout', 'callback_ajax_remove_from_checkout' );
add_action( 'wp_ajax_nopriv_ajax_remove_from_checkout', 'callback_ajax_remove_from_checkout' );

function callback_ajax_remove_from_checkout() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' );

	echo la_fleur_checkout();
	
	wp_die();
}


// product_ajax_add_to_cart
add_action( 'wp_ajax_product_ajax_add_to_cart', 'callback_product_ajax_add_to_cart' );
add_action( 'wp_ajax_nopriv_product_ajax_add_to_cart', 'callback_product_ajax_add_to_cart' );

function callback_product_ajax_add_to_cart() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' );
	
	$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
	$quantity = empty( $_POST['quantity'] ) ? 1 : apply_filters( 'woocommerce_stock_amount', $_POST['quantity'] );
	
	if ( isset( $_POST['variation_id'] ) && isset( $_POST['variation'] ) ) {
		$variation_id = intval( $_POST['variation_id'] );
		$variation = $_POST['variation'];
		
		$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id, $variation );
		if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation ) ) {
			echo la_fleur_mini_cart();	
			wp_die();
		}
	} else {
		$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
		if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity ) ) {
			echo la_fleur_mini_cart();	
			wp_die();
		}
	}
}

// product_ajax_add_to_cart
add_action( 'wp_ajax_beauty_set_ajax_add_to_cart', 'callback_beauty_set_ajax_add_to_cart' );
add_action( 'wp_ajax_nopriv_beauty_set_ajax_add_to_cart', 'callback_beauty_set_ajax_add_to_cart' );

function callback_beauty_set_ajax_add_to_cart() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' );
	$beauty_set = $_POST['beauty_set'];
	$beauty_set = explode( ',', $beauty_set );
	
	if ( ! empty( $beauty_set ) ) {
		foreach ( $beauty_set as $product_id ) {
			$variation_id = '';
			$quantity = 1;
			$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $product_id ) );
			if ( 'product_variation' === get_post_type( $product_id ) ) {
				$variation_id = $product_id;
				$product_id   = wp_get_post_parent_id( $variation_id );
			}
			
			if ( ! empty( $variation_id ) ) {
				$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id );
				if ( $passed_validation ) {
					WC()->cart->add_to_cart( $product_id, $quantity, $variation_id );
				}
			} else {
				$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
				if ( $passed_validation ) {
					WC()->cart->add_to_cart( $product_id, $quantity );
				}
			}
		}
		
		echo la_fleur_mini_cart();	
		wp_die();
	}
}

// group_ajax_add_to_cart
add_action( 'wp_ajax_group_ajax_add_to_cart', 'callback_group_ajax_add_to_cart' );
add_action( 'wp_ajax_nopriv_group_ajax_add_to_cart', 'callback_group_ajax_add_to_cart' );

function callback_group_ajax_add_to_cart() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' );
	$group = $_POST['item'];
	//$beauty_set = explode( ',', $beauty_set );
	
	if ( ! empty( $group ) ) {
		foreach ( $group as $product_id => $quantity ) {
			$quantity = empty( $quantity ) ? 1 : apply_filters( 'woocommerce_stock_amount', $quantity );
			$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $product_id ) );
			
			$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
			if ( $passed_validation ) {
				WC()->cart->add_to_cart( $product_id, $quantity );
			}			
		}
		
		echo la_fleur_mini_cart();	
		wp_die();
	}
}

// following_to_news
add_action( 'wp_ajax_following_to_news', 'callback_following_to_news' );
add_action( 'wp_ajax_nopriv_following_to_news', 'callback_following_to_news' );

function callback_following_to_news() {
	check_ajax_referer( 'iv-ajax-nonce', 'security_email' );
	$online_email = sanitize_email( $_POST['online_email'] );
	
	$return = array();
	
	if ( ! is_email( $online_email ) ) {
		$return['error'] = __( 'Этот эл. адрес некорректный.', 'la-fleur' );
		echo json_encode( $return );
		wp_die();
	}
	
	if ( email_exists( $online_email ) ) {
		$return['error'] = __( 'Этот эл. адрес уже существует.', 'la-fleur' );
		echo json_encode( $return );
		wp_die();
	}
	
	$arr_emails = get_option( 'following_to_news' );
	// search in custom option with emails
	if ( ! empty( $arr_emails ) ) {
		foreach ( $arr_emails as $email ) {
			if ( $email == $online_email ) {
				$return['error'] = __( 'Этот эл. адрес уже существует.', 'la-fleur' );
				echo json_encode( $return );
				wp_die();
			}
		}
	}
	
	if ( ! empty( $arr_emails ) ) {
		$arr_emails[] = $online_email;
	} else {
		$arr_emails = array();
		$arr_emails[] = $online_email;
	}
	$check_update = update_option( 'following_to_news', $arr_emails );
	if ( $check_update ) {
		$return['success'] = 'ok';
	} else {
		$return['error'] = __( 'Неизвестная ошыбка.', 'la-fleur' );
	}
	
	echo json_encode( $return );
	wp_die();
}

// User profile, edit fields of phone and city
add_action( 'show_user_profile', 'la_fleur_edit_user_fields' ); // editing your own profile
add_action( 'edit_user_profile', 'la_fleur_edit_user_fields' ); // editing another user
add_action( 'user_new_form', 'la_fleur_edit_user_fields' ); // creating a new user

function la_fleur_edit_user_fields( $user ) {
?>
<h2><?php _e( 'Дополнительные даные', 'la-fleur' ); ?></h2>
    <table class="form-table">
        <tr>
            <th><label for="phone"><?php _e( 'Телефон', 'la-fleur' ); ?></label></th>
            <td>
                <input
                    type="text"
                    value="<?php echo esc_attr( get_user_meta( $user->ID, 'phone', true ) ); ?>"
                    name="phone"
                    id="phone"
                >
            </td>       
        </tr>
        <tr>
			<th><label for="city"><?php _e( 'Город', 'la-fleur' ); ?></label></th>
            <td>
                <input
                    type="text"
                    value="<?php echo esc_attr( get_user_meta( $user->ID, 'city', true ) ); ?>"
                    name="city"
                    id="city"
                >
            </td>
        </tr>
    </table>
<?php
}

// User profile, save fields of phone and city
add_action( 'personal_options_update', 'la_fleur_save_user_fields' );
add_action( 'edit_user_profile_update', 'la_fleur_save_user_fields' );
add_action( 'user_register', 'la_fleur_save_user_fields' );

function la_fleur_save_user_fields( $userId ) {
    if ( ! current_user_can( 'edit_user', $userId ) ) {
        return;
    }
 
    update_user_meta( $userId, 'city', $_REQUEST['city'] );
    update_user_meta( $userId, 'phone', $_REQUEST['phone'] );
}


