<?php
// Add custom post type 'sale'
function la_fleur_custom_sale_init(){
	$args = array(
		'labels'             => array(
			'name'               => _x( 'Акции', 'Post type general name', 'la-fleur' ),
			'singular_name'      => _x( 'Акция', 'Post type singular name', 'la-fleur' ),
			'add_new'            => __( 'Добавить акцию', 'la-fleur' ),
			'add_new_item'       => __( 'Добавить новую акцию', 'la-fleur' ),
			'edit_item'          => __( 'Редактировать акцию', 'la-fleur' ),
			'new_item'           => __( 'Новая акция', 'la-fleur' ),
			'view_item'          => __( 'Просмотреть акцию', 'la-fleur' ),
			'search_items'       => __( 'Найти акцию', 'la-fleur' ),
			'not_found'          => __( 'Не найдено', 'la-fleur' ),
			'not_found_in_trash' => __( 'Не найдено в корзине', 'la-fleur' ),
			'parent_item_colon'  => __( 'Родительская акция', 'la-fleur' ),
			'menu_name'          => __( 'Акции', 'la-fleur' ),
			'all_items' 		 => __( 'Все акции', 'la-fleur' ),
		  ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => false,//true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-cart',
		'supports'           => array( 'title', 'editor', 'author' )
	);
	register_post_type( 'sale', $args );
}
add_action( 'init', 'la_fleur_custom_sale_init', 0 );

// Add custom post type 'beauty-set'
function la_fleur_custom_beauty_set_init(){
	$args = array(
		'labels'             => array(
			'name'               => _x( 'Бьюти набор', 'Post type general name', 'la-fleur' ),
			'singular_name'      => _x( 'Бьюти набор', 'Post type singular name', 'la-fleur' ),
			'add_new'            => __( 'Добавить бьюти набор', 'la-fleur' ),
			'add_new_item'       => __( 'Добавить бьюти набор', 'la-fleur' ),
			'edit_item'          => __( 'Редактировать бьюти набор', 'la-fleur' ),
			'new_item'           => __( 'Новый бьюти набор', 'la-fleur' ),
			'view_item'          => __( 'Просмотреть бьюти набор', 'la-fleur' ),
			'search_items'       => __( 'Найти бьюти набор', 'la-fleur' ),
			'not_found'          => __( 'Не найдено', 'la-fleur' ),
			'not_found_in_trash' => __( 'Не найдено в корзине', 'la-fleur' ),
			'parent_item_colon'  => __( 'Родительский бьюти набор', 'la-fleur' ),
			'menu_name'          => __( 'Бьюти наборы', 'la-fleur' ),
			'all_items' 		 => __( 'Все бьюти наборы', 'la-fleur' ),
		  ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => false,//true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-paperclip',
		'supports'           => array( 'title', 'author' )
	);
	register_post_type( 'beauty-set', $args );
}
add_action( 'init', 'la_fleur_custom_beauty_set_init', 0 );
