<?php
defined( 'ABSPATH' ) || exit;

class La_Fleur_Categories_List_Walker extends Walker_Category {
    function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
        extract($args);

        $cat_name = esc_attr( $category->name );
        $cat_id = $category->term_id;
        $cat_name = apply_filters( 'list_cats', $cat_name, $category );
        $cat_link = get_term_link( $category, 'product_cat' );
        //$link = '<a href="'. esc_url( $cat_link ) .'" ';
        
        $base_link = home_url( $_SERVER['REQUEST_URI'] );
        $link_arg = add_query_arg( 'product_cat', $category->slug, $base_link );
        $link = '<a href="'. esc_url( $link_arg ) .'" ';
        
		$link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';
		$link .= ' data-cat-id="'. $cat_id .'">';
		$link .= $cat_name;
            
        if ( ! empty( $show_count ) ) { $link .= ' (' . intval( $category->count ) . ')'; }
        $link .= '</a>';   

        $output .= $link;
    }
}

class La_Fleur_Widget_Categories_List extends WP_Widget {

	function __construct() {

		$widget_ops = array( 'classname' => 'widget-categories-list', 'description' => __( 'La Fleur - список категорий продуктов', 'la-fleur' ) );
		$control_ops = array( 'id_base' => 'widget_categories_list' );
		parent::__construct( 'widget_categories_list', __( 'La Fleur - список категорий продуктов', 'la-fleur' ), $widget_ops, $control_ops );
	}

    function widget( $args, $instance ) {
        extract( $args );
        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Фильтровать' ) : $instance['title'] );
        $c = $instance['count'] ? '1' : '0';
        $hec = $instance['hide_e_cat'] ? '1' : '0';

        echo $before_widget;
		?>
			
		<div class="section-choosed-filter_wrapper d-f-column-f_s-f_s">
			<div class="section-choosed-filter_title d-f-row-c-f_s">
				<svg width="14" height="11" viewBox="0 0 14 11" fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<line x1="12.4418" y1="0.628479" x2="12.4418" y2="11" stroke="black"
						stroke-width="0.7" />
					<line x1="7.07559" y1="0.628479" x2="7.07559" y2="11" stroke="black"
						stroke-width="0.7" />
					<line x1="1.70937" y1="0.628479" x2="1.70937" y2="11" stroke="black"
						stroke-width="0.7" />
					<circle cx="12.4248" cy="8.15778" r="1.22949" fill="black" />
					<circle cx="7.04395" cy="3.30356" r="1.22949" fill="black" />
					<circle cx="1.7002" cy="8.15778" r="1.22949" fill="black" />
				</svg>
				<h4><?php echo $title; ?></h4>
			</div>
			<div class="this-choosed-filter-block d-f-column-f_s-f_s">
				<?php				 
				$args = array( 
					'taxonomy' => 'product_cat',
					//'orderby' => 'name',
					//'hierarchical' => 1,
					'title_li' => '',
					'show_count' => $c,
					'hide_empty' =>  $hec,
					'walker'       => new La_Fleur_Categories_List_Walker(),
				);
				echo wp_list_categories( $args );
				?>
			</div>
		</div>
        
		<?php
		
		
        echo $after_widget;
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['count'] = $new_instance['count'] ? 1 : 0;
        $instance['hide_e_cat'] = $new_instance['hide_e_cat'] ? 1 : 0;

        return $instance;
    }

    function form( $instance ) {
        //Defaults
        $instance = wp_parse_args( (array) $instance, array( 'title' => '') );
        $title = esc_attr( $instance['title'] );
        $count = isset($instance['count']) ? (bool) $instance['count'] :false;
        $hide_e_cat = isset($instance['hide_e_cat']) ? (bool) $instance['hide_e_cat'] : false;
		?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		 
		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
		<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show category counts' ); ?></label><br />

        <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hide_e_cat'); ?>" name="<?php echo $this->get_field_name('hide_e_cat'); ?>"<?php checked( $hide_e_cat ); ?> />
        <label for="<?php echo $this->get_field_id('hide_e_cat'); ?>"><?php _e( 'Hide empty categories' ); ?></label><br />
        
	<?php
    }

}

function la_fleur_load_categories_widgets() {
	register_widget( 'La_Fleur_Widget_Categories_List' );
}
add_action( 'widgets_init', 'la_fleur_load_categories_widgets' );
