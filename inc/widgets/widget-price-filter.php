<?php
defined( 'ABSPATH' ) || exit;

class La_Fleur_Widget_Price_Filter extends WP_Widget {

	function __construct() {

		$widget_ops = array( 'classname' => 'widget-price-filter', 'description' => __( 'La Fleur - фильтр по ценам', 'la-fleur' ) );
		$control_ops = array( 'id_base' => 'widget_price_filter' );
		parent::__construct( 'widget_price_filter', __( 'La Fleur - фильтр по ценам', 'la-fleur' ), $widget_ops, $control_ops );
	}

    function widget( $args, $instance ) {
        extract( $args );
        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Цена' ) : $instance['title'] );

        //echo $before_widget;
		?>
			
		<div class="section-group-filter_item">
			<div class="section-group-filter_btn d-f-row-c-s_b">
				<div class="section-group-filter_btn_title">
					<h3><?php echo $title; ?></h3>
				</div>

				<svg width="16" height="16" viewBox="0 0 16 16" fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<line y1="-0.5" x2="20.7549" y2="-0.5"
						transform="matrix(-0.715456 0.698658 -0.715456 -0.698658 15.25 0.430695)"
						stroke="black" />
					<line x1="0.716797" y1="2.77975" x2="0.716797" y2="15.0001"
						stroke="black" />
					<line x1="12.7314" y1="15.5001" x2="0.217262" y2="15.5001"
						stroke="black" />
				</svg>
			</div>

			<div class="section-group-filter_show d-f-column-f_s-f_s">
				<?php
				$shop_filter_price = get_field( 'shop_filter_price', 'option' );
				if ( ! empty( $shop_filter_price ) ) {
					foreach ( $shop_filter_price as $filter ) {
						?>
						<div class="section-shop-filter_item animation-this-duration">
							<label class="d-f-row-c-f_s">
								<?php la_fleur_widget_price_arg( $filter['min'], $filter['max'], $filter['text'] ); ?>
							</label>
						</div>
						<?php
					}
				}
				?>
			</div>
		</div>
        
		<?php
        //echo $after_widget;
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);

        return $instance;
    }

    function form( $instance ) {
        //Defaults
        $instance = wp_parse_args( (array) $instance, array( 'title' => '') );
        $title = esc_attr( $instance['title'] );
		?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
	<?php
    }

}

function la_fleur_load_price_widgets() {
	register_widget( 'La_Fleur_Widget_Price_Filter' );
}
add_action( 'widgets_init', 'la_fleur_load_price_widgets' );
