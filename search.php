<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package LaFleur
 */

get_header();
?>

	<!-- <section pagination start -->
	<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
	<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
		<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
			<div class="section-pagination__wrapper all-width">
				<ul class="breadcrumb">
					<?php woocommerce_breadcrumb(); ?>
				</ul>
			</div>
		</div>
	<?php } ?>
	<!-- <section pagination end -->
	
	<div class="page-info-method w_100">
        <div class="page-info-method_wr all-width d-f-column-f_s-f_s">

		<?php if ( have_posts() ) : ?>

			<div class="page-info-method_title w_100">
				<h2>
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'la-fleur' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h2>
			</div>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</div>
	</div>

<?php
get_footer();
