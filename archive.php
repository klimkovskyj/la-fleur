<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package LaFleur
 */

get_header();
?>
	<!-- <section pagination start -->
	<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
	<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
		<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
			<div class="section-pagination__wrapper all-width">
				<ul class="breadcrumb">
					<?php woocommerce_breadcrumb(); ?>
				</ul>
			</div>
		</div>
	<?php } ?>
	<!-- <section pagination end -->
	
	<div class="page-info-method w_100">
        <div class="page-info-method_wr all-width d-f-column-f_s-f_s">

		<?php if ( have_posts() ) : ?>

			<div class="page-info-method_title w_100">
				<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</div>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</div>
	</div>

<?php
get_footer();
