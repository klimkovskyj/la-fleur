<?php
/**
 * LaFleur functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package LaFleur
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'la_fleur_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function la_fleur_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on LaFleur, use a find and replace
		 * to change 'la-fleur' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'la-fleur', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(			
				'primary-menu' => esc_html__( 'Primary menu', 'la-fleur' ),
				'footer-menu' => esc_html__( 'Footer menu', 'la-fleur' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'la_fleur_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'la_fleur_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function la_fleur_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'la_fleur_content_width', 640 );
}
add_action( 'after_setup_theme', 'la_fleur_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function la_fleur_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Shop - left sidebar 1', 'la-fleur' ),
			'id'            => 'shop-sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'la-fleur' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);	
	register_sidebar(
		array(
			'name'          => esc_html__( 'Shop - left sidebar 2', 'la-fleur' ),
			'id'            => 'shop-sidebar-2',
			'description'   => esc_html__( 'Add widgets here.', 'la-fleur' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'la_fleur_widgets_init' );

/**
 * Enqueue styles.
 */
function la_fleur_style() {
	// style
	wp_enqueue_style( 'la-fleur-style', get_stylesheet_uri(), array(), time() );// _S_VERSION
	
	//wp_enqueue_style( 'la-fleur-googl-fonts', "https://fonts.googleapis.com/css2?family=Cormorant+Garamond:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&family=Cormorant+SC:wght@400;500;700&family=Montserrat:ital,wght@0,400;0,500;0,600;0,700;0,900;1,400;1,600;1,700;1,900&display=swap" );
    
    //wp_enqueue_style( 'la-fleur-slick-cdn', "https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" );
    wp_enqueue_style( 'la-fleur-slick', get_template_directory_uri(). '/library/slick-1.8.1/slick/slick.css' );

    wp_enqueue_style( 'la-fleur-select2', "https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" );
    wp_enqueue_style( 'la-fleur-select2-min', "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" );
	
	wp_enqueue_style( 'la-fleur-style-origin', get_template_directory_uri(). '/style/style.css', array(), time() );
	
	// script
	//wp_enqueue_script( 'la-fleur-navigation', get_template_directory_uri() . '/script/navigation.js', array(), _S_VERSION, true );
}
add_action( 'wp_enqueue_scripts', 'la_fleur_style', 99 );

/**
 * Enqueue scripts.
 */
function la_fleur_scripts() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(), '', true );
    wp_enqueue_script( 'jquery' );
    
    wp_enqueue_script( 'la-fleur-inputmask', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.1/jquery.inputmask.bundle.min.js', array('jquery'), '', true );
    
    wp_enqueue_script( 'la-fleur-slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), '', true );
	wp_enqueue_script( 'la-fleur-select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js', array(), '', true );

    wp_enqueue_script( 'la-fleur-jquery', get_template_directory_uri() . '/script/lafleur-jquery.js', array('jquery'), time(), true );//_S_VERSION
    wp_enqueue_script( 'la-fleur-script', get_template_directory_uri() . '/script/script.js', array('jquery'), time(), true );//_S_VERSION
    
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'la_fleur_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom post types.
 */
require get_template_directory() . '/inc/custom-post-types.php';

/**
 * Widgets.
 */
require get_template_directory() . '/inc/widgets/widget-price-filter.php';
require get_template_directory() . '/inc/widgets/widget-categories-list.php';
require get_template_directory() . '/inc/widgets/la-fleur-widget-filters.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	//require get_template_directory() . '/inc/woocommerce.php';
	require get_template_directory() . '/woocommerce/wc-functions.php';
	require get_template_directory() . '/woocommerce/wc-account-functions.php';
}

/**
 *  Template directory uri
 */
if ( ! defined( 'LA_FLEUR_THEME_URI' ) ) {
	define( 'LA_FLEUR_THEME_URI', get_template_directory_uri() );
}

/**
 *  Check is mobil
 */
require_once "inc/Mobile_Detect.php";
$detect = new Mobile_Detect;
if ( $detect->isMobile() || $detect->isTablet() ) { define( 'IS_MOBIL', true ); } else { define( 'IS_MOBIL', false ); }

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
remove_filter( 'comment_text', 'wpautop' );

/**
 *  Site setting
 */
add_action('admin_menu', 'my_acf_op_init');
function my_acf_op_init() {
	if ( function_exists('acf_add_options_page') ) {	
		acf_add_options_page(array(
			'page_title' 	=> 'La-Fleur - настройки сайта',
			'menu_title'	=> 'Настройки сайта',
			'menu_slug' 	=> 'la-fleur-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false,
			'position' 		=> '33.3',
		));
		acf_add_options_page(array(
			'page_title' 	=> 'Настройки для страницы - Магазин',
			'menu_title'	=> 'Настройки - Магазин',
			'menu_slug' 	=> 'la-fleur-settings-shop',
			'parent_slug' 	=> 'la-fleur-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false,
		));
	}
}


// Follow us
add_action('admin_menu', 'la_fleur_emails_menu_page');

function la_fleur_emails_menu_page() {
	add_menu_page( 'Страница со списком имейлов', 'Настройки - Подписка', 'manage_options', 'la-fleur-settings-emails', 'la_fleur_settings_emails_callback', '', 34 ); 
}

function la_fleur_settings_emails_callback() {
	// контент страницы
	echo '<div class="wrap">';
		echo '<h2>Страница со списком имейлов</h2>';
		
		$arr_emails = get_option( 'following_to_news' );
		if ( ! empty( $arr_emails ) ) {
			foreach ( $arr_emails as $email ) {
				echo '<p>'. $email .'</p>';
			}
		}
	echo '</div>';

}

// We need some CSS to position the paragraph.
function la_fleur_product_list_css() {
	echo "
	<style type='text/css'>
		table.wp-list-table .column-name {
			width: 18%;
		}
		table.wp-list-table .column-sku {
			width: 7%  !important;
		}
		table.wp-list-table .column-is_in_stock {
			text-align: left !important;
			width: 6ch  !important;
		}
		table.wp-list-table .column-product_cat, table.wp-list-table .column-product_tag {
			width: 8% !important;
		}
		table.wp-list-table .column-language {
			width: 8% !important;
		}
		table.wp-list-table .column-featured, table.wp-list-table .column-product_type {
			width: 38px;
		}
		.fixed .column-date {
			width: 10%;
		}
		.column-taxonomy-character-of-an-aroma {
			width: 8%;
		}
	</style>
	";
}

add_action( 'admin_head', 'la_fleur_product_list_css' );

// Enqueue script for ajax
function iv_ajax_init() {
	wp_enqueue_script( 'iv-data', get_template_directory_uri() . '/script/iv-data.js', array('jquery'), time(), false );
	
	wp_localize_script( 'iv-data', 'iv_ajax_object', array( 
	  'ajaxurl' => admin_url( 'admin-ajax.php' ),
	  'redirecturl' => $_SERVER['REQUEST_URI'],
	  'loadingmessage' => __( 'Данные проверяются ...', 'la-fleur' ),
	  'downloading' => __( 'Загружаю ...', 'la-fleur' ),
	  'watch_all' => __( 'Смотреть все', 'la-fleur' ),
	  'error' => __( 'Ошыбка', 'la-fleur' ),
	  'thank' => __( 'Спасибо!', 'la-fleur' ),
	  'thank_msg' => __( 'Благодарим за Ваше сообщение.<br>Оно успешно отправлено.', 'la-fleur' ),
	  'pass_not_match' => __( 'Пароли не совпадают', 'la-fleur' ),
	  'pass_repeat' => __( 'Повторите пароль', 'la-fleur' ),
	  'pass_fill_in' => __( 'Заполните поле "Пароль"', 'la-fleur' ),
	  'pass_long' => __( 'Длинна пароля должна быть не менее 6 символов', 'la-fleur' ),
	  'less' => __( 'Меньше', 'la-fleur' ),
	  'more' => __( 'Больше', 'la-fleur' ),
	  'pass_error' => __( 'Одно или несколько полей содержат ошибочные данные. <br> Пожалуйста, проверьте их и попробуйте ещё раз.', 'la-fleur' ),
	  'pass_changed' => __( 'Ваш пароль успешно изменен.', 'la-fleur' ),
	  'edit_address' => __( 'Редактирование адреса.', 'la-fleur' ),
	));

	add_action( 'wp_ajax_nopriv_ajax_login', 'callback_ajax_login' );
	add_action( 'wp_ajax_nopriv_ajax_registration', 'callback_ajax_registration' );
	add_action( 'wp_ajax_nopriv_ajax_reset_password', 'callback_ajax_reset_password' );
}

//if ( ! is_user_logged_in() ) {
	add_action( 'init', 'iv_ajax_init' );
//}

add_filter( 'wp_new_user_notification_email', 'la_fleur_new_user_notification', 10, 3 );
function la_fleur_new_user_notification( $email_data, $user, $blogname ){
	$message  = sprintf( __( 'Username: %s' ), $user->user_login ) . "\r\n\r\n";
	$message .= __( 'Вход: ', 'la-fleur' ) . esc_url( home_url('/#modal-enter-registration') ) . "\r\n";
	
	$email_data['message'] = $message;

	return $email_data;
}

// Login ajax
function callback_ajax_login() {
	//check_ajax_referer( 'ajax-login-nonce', 'security' );

	$info = array();
	$info['user_login'] = trim( $_POST['username'] );
	$info['user_password'] = trim( $_POST['password'] );
	$info['remember'] = true;

	$user_signon = wp_signon( $info, false );
	if ( is_wp_error($user_signon) ){
	  echo json_encode( array('loggedin'=>false, 'message'=>__( 'Неправильный логин или пароль!', 'la-fleur' )) );
	} else {
	  echo json_encode( array('loggedin'=>true, 'message'=>__( 'Идет перенаправление ...', 'la-fleur' )) );
	}
	die();
}

// Registration ajax
function callback_ajax_registration() {
	check_ajax_referer( 'iv-ajax-nonce', 'security' ); 
	
	if( trim( $_POST['password'] ) !== trim( $_POST['check-password'] ) ) {
		echo json_encode( array( 'registration' => false, 'message' => __( 'Please, check fields of password.' ) ) );
		die();
	}
		
	$userdata = array();
	$userdata['user_login'] = sanitize_user( $_POST['login'] );
	$userdata['user_pass'] = $_POST['password'];
	$userdata['user_email'] = sanitize_email( $_POST['email'] );
	$userdata['first_name'] = sanitize_user( $_POST['login'] );//nickname
	$userdata['last_name'] = sanitize_user( $_POST['last-name'] );//first_name last_name	
	//$userdata['phone'] = $_POST['phone'];	
	//$userdata['receive_messages'] = $_POST['checkbox-registration'];

	$user_id = wp_insert_user( $userdata );
	
	if( ! is_wp_error( $user_id ) ) {
		update_user_meta( $user_id, 'phone', $_POST['phone'] );
		update_user_meta( $user_id, 'receive_messages', $_POST['receive_messages'] );
    }
	
	if ( is_wp_error( $user_id ) ){
		echo json_encode( array( 'registration' => false, 'message' => $user_id->get_error_message() ) );
	} else {
		wp_new_user_notification( $user_id, 'admin' );// both
		echo json_encode( array( 
			'registration' => true, 
			'message' => __( 'Регистрация прошла успешно.' ),
			'reg_message' => __( 'Регистрация прошла успешно. Введите email и пароль.' ),
		) );
	}
	die();
}

// Reset password ajax
function callback_ajax_reset_password() {
	check_ajax_referer( 'iv-ajax-nonce', 'security_reset' );
	
	$lost_pass = $_POST['lost_pass'];

	if ( $lost_pass == null ){
		echo json_encode( array( 'reset' => false, 'message'  =>__( 'Пожалуйста заполните поле.', 'la-fleur' ) ) );
	} else {
		if ( is_email( $lost_pass ) ) {
			$username = sanitize_email( $lost_pass );
		} else {
			$username = sanitize_user( $lost_pass );
		}
	}
	$user_forgotten = ajax_lostPassword_retrieve( $username );

	if ( is_wp_error( $user_forgotten ) ) {
		$lostpass_error_messages = $user_forgotten->errors;
		foreach ( $lostpass_error_messages as $error ) {
			$display_errors .= ". $error[0] .";
		}

		echo json_encode( array(
			'reset' => false,
			'message' => $display_errors,
		) );
	} else {
		echo json_encode( array(
			'reset' => true,
			'message' => __( 'Сгенерирован новый пароль.<br>Пароль отправлен на Ваш e-mail адрес.', 'la-fleur' ),
		) );
	}
	die();
}	


function ajax_lostPassword_retrieve( $user_data ) {

	global $wpdb;//, $current_site, $wp_hasher

	$errors = new WP_Error();

	if ( empty( $user_data ) ) {
		$errors->add( 'empty_username', __( 'Пожалуйста укажите email.', 'la-fleur' ) );
	} elseif ( strpos( $user_data, '@' ) ) {
		$user_data = get_user_by( 'email', trim( $user_data ) );
	if ( empty( $user_data ) )
		$errors->add( 'invalid_email', __( 'Ни один пользователь не зарегестрирован под этим e-mail адресом.', 'la-fleur' ) );
	} else {
		$login = trim( $user_data );
		$user_data = get_user_by( 'login', $login );
	}

	if ( $errors->get_error_code() )
		return $errors;

	if ( ! $user_data ) {
		$errors->add( 'invalidcombo', __( 'Неверный e-mail адрес.', 'la-fleur' ) );
		return $errors;
	}

	$user_login = $user_data->user_login;
	$user_email = $user_data->user_email;

	do_action( 'retrieve_password', $user_login );

	$allow = apply_filters( 'allow_password_reset', true, $user_data->ID );

	if ( ! $allow ) {
		return new WP_Error( 'no_password_reset', __( 'Возобновление пароля невозможно. Пожалуйста, свяжитесь с администратором.', 'la-fleur' ) );
	} elseif ( is_wp_error( $allow ) ) {
		return $allow;
	}

	//$key = wp_generate_password( 20, false );
	//do_action( 'retrieve_password_key', $user_login, $key );
	//if ( empty( $wp_hasher ) ) {
		//require_once ABSPATH . 'wp-includes/class-phpass.php';
		//$wp_hasher = new PasswordHash( 8, true );
	//}

	$newpass_gen = wp_generate_password( 8, false );

	$newpass = wp_hash_password($newpass_gen);

	//$hashed = $wp_hasher->HashPassword( $key );
	//$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );
	$wpdb->update( $wpdb->users, array( 'user_pass' => $newpass ), array( 'user_login' => $user_login ) );

	$message = __( 'Кто-то запросил новый пароль для аккаунта с сайта:', 'la-fleur' ) . "\r\n\r\n";
	$message .= network_home_url( '/' ) . "\r\n\r\n";
	$message .= sprintf( __( 'Username: %s' ), $user_login ) . "\r\n\r\n";
	$message .= __( 'Если произошла ошибка, просто проигнорируйте это письмо, и ничего не произойдёт.', 'la-fleur' ) . "\r\n\r\n";
	$message .= sprintf( __( 'Новый сгенерированный пароль: %s', 'la-fleur' ), $newpass_gen ) . "\r\n\r\n";

	if ( is_multisite() ) {
		$blogname = $GLOBALS['current_site']->site_name;
	} else {
		$blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
	}

	$title = sprintf( __( '[%s] Сброс пароля', 'la-fleur' ), $blogname );
	$title = apply_filters( 'retrieve_password_title', $title );
	$message = apply_filters( 'retrieve_password_message', $message, $key );

	if ( $message && ! wp_mail( $user_email, $title, $message ) ) {
		$errors->add( 'noemail', __( 'The e-mail could not be sent.Possible reason: your host may have disabled the mail() function.', 'alimir' ) );

		return $errors;

		wp_die();
	}

	return true;
}
