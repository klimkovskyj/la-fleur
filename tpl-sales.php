<?php
/**
 * Template name: Template Sales
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package  LaFleur
 */

get_header();
?>

       <!-- <section pagination start -->
		<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
		<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
			<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
				<div class="section-pagination__wrapper all-width">
					<ul class="breadcrumb">
						<?php woocommerce_breadcrumb(); ?>
					</ul>
				</div>
			</div>
		<?php } ?>
		<!-- <section pagination end -->
        
        <?php
        $page_sales_products = get_field( 'page_sales_products' );
        $page_sales_products_button = get_field( 'page_sales_products_button' );
        ?>
        <!-- sales product start-->
        <div class="page-sales-rec-pr w_100">
            <div class="page-sales-rec-pr_wr all-width d-f-column-f_c-f_s">

                <!-- slider -->
                <div class="page-sales-rec-pr-slider w_100" id="page-sales-rec-pr-slider">			
                    <!-- item 1 -->
                    <?php
                    if ( ! empty( $page_sales_products ) ) {			
						$posts = get_posts( array(
							'post_type' => 'product',
							'post_status' => 'publish',
							'include' => implode( ',', $page_sales_products ),
							'orderby' => 'post__in',
						) );
						foreach ( $posts as $post ) {
							setup_postdata( $post );
							global $product;
							if ( empty( $product ) || ! $product->is_visible() ) { return; }

							?>
							<div class="page-sales-slider-itm">
								<div class="shop-product-item">
								<?php wc_get_template_part( 'content', 'product' ); ?>
								</div>
							</div>
							<?php
							}
						wp_reset_postdata();
					}
					?>
                </div>

                <!-- btn -->
                <div class="page-test-btn-slider page-test-btn-slider_sales">
					<?php if ( ! empty( $page_sales_products ) ) { ?>
						<!-- left -->
						<button class="beauty-big-slider_btn__arrow slick-arrow" id="btn-left-slider-p-sales">
							<svg width="107" height="14" viewBox="0 0 107 14" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<line x1="106.219" y1="7.23242" x2="1.00139" y2="7.23244" stroke="black"></line>
								<line x1="7.28512" y1="13.086" x2="0.867469" y2="6.66832" stroke="black"></line>
								<line x1="0.86715" y1="7.33199" x2="7.2848" y2="0.914344" stroke="black"></line>
							</svg>
						</button>

						<!-- right -->
						<button class="beauty-big-slider_btn__arrow slick-arrow" id="btn-right-slider-p-sales">
							<svg width="107" height="14" viewBox="0 0 107 14" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<line x1="0.902344" y1="6.76782" x2="106.12" y2="6.76781" stroke="black"></line>
								<line x1="99.836" y1="0.914269" x2="106.254" y2="7.33192" stroke="black"></line>
								<line x1="106.254" y1="6.66825" x2="99.8363" y2="13.0859" stroke="black"></line>
							</svg>
						</button>
					<?php } ?>
                </div>
                <div class="all_link all-width hover_effect-for link-mob-for-sales is_mobile">
					<?php if ( ! empty( $page_sales_products_button ) ) { ?>
						<a href="<?php echo esc_url( home_url( '/catalog/' ) ); ?>">
							<p><?php echo esc_html( $page_sales_products_button ); ?></p>
						</a>
					<?php } ?>
                </div>
            </div>

        </div>
        <!-- sales product end-->

        <!-- other card with sales start -->
        <?php
        $page_sales_sales = get_field( 'page_sales_sales' );
        $page_sales_actions_button = get_field( 'page_sales_actions_button' );
        ?>
        <div class="page-sales-other w_100">
            <div class="page-sales-other_wr all-width d-f-column-f_c-f_s">

                <!-- items  -->
                <div id="sales_posts" class="page-sales-other_items d-f-row-stre-f_s w_100">
					
					<?php
					if ( ! empty( $page_sales_sales ) ) {                   
						$args = array(
							'post_type' => 'sale',
							'post_status' => 'publish',
							'posts_per_page' => 4,
							'post__in' => $page_sales_sales,
							'orderby' => 'post__in',
						);
						
						$sales_query = new WP_Query( $args );
						if( $sales_query->have_posts() ) {
							while ( $sales_query->have_posts() ) {
								$sales_query->the_post();
							
								$sale_content = get_field( 'singl_sale_content' );
								$singl_sale_end = get_field( 'singl_sale_end' );
								?>
								<div class="page-sales-other_item">
									<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="d-f-column-f_s-f_s">
										<div class="page-sales-other_item_img w_100 animation-img-bottom bg-light _anim-items">
											<?php
											if ( $sale_content['image'] ) {
												echo '<img src="'. esc_url( $sale_content['image']['url'] ) .'" alt="'. esc_html( $sale_content['image']['alt'] ) .'">';
											} else {
												echo '<img src="'. LA_FLEUR_THEME_URI .'/img/png/sale-item.png" alt="sale">';
											}
											?>
										</div>
									</a>
									<div class="page-sales-other_item_info d-f-row-c-s_b w_100">
										<div class="page-sales-other_item_info_l d-f-row-c-f_s">
											<?php
											if ( ! empty( $singl_sale_end ) && $singl_sale_end > time() ) {
												echo '<h4>'. la_fleur_get_end_time( $singl_sale_end ) .'</h4>';
												echo '<p>'. __( 'до конца акции', 'la-fleur' ) .'</p>';
											}
											?>
										</div>

										<div class="page-sales-other_item_info_r d-f-row-c-f_end">
											<a href="<?php echo esc_url( get_the_permalink() ); ?>"><?php _e( 'Читать больше', 'la-fleur' ); ?></a>
										</div>
									</div>
								</div>
								<?php
							}
						}
						wp_reset_postdata();
					}
					?>

                </div>

                <!-- btn -->
                <?php			
                if (  $sales_query->max_num_pages > 1 ) {            
                ?>
					<script>
					var true_posts = '<?php echo serialize( $sales_query->query_vars ); ?>';
					var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
					var max_pages = '<?php echo $sales_query->max_num_pages; ?>';				
					</script>				
				<?php
				}
				if ( ! empty( $page_sales_actions_button ) ) {
					echo '<div class="all_link hover_effect-for">';
					echo '<a href="'. esc_url( home_url( '/sales/' ) ) .'">';
					echo '<p id="sales_loadmore">'. esc_html( $page_sales_actions_button ) .'</p>';
					echo '</a>';
					echo '</div>';
				}
				?>
                                
            </div>
        </div>
        <!-- other card with sales start -->

        
<?php
get_footer();
