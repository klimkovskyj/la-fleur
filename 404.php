<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package LaFleur
 */

get_header();
?>
	<style>
		._p-404{
			display: flex;
			flex-direction: column;
			align-items: center;
			justify-content: center;
		}
		._404_img{
			position: absolute;
			right: 0;
			top: 25%;
			bottom: 0;
			margin-top: auto;
			margin-bottom: auto;
			width: 35%;
			height: max-content;
			display: flex;
			flex-direction: row;
			align-items: center;
			justify-content: flex-end;
			padding-top: calc(40px + 60 * ((100vw - 320px) / (1920 - 320)));
		}
		._404_img img{
			width: auto;
			height: auto;
			max-width: 100%;
			max-height: 100%;
		}
		._p-404_desc{
			display: flex;
			flex-direction: column;
			align-items: center;
			justify-content: center;
		}
		._p-404_desc h1{
			font-family: 'Cormorant SC', serif !important;
			font-style: normal;
			font-weight: 500;
			/* font-size: 130px; */
			font-size: calc(32px + 98 * ((100vw - 320px) / (1920 - 320)));
			line-height: 140%;
			letter-spacing: 0.05em;
			text-transform: uppercase;
			text-align: center;
			color: #000000;
			margin-bottom: calc(20px + 15 * ((100vw - 320px) / (1920 - 320)))
		}
		._p-404_desc > p{
			font-weight: 400;
			font-size: calc(16px + 2 * ((100vw - 320px) / (1920 - 320)));
			line-height: 160%;
			letter-spacing: 0.1em;
			text-transform: uppercase;
			color: #000000;
			margin-bottom: calc(20px + 15 * ((100vw - 320px) / (1920 - 320)));
			text-align: center;
		}
		._p-404_desc .hover_effect-for a:hover::after{
			background: #FFFFFF;
		}

	</style>
	
	<!-- <section pagination start -->
	<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
	<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
		<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
			<div class="section-pagination__wrapper all-width">
				<ul class="breadcrumb">
					<?php woocommerce_breadcrumb(); ?>
				</ul>
			</div>
		</div>
	<?php } ?>
	<!-- <section pagination end -->
	
	<div class="page-info-method w_100" style="background: #FAF3D9; position: relative;">
        <div class="page-info-method_wr all-width _p-404">

			
				<div class="_p-404_desc w_100">
					<h1><?php esc_html_e( 'Ooops', 'la-fleur' ); ?></h1>
					<p><?php esc_html_e( 'Этой страницы не существует', 'la-fleur' ); ?></p>

					<div class="all_link hover_effect-for">
						<a href="<?php echo esc_url( home_url() ); ?>">
							<p><?php esc_html_e( 'на главную', 'la-fleur' ); ?></p>
						</a>
                	</div>
				</div>
				
		</div>

		<div class="_404_img">
			<img src="<?php echo LA_FLEUR_THEME_URI; ?>/img/png/img_404.png" alt="404">
		</div>
	</div>

<?php
get_footer();
