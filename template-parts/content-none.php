<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package LaFleur
 */

?>

<section class="no-results not-found">
	
	<div class="page-info-method_title w_100">
		<h2><?php esc_html_e( 'Ничего не найдено', 'la-fleur' ); ?></h2>
	</div>

	<div class="page-about-info_r_description w_100">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Готовы опубликовать свой первый пост? <a href="%1$s">Начните здесь</a>.', 'la-fleur' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>

			<p><?php esc_html_e( 'Извините, но ничего не соответствовало вашим условиям поиска. Пожалуйста, попытайтесь снова с другими ключевыми словами.', 'la-fleur' ); ?></p>
			<?php
			get_search_form();

		else :
			?>

			<p><?php esc_html_e( 'Похоже, мы не можем найти то, что вы ищете. Возможно, поиск поможет.', 'la-fleur' ); ?></p>
			<?php
			get_search_form();

		endif;
		?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
