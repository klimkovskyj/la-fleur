<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package LaFleur
 */

get_header();
?>

	<!-- <section pagination start -->
	<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
	<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
		<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
			<div class="section-pagination__wrapper all-width">
				<ul class="breadcrumb">
					<?php woocommerce_breadcrumb(); ?>
				</ul>
			</div>
		</div>
	<?php } ?>
	<!-- <section pagination end -->
	
	<div class="page-info-method w_100">
        <div class="page-info-method_wr all-width d-f-column-f_s-f_s">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation(
				array(
					'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'la-fleur' ) . '</span> <span class="nav-title">%title</span>',
					'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'la-fleur' ) . '</span> <span class="nav-title">%title</span>',
				)
			);

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</div>
	</div>

<?php
get_footer();
