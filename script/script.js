let timerStart = 0;
let timerClosModal = 5000;
// show menu when hover "Catalog"
let btnCatalog;
if (document.getElementById('header_catalog') != null && document.getElementById('header_catalog') != undefined) {
    btnCatalog = document.getElementById('header_catalog');
    btnCatalog.onclick = showMenuForCatalog;
}

let modalMenuCatalog;
if (document.getElementById('show_menu-catalog') != null && document.getElementById('show_menu-catalog') != null) {
    modalMenuCatalog = document.getElementById('show_menu-catalog');
}

let blockHeaderBottom;
if (document.querySelector('.header_bottom') != null && document.querySelector('.header_bottom') != undefined) {
    blockHeaderBottom = document.querySelector('.header_bottom');
}

// container in sub menu for catalog
let containerSubMenuCatalog;
if (document.querySelector('.show_menu-catalog_container') != null && document.querySelector('.show_menu-catalog_container') != undefined) {
    containerSubMenuCatalog = document.querySelector('.show_menu-catalog_container');
}

// show sub menu
let allSubMenuBtn;
if (document.querySelectorAll('.show_menu-catalog_item_btn') != null && document.querySelectorAll('.show_menu-catalog_item_btn') != undefined) {
    allSubMenuBtn = document.querySelectorAll('.show_menu-catalog_item_btn');
}

// show img for header sub menu
let allLinkSubMenu;
if (document.querySelectorAll('.link_with_img_in_sub') != null && document.querySelectorAll('.link_with_img_in_sub') != undefined) {
    allLinkSubMenu = document.querySelectorAll('.link_with_img_in_sub');
}

// set width for sub menu in catalog + scroll
let widthForSubMenuContainer;
if (document.querySelector('.show_menu-catalog_container') != null && document.querySelector('.show_menu-catalog_container') != undefined) {
    widthForSubMenuContainer = document.querySelector('.show_menu-catalog_container');
}

// pass in form registration
let pass1;
if (document.getElementById('password-registration') != null && document.getElementById('password-registration') != undefined) {
    pass1 = document.getElementById('password-registration');
}

let pass2
if (document.getElementById('repeat-password-registration') != null && document.getElementById('repeat-password-registration') != undefined) {
    pass2 = document.getElementById('repeat-password-registration');
}

// modal enter registration
let modalEnterRegistration;
if (document.getElementById('modal-enter-registration') != null && document.getElementById('modal-enter-registration') != undefined) {
    modalEnterRegistration = document.getElementById('modal-enter-registration');
}

//modal all message
let modalMessage;
if (document.getElementById('modal-all-message') != null && document.getElementById('modal-all-message') != undefined) {
    modalMessage = document.getElementById('modal-all-message');
}

// modal remember password
let modalRememberPass;
if (document.getElementById('modal-remember-password') != null && document.getElementById('modal-remember-password') != undefined) {
    modalRememberPass = document.getElementById('modal-remember-password');
}

// modal new password
let modalNewPass;
if (document.getElementById('modal-new-password') != null && document.getElementById('modal-new-password') != undefined) {
    modalNewPass = document.getElementById('modal-new-password');
}

// new password
let newpass1,
    newpass2;
if (document.getElementById('new-password') != undefined && document.getElementById('new-password') != null) {
    newpass1 = document.getElementById('new-password');
}
if (document.getElementById('registration-repeat-new-password') != undefined && document.getElementById('registration-repeat-new-password') != null) {
    newpass2 = document.getElementById('registration-repeat-new-password');
}

// modal shop card
let modalShopCard;
if (document.getElementById('modal-shop-card') != null && document.getElementById('modal-shop-card') != undefined) {
    modalShopCard = document.getElementById('modal-shop-card');
}

//modal shop filter
let modalShopFilter;
if (document.getElementById('section-filter-shop') != null && document.getElementById('section-filter-shop') != undefined) {
    modalShopFilter = document.getElementById('section-filter-shop');
}

// script for mobile sub menu
let allBtnMobileSub;
if (document.querySelectorAll('.btn-for-section-mobile-sub-menu') != null && document.querySelectorAll('.btn-for-section-mobile-sub-menu') != undefined) {
    allBtnMobileSub = document.querySelectorAll('.btn-for-section-mobile-sub-menu');
}

let allProductDescInfoTab;
if (document.querySelectorAll('.s-pr-desc_item button') != null && document.querySelectorAll('.s-pr-desc_item button') != undefined) {
    allProductDescInfoTab = document.querySelectorAll('.s-pr-desc_item button');
}

let timeOutEnter,
    timeOutEnterError,
    timeOutRegistration,
    timeOutRegistrationError,
    timeOutNewPassword,
    timeOutNewPasswordError,
    timeOutRememberPassword,
    timeOutRememberPasswordError,
    timeOutthankyou,
    timeOutthankyouError;

// this block add class when block is active and user can see this block
let animItems;
if (document.querySelectorAll('._anim-items') != null && document.querySelectorAll('._anim-items') != undefined) {
    animItems = document.querySelectorAll('._anim-items');
}

// section_menu-catalog_link
document.addEventListener('DOMContentLoaded', function () {

    window.addEventListener('scroll', animOnScroll);

    setTimeout(() => {
        animOnScroll();
    }, 300)

    // if body contains class when open modal or hash contains
    //modal enter / registration
    if (window.location.hash == "#modal-enter-registration") {
        setTimeout(function () {
            openModalEnterRegistration();
            document.body.classList.add('show-modal-enter-registration');
        }, 100);
    }
    if (document.body.classList.contains('show-modal-enter-registration') == true) {
        setTimeout(function () {
            openModalEnterRegistration();
            window.location.hash == "#modal-enter-registration";
        }, 100);
    }

    // modal new password
    if (window.location.hash == "#modal-new-password") {
        setTimeout(function () {
            openModalNewPassword();
            document.body.classList.add('show-modal-new-password');
        }, 100);
    }
    if (document.body.classList.contains('show-modal-new-password') == true) {
        setTimeout(function () {
            openModalNewPassword();
            window.location.hash == "#modal-new-password";
        }, 100);
    }

    // modal remember password
    if (window.location.hash == "#modal-remember-password") {
        setTimeout(function () {
            openModalRememberPassword();
            document.body.classList.add('show-modal-remember-password');
        }, 100);
    }
    if (document.body.classList.contains('show-modal-remember-password') == true) {
        setTimeout(function () {
            openModalRememberPassword();
            window.location.hash == "#modal-remember-password";
        }, 100);
    }

    // modal filter mobile
    if (window.location.hash == "#modal-shop-filter") {
        setTimeout(function () {
            openModalShopFilter();
            document.body.classList.add('show-modal-shop-filter');
        }, 100);
    }
    if (document.body.classList.contains('show-modal-shop-filter') == true) {
        setTimeout(function () {
            openModalShopFilter();
            window.location.hash == "#modal-shop-filter";
        }, 100);
    }

    // modal shop card
    if (window.location.hash == "#modal-shop-card") {
        setTimeout(function () {
            openShopCard();
            document.body.classList.add('show-modal-shop-card');
        }, 100);
    }
    if (document.body.classList.contains('show-modal-shop-card') == true) {
        setTimeout(function () {
            openShopCard();
            window.location.hash == "#modal-shop-card";
        }, 100);
    }

    document.addEventListener('mouseover', function (e) {
        // for sub menu in header
        if ((!btnCatalog.contains(e.target) && !btnCatalog.children[0].contains(e.target) && !blockHeaderBottom.contains(e.target) && !containerSubMenuCatalog.contains(e.target))) {
            closeMenuForCatalog();
        }
    });

    allSubMenuBtn[0].parentElement.classList.add('active_show_sub-menu');
    for (let item of allSubMenuBtn) {
        item.onclick = showSubMenu;
    }

    for (let item of allLinkSubMenu) {
        item.onmouseenter = showImgForSubMenu;
    }

    // btn show new on main page
    if (document.getElementById('btn-main-filter-news') != null && document.getElementById('btn-main-filter-news') != undefined) {
        document.getElementById('btn-main-filter-news').onclick = function () {
            document.getElementById('main-brand_show-news').style.display = 'flex';
            document.getElementById('main-brand_show-hit').style.display = 'none';

            if (this.classList.contains('active_filter-main') == false) {
                this.classList.add('active_filter-main');
            }

            if (document.getElementById('btn-main-filter-hit').classList.contains('active_filter-main') == true) {
                document.getElementById('btn-main-filter-hit').classList.remove('active_filter-main');
            }

            animOnScroll();
        };
    }

    // btn show hit on main page
    if (document.getElementById('btn-main-filter-hit') != null && document.getElementById('btn-main-filter-hit') != undefined) {
        document.getElementById('btn-main-filter-hit').onclick = function () {
            document.getElementById('main-brand_show-news').style.display = 'none';
            document.getElementById('main-brand_show-hit').style.display = 'flex';

            if (this.classList.contains('active_filter-main') == false) {
                this.classList.add('active_filter-main');
            }

            if (document.getElementById('btn-main-filter-news').classList.contains('active_filter-main') == true) {
                document.getElementById('btn-main-filter-news').classList.remove('active_filter-main');
            }
            if (window.innerWidth <= 1024) {
                // slider mobile brand
                $('#main-brand_show-hit_slider').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    speed: 500,
                    fade: true,
                    cssEase: 'linear',
                    prevArrow: $('#btn-prev_mobile-perfumency-hit'),
                    nextArrow: $('#btn-next_mobile-perfumency-hit'),
                });
            }

            animOnScroll();
        };
    };

    // ********************************************************************************
    // script validate all form start
    // ********************************************************************************
    let timerStart = 0;

    //form validate when user keypress simple 
    let sectionInput;
    if (document.querySelectorAll('.section-all-input') != null) {
        sectionInput = document.querySelectorAll('.section-all-input');
    }

    for (let item of sectionInput) {
        item.addEventListener('click', getLabelInput);
    }

    //following  to news
    let formFollowing;
    if (document.getElementById('following_to_news') != null) {
        formFollowing = document.getElementById('following_to_news');

        formFollowing.onsubmit = (event) => {
            event.preventDefault();
            let validM = false;

            for (let item of formFollowing) {
                if (validMail(item) == true && item.type == 'email') {
                    validM = true;
                }
            }

            let allValid = validM;

            if (allValid == false) {
                openModalAllMessage('Oooops!', iv_ajax_object.pass_error);
                setTimeout(function () {
                    closeModalAllMessage();
                }, timerClosModal);
            } else {
				// Custom code
				followingToNews();
                //openModalAllMessage(iv_ajax_object.thank, iv_ajax_object.thank_msg);
                //if (document.body.classList.contains('show-modal-message') == true) {
                    //setTimeout(function () {
                        //closeModalAllMessage();
                    //}, timerClosModal);
                //}
            }

            return allValid;
        }
    }

    // form enter
    let formEnter;
    if (document.getElementById('form-for-enter') != null) {
        formEnter = document.getElementById('form-for-enter');

        formEnter.onsubmit = (event) => {
            event.preventDefault();
            let validMailEnter = false,
                validPassEnter = false;

            for (let item of formEnter) {

                if (validMail(item) == true && item.type == 'email') {
                    validMailEnter = true;
                }

                if (validPasswordLogin(item) == true && item.type == 'password') {
                    validPassEnter = true;
                }
            }

            let allValidEnter = validMailEnter && validPassEnter;

            if (allValidEnter == false) {
                openModalAllMessage('Oooops!', iv_ajax_object.pass_error);
                document.body.classList.add('error-valid-form');
                closeModalEnterRegistration();
                timeOutEnterError = setTimeout(function () {

                    if (document.body.classList.contains('error-valid-form') == true && document.body.classList.contains('show-modal-message') == true) {
                        closeModalAllMessage();
                        document.body.classList.remove('error-valid-form');
                        openModalEnterRegistration();
                    }
                }, timerClosModal);
            } else {
                if (document.body.classList.contains('error-valid-form') == true) {
                    document.body.classList.remove('error-valid-form');
                }
                //closeModalEnterRegistration();
                //openModalAllMessage(iv_ajax_object.thank, iv_ajax_object.thank_msg);
                //if (document.body.classList.contains('show-modal-message') == true) {
                    //timeOutEnter = setTimeout(function () {

                        //closeModalAllMessage();
                    //}, timerClosModal);
                //}
                // CUSTOM CODE
                ivAjaxLogin();
            }

            return validPassEnter;
        }
    }

    // form-for-registration
    let formRegistration;
    if (document.getElementById('form-for-registration') != null) {
        formRegistration = document.getElementById('form-for-registration');

        formRegistration.onsubmit = (event) => {
            event.preventDefault();
            let validMails = false;
            let validPass = false;
            let validValuePass = false;
            let validC = false,
                validE = false,
                validThisName = false;

            for (let item of formRegistration) {
                if (validName(item) == true && item.type == 'text') {
                    validThisName = true;
                }

                if (validMail(item) == true && item.type == 'email') {
                    validMails = true;
                }

                if (validPasswordRegistration(item) == true && item.type == 'password') {
                    validPass = true;
                }

                if (document.getElementById('password-registration').value == document.getElementById('repeat-password-registration').value) {
                    validValuePass = true
                }

                if (validCheckbox(item) == true && item.type == 'checkbox') {
                    validC = true;
                }

                if (validMail(item) == true && item.type == 'email') {
                    validE = true;
                }
            }

            let allValid = validMails && validPass && validValuePass && validC && validE;



            if (allValid == false) {
                openModalAllMessage('Oooops!', iv_ajax_object.pass_error);
                document.body.classList.add('error-valid-form');
                closeModalEnterRegistration();
                timeOutRegistration = setTimeout(function () {
                    if (document.body.classList.contains('error-valid-form') == true && document.body.classList.contains('show-modal-message') == true) {
                        closeModalAllMessage();
                        document.body.classList.remove('error-valid-form');
                        openModalEnterRegistration();
                    }
                }, timerClosModal);
            } else {
                if (document.body.classList.contains('error-valid-form') == true) {
                    document.body.classList.remove('error-valid-form');
                }
                //closeModalEnterRegistration();
                //openModalAllMessage(iv_ajax_object.thank, iv_ajax_object.thank_msg);
                //if (document.body.classList.contains('show-modal-message') == true) {
                    //timeOutRegistrationError = setTimeout(function () {
                        //closeModalAllMessage();
                    //}, timerClosModal);
                //}
                // Custom code
                ivAjaxRegistration();
            }

            return allValid;
        }
    }

    // form remember password
    let formRememberPassword;
    if (document.getElementById('form-for-remember-password') != null && document.getElementById('form-for-remember-password') != undefined) {
        formRememberPassword = document.getElementById('form-for-remember-password');

        formRememberPassword.onsubmit = (event) => {
            event.preventDefault();
            let validMailRemember = false;

            for (let item of formRememberPassword) {

                if (validMail(item) == true && item.type == 'email') {
                    validMailRemember = true;
                }

            }

            let allValidRemember = validMailRemember;

            if (validMailRemember == false) {
                openModalAllMessage('Oooops!', iv_ajax_object.pass_error);
                document.body.classList.add('error-form-remeber-password');
                closeModalRememberPassword();
                timeOutRememberPasswordError = setTimeout(function () {

                    if (document.body.classList.contains('error-form-remeber-password') == true && document.body.classList.contains('show-modal-message') == true) {
                        closeModalAllMessage();
                        document.body.classList.remove('error-form-remeber-password');
                        openModalRememberPassword();
                    }
                }, timerClosModal);
            } else {
                if (document.body.classList.contains('error-form-remeber-password') == true) {
                    document.body.classList.remove('error-form-remeber-password');
                }
                //closeModalRememberPassword();
                //openModalAllMessage(iv_ajax_object.thank, iv_ajax_object.thank_msg);
                //if (document.body.classList.contains('show-modal-message') == true) {

                    //timeOutRememberPassword = setTimeout(function () {
                        //closeModalAllMessage();
                    //}, timerClosModal);
                //}
                // Custom code
                ivAjaxResetPassword();
            }

            return allValidRemember;
        }
    }

    // form new password
    let formNewPassword;
    if (document.getElementById('form-for-new-password') != null && document.getElementById('form-for-new-password') != undefined) {
        formNewPassword = document.getElementById('form-for-new-password');

        formNewPassword.onsubmit = (event) => {
            event.preventDefault();
            let validPassNew = false;
            let validValuePassNew = false;

            for (let item of formNewPassword) {

                if (validPasswordNew(item) == true && item.type == 'password') {
                    validPassNew = true;
                }

                if (document.getElementById('new-password').value == document.getElementById('registration-repeat-new-password').value) {
                    validValuePassNew = true
                }
            }

            let allValidNew = validPassNew && validValuePassNew;

            if (allValidNew == false) {
                openModalAllMessage('Oooops!', iv_ajax_object.pass_error);
                document.body.classList.add('error-form-new-password');
                closeModalNewPassword();
                timeOutNewPasswordError = setTimeout(function () {

                    if (document.body.classList.contains('error-form-new-password') == true && document.body.classList.contains('show-modal-message') == true) {
                        closeModalAllMessage();
                        document.body.classList.remove('error-form-new-password');
                        openModalNewPassword();
                    }
                }, timerClosModal);
            } else {
                if (document.body.classList.contains('error-form-new-password') == true) {
                    document.body.classList.remove('error-form-new-password');
                }
                closeModalRememberPassword();
                openModalAllMessage(iv_ajax_object.thank, iv_ajax_object.thank_msg);
                if (document.body.classList.contains('show-modal-message') == true) {

                    timeOutNewPassword = setTimeout(function () {
                        closeModalAllMessage();
                    }, timerClosModal);
                }
            }

            return allValidNew;
        }
    }

    // form order step 1
    let formOrderStep1;
    if (document.getElementById('order-step-1') != null && document.getElementById('order-step-1') != undefined) {
        formOrderStep1 = document.getElementById('order-step-1');

        let linkToStep2;
        if (document.getElementById('link-to-order-step-2') != null && document.getElementById('link-to-order-step-2') != undefined) {
            linkToStep2 = document.getElementById('link-to-order-step-2');

            linkToStep2.onclick = function () {
                let validN = false;
                let validSN = false;
                let validE = false;
                let validP = false;

                for (let item of formOrderStep1) {
                    if (validPhone(item) == true && item.type == 'tel') {
                        validP = true;
                    }
                    if (validName(item) == true && item.type == 'text' && item.id == 'name-order-st-1') {
                        validN = true;
                    }
                    if (validName(item) == true && item.type == 'text' && item.id == 'surname-order-st-1') {//account_last_name
                        validSN = true;
                    }
                    if (validMail(item) == true && item.type == 'email') {
                        validE = true;
                    }
                }

                let allValid = validP && validN && validSN && validE;

                if (allValid == false) {
                    openModalAllMessage('Oooops!', iv_ajax_object.pass_error);
                    document.body.classList.add('error-form-order-step-1');
                    timeOutNewPasswordError = setTimeout(function () {

                        if (document.body.classList.contains('error-form-order-step-1') == true && document.body.classList.contains('show-modal-message') == true) {
                            closeModalAllMessage();
                            document.body.classList.remove('error-form-order-step-1');
                        }
                    }, timerClosModal);
                }
                // else {
                //     if (document.body.classList.contains('error-form-order-step-1') == true) {
                //         document.body.classList.remove('error-form-order-step-1');
                //     }
                //     closeModalRememberPassword();
                //     openModalAllMessage(iv_ajax_object.thank, iv_ajax_object.thank_msg);
                //     if (document.body.classList.contains('show-modal-message') == true) {

                //         timeOutNewPassword = setTimeout(function () {
                //             closeModalAllMessage();
                //         }, timerClosModal);
                //     }
                // }

                return allValid;
            }
        }
    }

    let orderStep2;
    if (document.getElementById('form-order-step-2') != null && document.getElementById('form-order-step-2') != undefined) {
        orderStep2 = document.getElementById('form-order-step-2');

        //orderStep2.onsubmit = function (event) {
            //event.preventDefault();

            //let validCity = false;
            //let validDelivery = false;
            //let validPay = false;

            //for (let item of orderStep2) {
                //if (item.type == 'text' && item.id == 'city-order-st-2' && item.value != '') {
                    //validCity = true;
                //}

                //let step2Delivery;
                //if (document.getElementById('order-delivery') != null && document.getElementById('order-delivery') != undefined) {
                    //step2Delivery = document.getElementById('order-delivery');
                    //if (step2Delivery.value !== '') {
                        //validDelivery = true;
                    //}
                //}


                //let step2Pay;
                //if (document.getElementById('order-paymend-method') != null && document.getElementById('order-paymend-method') != undefined) {
                    //step2Pay = document.getElementById('order-paymend-method');
                    //if (step2Pay.value !== '') {
                        //validPay = true;
                    //}
                //}
            //}

            //let allValid = validCity && validDelivery && validPay;

            //if (allValid == false) {
                //openModalAllMessage('Oooops!', iv_ajax_object.pass_error);
                //document.body.classList.add('error-form-order-step-2');
                //timeOutNewPasswordError = setTimeout(function () {

                    //if (document.body.classList.contains('error-form-order-step-1') == true && document.body.classList.contains('show-modal-message') == true) {
                        //closeModalAllMessage();
                        //document.body.classList.remove('error-form-order-step-2');
                    //}
                //}, timerClosModal);
            //}
            //else {
                //if (document.body.classList.contains('error-form-order-step-2') == true) {
                    //document.body.classList.remove('error-form-order-step-2');
                //}
                //closeModalRememberPassword();
                //openModalAllMessage(iv_ajax_object.thank, iv_ajax_object.thank_msg);
                //if (document.body.classList.contains('show-modal-message') == true) {

                    //timeOutNewPassword = setTimeout(function () {
                        //closeModalAllMessage();
                    //}, timerClosModal);
                //}
            //}

            //return allValid;
       // }

    }

    // personal-data
    let formCabinetData;
    if (document.getElementById('personal-data') != null && document.getElementById('personal-data') != undefined) {
        formCabinetData = document.getElementById('personal-data');

        formCabinetData.onsubmit = function (event) {
            event.preventDefault();
            let validN = false;
            let validCity = false;
            let validSN = false;
            let validE = false;
            let validP = false;

            for (let item of formCabinetData) {
                if (validPhone(item) == true && item.type == 'tel') {
                    validP = true;
                }
                if (validName(item) == true && item.type == 'text' && item.id == 'name-cabinet-data') {
                    validN = true;
                }
                if (validName(item) == true && item.type == 'text' && item.id == 'city-cabinet-data') {
                    validCity = true;
                }
                if (validName(item) == true && item.type == 'text' && item.id == 'surname-cabinet-data') {
                    validSN = true;
                }
                if (validMail(item) == true && item.type == 'email') {
                    validE = true;
                }
            }

            let allValid = validP && validN && validSN && validE && validCity;

            if (allValid == false) {
                openModalAllMessage('Oooops!', iv_ajax_object.pass_error);
                document.body.classList.add('error-form-order-step-1');
                timeOutNewPasswordError = setTimeout(function () {

                    if (document.body.classList.contains('error-form-cabinet-data') == true && document.body.classList.contains('show-modal-message') == true) {
                        closeModalAllMessage();
                        document.body.classList.remove('error-form-cabinet-data');
                    }
                }, timerClosModal);
            }
            else {
                if (document.body.classList.contains('error-form-cabinet-data') == true) {
                    document.body.classList.remove('error-form-cabinet-data');
                }
                openModalAllMessage('Спасибо!', 'Ваши данные успешно изменены.');
                if (document.body.classList.contains('show-modal-message') == true) {
                    timeOutNewPassword = setTimeout(function () {
                        closeModalAllMessage();
                    }, timerClosModal);
                }
            }

            return allValid;
        }
    }

    // new Pssword Cabinet
    let newPasswordCabinet;
    if (document.getElementById('cabinet-add-new-password') != null && document.getElementById('cabinet-add-new-password') != undefined) {
        newPasswordCabinet = document.getElementById('cabinet-add-new-password');

        newPasswordCabinet.onsubmit = function (event) {
            // Custom code event.preventDefault();
            let validPass = false;
            let validReapeatPass = false;

            let passCabinet1;
            if (document.getElementById('cabinet-pass') != null && document.getElementById('cabinet-pass') != undefined) {
                passCabinet1 = document.getElementById('cabinet-pass');
            }
            let passCabinet2;
            if (document.getElementById('cabinet-repeat-pass') != null && document.getElementById('cabinet-repeat-pass') != undefined) {
                passCabinet2 = document.getElementById('cabinet-repeat-pass');
            }

            // Custom code  && passCabinet1.value.length >= 6
            if (passCabinet1.value != '') {
                passCabinet1.classList.remove('input-mail-error');
                let divWithMessage = passCabinet1.parentElement.children[2];
                divWithMessage.style.display = 'none';
                divWithMessage.style.visibility = 'hidden';
                divWithMessage.style.opacity = '0';
                validPass = true;
            } else {
                passCabinet1.classList.add('input-mail-error');
                let divWithMessage = passCabinet1.parentElement.children[2];
                divWithMessage.style.display = 'flex';
                divWithMessage.style.visibility = 'visible';
                divWithMessage.style.opacity = '1';
                validPass = false;
            }

            // vali repeat pass
            // Custom code passCabinet1.value == passCabinet2.value &&
            if ( passCabinet2.value != null && passCabinet2.value.length >= 6) {
                passCabinet2.classList.remove('input-mail-error');
                let divWithMessage = passCabinet2.parentElement.children[2];
                divWithMessage.style.display = 'none';
                divWithMessage.style.visibility = 'hidden';
                divWithMessage.style.opacity = '0';
                validReapeatPass = true;
            } else {
                passCabinet2.classList.add('input-mail-error');
                let divWithMessage = passCabinet2.parentElement.children[2];
                divWithMessage.style.display = 'flex';
                divWithMessage.style.visibility = 'visible';
                divWithMessage.style.opacity = '1';
                validReapeatPass = false;
            }

            let allValid = validPass && validReapeatPass;

            if (allValid == false) {
                openModalAllMessage('Oooops!', iv_ajax_object.pass_error);
                document.body.classList.add('error-form-order-step-1');
                timeOutNewPasswordError = setTimeout(function () {

                    if (document.body.classList.contains('error-form-cabinet-data') == true && document.body.classList.contains('show-modal-message') == true) {
                        closeModalAllMessage();
                        document.body.classList.remove('error-form-cabinet-data');
                    }
                }, timerClosModal);
            }
            else {
                if (document.body.classList.contains('error-form-cabinet-data') == true) {
                    document.body.classList.remove('error-form-cabinet-data');
                }
                // Custom Code
                //openModalAllMessage(iv_ajax_object.thank, iv_ajax_object.pass_changed);
                //if (document.body.classList.contains('show-modal-message') == true) {
                    //timeOutNewPassword = setTimeout(function () {
                        //closeModalAllMessage();
                    //}, timerClosModal);
                //}
            }
            return allValid;
        }
    }

    let formTest;
    if (document.getElementById('page-test') != null && document.getElementById('page-test') != undefined) {
        formTest = document.getElementById('page-test');

        formTest.onsubmit = function (event) {
			// Custom code
            //event.preventDefault();
        }
    }

    // ********************************************************************************
    // script validate all form end
    // ********************************************************************************

    // ********************************************************************************
    // script for all modal open / close start
    // ********************************************************************************
    document.addEventListener('click', function (e) {
        // close modal enter / registration when click other smt
        if (document.body.classList.contains('show-modal-enter-registration') == true) {
            if (!document.querySelector(".section-modal-enter-registration").contains(e.target) && !document.getElementById('btn-open_modal-cabinet').contains(e.target) && !document.getElementById('btn_personal_cabinet-mobile').contains(e.target)) {
                closeModalEnterRegistration();
                scrollEnable();
            }
        }

        if (document.body.classList.contains('show-modal-remember-password') == true) {
            if (!document.querySelector(".section-modal-remember-password").contains(e.target) && !document.getElementById('close-modal-remember-password').contains(e.target)) {
                closeModalRememberPassword();
                scrollEnable();
            }
        }

        if (document.body.classList.contains('show-modal-new-password') == true) {
            if (!document.querySelector(".section-modal-new-password").contains(e.target) && !document.getElementById('close-modal-new-password').contains(e.target)) {
                closeModalNewPassword();
                scrollEnable();
            }
        }

        if (document.body.classList.contains('show-modal-shop-card') == true) {
            if (!document.querySelector(".section-modal-shop-card").contains(e.target) && !document.getElementById('close-modal-shop-card').contains(e.target) && !document.getElementById('btn-open_modal-shop').contains(e.target) && !document.getElementById('btn_shop_art-mobile').contains(e.target)) {
                closeShopCard();
                scrollEnable();
            }
        }

        if (document.querySelectorAll('.section-filter-itm') != null && document.querySelectorAll('.section-filter-itm') != undefined) {
            // filter 1
            let fltr1;
            if (document.getElementById('this-filter-1') != null && document.getElementById('this-filter-1') != undefined) {
                fltr1 = document.getElementById('this-filter-1');
                if (!fltr1.contains(e.target)) {
                    fltr1.classList.remove('active-show-main-dropdown');
                }
            }
            // filter 2
            let fltr2;
            if (document.getElementById('this-filter-2') != null && document.getElementById('this-filter-1') != undefined) {
                fltr2 = document.getElementById('this-filter-2');
                if (!fltr2.contains(e.target)) {
                    fltr2.classList.remove('active-show-main-dropdown');
                }
            }
            // filter 3
            let fltr3;
            if (document.getElementById('this-filter-3') != null && document.getElementById('this-filter-1') != undefined) {
                fltr3 = document.getElementById('this-filter-3');
                if (!fltr3.contains(e.target)) {
                    fltr3.classList.remove('active-show-main-dropdown');
                }
            }
            // filter 4
            let fltr4;
            if (document.getElementById('this-filter-4') != null && document.getElementById('this-filter-4') != undefined) {
                fltr4 = document.getElementById('this-filter-4');
                if (!fltr4.contains(e.target)) {
                    fltr4.classList.remove('active-show-main-dropdown');
                }
            }
        }
    });

    // btn open modal enter and registration
    if (document.getElementById('btn-open_modal-cabinet') != null && document.getElementById('btn-open_modal-cabinet') != undefined) {
        document.getElementById('btn-open_modal-cabinet').onclick = openModalEnterRegistration;
    }
    if (document.getElementById('close-modal-enter-registration') != null && document.getElementById('close-modal-enter-registration') != undefined) {
        document.getElementById('close-modal-enter-registration').onclick = closeModalEnterRegistration();
    }

    // show tab when click enter
    if (document.getElementById('tab-modal-enter') != null && document.getElementById('tab-modal-enter') != undefined) {
        document.getElementById('tab-modal-enter').onclick = function () {
            this.classList.add('active-this-tab-in-modal');
            document.getElementById('tad-only-enter_modal').style.display = 'flex';

            // if (document.getElementById('tab-enter-registration').classList.contains('active-this-tab-in-modal') == true) {
            document.getElementById('tab-modal-registration').classList.remove('active-this-tab-in-modal');
            document.getElementById('tad-only-registration_modal').style.display = 'none';
            // }
        }
    }

    // show tab when click registration
    if (document.getElementById('tab-modal-registration') != null && document.getElementById('tab-modal-registration') != undefined) {
        document.getElementById('tab-modal-registration').onclick = function () {
            this.classList.add('active-this-tab-in-modal');
            document.getElementById('tad-only-registration_modal').style.display = 'flex';

            // if (document.getElementById('tab-modal-enter').classList.contains('tab-modal-enter') == true) {
            document.getElementById('tab-modal-enter').classList.remove('active-this-tab-in-modal');
            document.getElementById('tad-only-enter_modal').style.display = 'none';
            // }
        }
    }

    if (document.getElementById('close-modal-all-message') != null && document.getElementById('close-modal-all-message') != undefined) {
        document.getElementById('close-modal-all-message').onclick = function () {
            if (document.body.classList.contains('error-valid-form') == true && document.body.classList.contains('show-modal-message') == true) {
                closeModalAllMessage();

                clearInterval(timeOutEnter);
                clearInterval(timeOutEnterError);

                clearInterval(timeOutRegistration);
                clearInterval(timeOutRegistration);

                setTimeout(function () {
                    openModalEnterRegistration();
                }, 100);

                if (document.body.classList.contains('error-valid-form') == true) {
                    document.body.classList.remove('error-valid-form');
                }
            } else if (document.body.classList.contains('show-modal-message') == true && document.body.classList.contains('error-form-remeber-password') == true) {
                closeModalAllMessage();

                clearInterval(timeOutRememberPassword);
                clearInterval(timeOutRememberPasswordError);

                setTimeout(function () {
                    openModalRememberPassword();
                }, 100);

                if (document.body.classList.contains('error-form-remeber-password') == true) {
                    document.body.classList.remove('error-form-remeber-password');
                }
                // error-form-new-password
            } else if (document.body.classList.contains('show-modal-message') == true && document.body.classList.contains('error-form-new-password') == true) {
                closeModalAllMessage();

                clearInterval(timeOutNewPassword);
                clearInterval(timeOutNewPasswordError);

                setTimeout(function () {
                    openModalNewPassword();
                }, 100);

                if (document.body.classList.contains('error-form-new-password') == true) {
                    document.body.classList.remove('error-form-new-password');
                }
            } else {
                closeModalAllMessage();
            }
        }
    }

    // show modal remember password
    if (document.getElementById('btn-show-remember-password') != null && document.getElementById('btn-show-remember-password') != undefined) {
        document.getElementById('btn-show-remember-password').onclick = function () {
            closeModalEnterRegistration();
            setTimeout(function () {
                openModalRememberPassword();
            }, 100)
        }
    }

    if (document.getElementById('close-modal-remember-password') != null && document.getElementById('close-modal-remember-password') != undefined) {
        document.getElementById('close-modal-remember-password').onclick = closeModalRememberPassword;
    }

    if (document.getElementById('close-modal-new-password') != null && document.getElementById('close-modal-new-password') != undefined) {
        document.getElementById('close-modal-new-password').onclick = closeModalNewPassword;
    }

    // show shop card
    if (document.getElementById('btn-open_modal-shop') != null && document.getElementById('btn-open_modal-shop') != undefined) {
        document.getElementById('btn-open_modal-shop').onclick = openShopCard;
    }

    // close shop card
    if (document.getElementById('close-modal-shop-card') != null && document.getElementById('close-modal-shop-card') != undefined) {
        document.getElementById('close-modal-shop-card').onclick = closeShopCard;
    }

    if (document.getElementById('show-mobile-filter-shop') != null && document.getElementById('show-mobile-filter-shop') != undefined) {
        document.getElementById('show-mobile-filter-shop').onclick = openModalShopFilter;
    }

    if (document.getElementById('close-modal-shop-filter') != null && document.getElementById('close-modal-shop-filter') != undefined) {
        document.getElementById('close-modal-shop-filter').onclick = closeShopFilter
    }


    // ********************************************************************************
    // script for all modal open / close end
    // ********************************************************************************

    // script for modal shop card count product
    let allBtnModalMinusProduct;
    if (document.querySelectorAll('.btn-modal-item-minus') != null && document.querySelectorAll('.btn-modal-item-minus') != undefined) {
        allBtnModalMinusProduct = document.querySelectorAll('.btn-modal-item-minus');
        for (let item of allBtnModalMinusProduct) {
            item.onclick = minusCountThisProductModal;
        }
    }

    let allBtnModalPlusProduct;
    if (document.querySelectorAll('.btn-modal-item-plus') != null && document.querySelectorAll('.btn-modal-item-plus') != undefined) {
        allBtnModalPlusProduct = document.querySelectorAll('.btn-modal-item-plus');
        for (let item of allBtnModalPlusProduct) {
            item.onclick = plusCountThisProductModal;
        }
    }

    // script check if basket don't have products hide else show
    if (document.querySelectorAll('#modal-shop-card-basket .modal-shop-card-item') != null && document.querySelectorAll('#modal-shop-card-basket .modal-shop-card-item') != undefined) {
        if (document.querySelectorAll('#modal-shop-card-basket .modal-shop-card-item').length == 0) {
            //document.getElementById('modal-shop-card-with-product').style.display = `none`; // Custom code
            document.getElementById('modal-shop-card-no-product').style.display = `flex`;
        } else if (document.querySelectorAll('#modal-shop-card-basket .modal-shop-card-item').length > 0) {
            document.getElementById('modal-shop-card-with-product').style.display = `flex`;
            //document.getElementById('modal-shop-card-no-product').style.display = `none`; // Custom code
        }
    }

    // only mobile !!!!
    if (document.getElementById('show-mobile-menu') != null && document.getElementById('show-mobile-menu') != undefined) {
        document.getElementById('show-mobile-menu').onclick = toggleMobileMenu;
    }

    // show menu catalog
    if (document.getElementById('btn-for-show-menu-catalog') != null && document.getElementById('btn-for-show-menu-catalog') != undefined) {
        document.getElementById('btn-for-show-menu-catalog').onclick = toggleMenuCatalog;
    }

    // show sub menu in mobile
    allBtnMobileSub[0].parentElement.classList.add('this-catalog-sub-menu-active');

    for (let item of allBtnMobileSub) {
        item.onclick = showSubMenuMobile;
    }

    // btn in catalog go to main menu
    if (document.getElementById('btn-back-to-mobile-menu') != null && document.getElementById('btn-back-to-mobile-menu') != undefined) {
        document.getElementById('btn-back-to-mobile-menu').onclick = goToMainMobileMenuWithCatalog;
    }

    // btn show enter registration mobile
    if (document.getElementById('btn_personal_cabinet-mobile') != undefined && document.getElementById('btn_personal_cabinet-mobile') != null) {
        document.getElementById('btn_personal_cabinet-mobile').onclick = openMobileEnterRegistration;
    }

    // btn close enter registration
    if (document.getElementById('close-mobile-enter-registration') != null && document.getElementById('close-mobile-enter-registration') != undefined) {
        document.getElementById('close-mobile-enter-registration').onclick = closeMobileEnterRegistration;
    }

    // open mobile shop card
    if (document.getElementById('btn_shop_art-mobile') != null && document.getElementById('btn_shop_art-mobile') != undefined) {
        document.getElementById('btn_shop_art-mobile').onclick = openMobileShopCard;
    }

    if (document.getElementById('close-mobile-shop-card') != null && document.getElementById('close-mobile-shop-card') != undefined) {
        document.getElementById('close-mobile-shop-card').onclick = closeMobileShopCard;
    }

    // mobile btn remember password
    if (document.getElementById('close-mobile-remember-pass-mobile') != null && document.getElementById('close-mobile-remember-pass-mobile') != undefined) {
        document.getElementById('close-mobile-remember-pass-mobile').onclick = closeModalMobileRememberPassword;
    }

    // close mobile modal new password
    if (document.getElementById('close-mobile-new-pass-mobile') != null && document.getElementById('close-mobile-new-pass-mobile') != undefined) {
        document.getElementById('close-mobile-new-pass-mobile').onclick = closeModalMobileNewPassWord;
    }

    let allFilterBtn;
    if (document.querySelectorAll('section-group-filter_btn') != null && document.querySelectorAll('section-group-filter_btn') != undefined) {
        allFilterBtn = document.querySelectorAll('.section-group-filter_btn');

        if (allFilterBtn[0] != undefined && allFilterBtn[0] != null) {
            allFilterBtn[0].parentElement.classList.toggle('show-more-filter-item');
        }
    }

    for (let item of allFilterBtn) {
        item.onclick = toggleFilterItem;
    }

    // btn more info for mobile page product
    if (document.getElementById('btn-product-more-info') != null && document.getElementById('btn-product-more-info') != undefined) {
        document.getElementById('btn-product-more-info').onclick = showMoreInfoProduct;
    }

    // btn-more-description-product
    if (document.getElementById('btn-more-description-product') != null && document.getElementById('btn-more-description-product') != undefined) {
        document.getElementById('btn-more-description-product').onclick = showMoreDescriptionProduct;
    }

    if (innerWidth >= 1024) {
        if (document.getElementById('btnn-show-all-product-beauty') != null && document.getElementById('btnn-show-all-product-beauty') != undefined) {
            document.getElementById('btnn-show-all-product-beauty').onclick = showAllBeautyProduct;
        }
    } else {
        if (document.getElementById('btnn-show-all-product-beauty') != null && document.getElementById('btnn-show-all-product-beauty') != undefined) {
            document.getElementById('btnn-show-all-product-beauty').onclick = showAllBeautyProductMobile;
        }
    }

    if (allProductDescInfoTab[0] != null && allProductDescInfoTab[0] != undefined) {
        allProductDescInfoTab[0].parentElement.classList.add('s-pr-desc_item_active');
    }
    for (let item of allProductDescInfoTab) {
        item.onclick = showThisDescInfoProduct;
    }

    // dropdown 
    // filter-show
    let allFilterBtnHeader;
    if (document.querySelectorAll('.shop-filter-btn') != null && document.querySelectorAll('.shop-filter-btn') != undefined) {
        allFilterBtnHeader = document.querySelectorAll('.shop-filter-btn');

        for (let item of allFilterBtnHeader) {
            item.onclick = showThisFilter;
        }
    }

    // Page cabinet
    let allLinkCabinet;
    if (document.querySelectorAll('.link-menu-for-cabinet') != null && document.querySelectorAll('.link-menu-for-cabinet') != undefined) {
        allLinkCabinet = document.querySelectorAll('.link-menu-for-cabinet');

        for (let item of allLinkCabinet) {
            item.onclick = function () {
                for (let it of allLinkCabinet) {
                    it.classList.remove('active_link-cabinet');
                }
                let thisCabinetLinkAttr = this.getAttribute('data-cabinet-tab');
                this.classList.add('active_link-cabinet');

                let tabCabinet;
                if (document.querySelectorAll('.cabinet-show-tab') != null && document.querySelectorAll('.cabinet-show-tab') != undefined) {
                    tabCabinet = document.querySelectorAll('.cabinet-show-tab');

                    for (let elTab of tabCabinet) {
                        if (elTab.getAttribute('data-cabinet-tab') == thisCabinetLinkAttr) {
                            elTab.classList.add('active_cabinet-show-tab');
                        } else {
                            if (elTab.classList.contains('active_cabinet-show-tab') == true) {
                                elTab.classList.remove('active_cabinet-show-tab');
                            }
                        }
                    }
                }
            }
        }
    }

    // cabinet your order
    let allOrderHistory;
    if (document.querySelectorAll('.cabinet-all-order_itm_btn') != null && document.querySelectorAll('.cabinet-all-order_itm_btn') != undefined) {
        allOrderHistory = document.querySelectorAll('.cabinet-all-order_itm_btn');

        for (let item of allOrderHistory) {
            item.onclick = function () {
                this.parentElement.classList.toggle('active-history');
            }
        }
    }

    // cabinet tab mobile
    let allTabMobilel;
    if (document.querySelectorAll('.cabinet-mobile-btn') != null && document.querySelectorAll('.cabinet-mobile-btn') != undefined) {
        allTabMobilel = document.querySelectorAll('.cabinet-mobile-btn');

        for (let item of allTabMobilel) {
            item.onclick = function () {
                if (this.classList.contains('active-cabinet-mob-btn') == true) {
                    this.classList.remove('active-cabinet-mob-btn');
                } else {
                    this.classList.add('active-cabinet-mob-btn');
                }
                // this.classList.toggle('active-cabinet-mob-btn');
                thisAttrData = this.getAttribute('data-cabinet-tab');

                let showCabinetMobTab;
                if (document.querySelectorAll('.cabinet-show-tab') != null && document.querySelectorAll('.cabinet-show-tab') != undefined) {
                    showCabinetMobTab = document.querySelectorAll('.cabinet-show-tab');

                    for (let i of showCabinetMobTab) {
                        if (i.getAttribute('data-cabinet-tab') == thisAttrData) {
                            i.classList.toggle('active_cabinet-show-tab');
                        }
                    }
                }
            }
        }
    }

    // page about delivery and method pay
    let allBtnDMP;
    if (document.querySelectorAll('.page-info-method_item_head') != null && document.querySelectorAll('.page-info-method_item_head') != undefined) {
        allBtnDMP = document.querySelectorAll('.page-info-method_item_head');

        for (let item of allBtnDMP) {
            item.onclick = function () {
                this.parentElement.classList.toggle('active-page-del-pay');
            }
        }
    }

});
function showThisFilter() {
    this.parentElement.classList.toggle("active-show-main-dropdown");
}

function showThisDescInfoProduct() {
    for (let item of allProductDescInfoTab) {
        item.parentElement.classList.remove('s-pr-desc_item_active');
    }
    this.parentElement.classList.add('s-pr-desc_item_active');
}
function showAllBeautyProductMobile() {
    this.parentElement.parentElement.parentElement.classList.toggle('active-view-all-beauty-mobile');
}
function showAllBeautyProduct() {
    this.parentElement.classList.toggle('active-view-all-beauty');
}
function showMoreInfoProduct() {
    this.parentElement.children[0].classList.toggle('active-mobile-more');

    if (this.parentElement.children[0].classList.contains('active-mobile-more') == true) {
        this.innerHTML = iv_ajax_object.less;
    } else {
        this.innerHTML = iv_ajax_object.more;
    }
}
function showMoreDescriptionProduct() {
    this.parentElement.children[0].classList.toggle('active-mobile-more-description');

    if (this.parentElement.children[0].classList.contains('active-mobile-more-description') == true) {
        this.innerHTML = iv_ajax_object.less;
    } else {
        this.innerHTML = iv_ajax_object.more;
    }
}

function toggleFilterItem() {
    this.parentElement.classList.toggle('show-more-filter-item');
}


// function for animation this block
function animOnScroll() {
    for (let index = 0; index < animItems.length; index++) {
        const animItem = animItems[index];
        const animItemHeight = animItem.offsetHeight;
        const animItemOffset = offset(animItem).top;
        const animStart = 2;

        let animItemPoint = window.innerHeight - animItemHeight / animStart;

        if (animItemHeight > window.innerHeight) {
            animItemPoint = window.innerHeight - window.innerHeight / animStart;
        }

        if ((pageYOffset > animItemOffset - animItemPoint) && pageYOffset < (animItemOffset + animItemHeight)) {
            animItem.classList.add('_active-scroll-animation');
        }
    }
}

function offset(el) {
    const rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

function goToMainMobileMenuWithCatalog() {
    document.body.classList.remove('show-only-mobile-menu-catalog');
    document.getElementById('show-mobile-menu').parentElement.classList.remove('active-show-mobile-menu-catalog');

    document.body.classList.add('show-only-mobile-menu');
    document.getElementById('show-mobile-menu').parentElement.classList.add('active-show-mobile-menu');
}

function showSubMenuMobile() {

    for (let item of allBtnMobileSub) {
        item.parentElement.classList.remove('this-catalog-sub-menu-active');
    }
    this.parentElement.classList.add('this-catalog-sub-menu-active');
}

function toggleMenuCatalog() {
    if (document.getElementById('show-mobile-menu').parentElement.classList.contains('active-show-mobile-menu-catalog') == false) {
        document.body.classList.add('show-only-mobile-menu-catalog');
        document.getElementById('show-mobile-menu').parentElement.classList.add('active-show-mobile-menu-catalog');

        document.body.classList.remove('show-only-mobile-menu');
        document.getElementById('show-mobile-menu').parentElement.classList.remove('active-show-mobile-menu');
    } else {
        document.body.classList.remove('show-only-mobile-menu-catalog');
        document.getElementById('show-mobile-menu').parentElement.classList.remove('active-show-mobile-menu-catalog');
    }
}

function toggleMobileMenu() {
    if (document.getElementById('show-mobile-menu').parentElement.classList.contains('active-show-mobile-menu-catalog') == true && this.parentElement.classList.contains('active-show-mobile-menu') == false) {
        this.parentElement.classList.remove('active-show-mobile-menu')
        document.body.classList.remove('show-only-mobile-menu');

        document.body.classList.remove('show-only-mobile-menu-catalog');
        this.parentElement.classList.remove('active-show-mobile-menu-catalog');
    } else
        if (this.parentElement.classList.contains('active-show-mobile-menu') == false) {
            this.parentElement.classList.add('active-show-mobile-menu')
            document.body.classList.add('show-only-mobile-menu');
        } else {
            this.parentElement.classList.remove('active-show-mobile-menu')
            document.body.classList.remove('show-only-mobile-menu');
        }
}

function minusCountThisProductModal() {	
    this.parentElement.children[1].children[0].value--;
    if (this.parentElement.children[1].children[0].value < 1) {
        this.parentElement.children[1].children[0].value = 1;
    }
}

function plusCountThisProductModal() {
    this.parentElement.children[1].children[0].value++;
}
// ********************************************************************************
// all function for open / close modal start
// ********************************************************************************
function openModalEnterRegistration() {
    scrollDisable();
    document.body.classList.add('show-modal-enter-registration');
    setVisible(modalEnterRegistration);
    window.location.hash = "#modal-enter-registration";
}

function openMobileEnterRegistration() {
    scrollDisable();
    document.body.classList.add('show-modal-enter-registration');
    setVisible(modalEnterRegistration);
    window.location.hash = "#modal-enter-registration";
}

function closeModalEnterRegistration() {
    scrollEnable();
    setHidden(modalEnterRegistration);
    if (window.location.hash == "#modal-enter-registration") {
        remove_hash_from_url();
    }
    document.body.classList.remove('show-modal-enter-registration');
}

function closeMobileEnterRegistration() {
    scrollEnable();
    setHidden(modalEnterRegistration);
    if (window.location.hash == "#modal-enter-registration") {
        remove_hash_from_url();
    }
    document.body.classList.remove('show-modal-enter-registration');
}

function openModalAllMessage(messageTitle, messageTXT) {
    scrollDisable();
    document.body.classList.add('show-modal-message');
    setVisible(modalMessage);
    remove_hash_from_url();
    window.location.hash = "#modal-message";
    document.getElementById('this-message').innerHTML = `${messageTXT}`;
    document.getElementById('this-message-title').innerHTML = `${messageTitle}`;
}

function closeModalAllMessage() {
    scrollEnable();
    setHidden(modalMessage);
    if (window.location.hash == "#modal-message") {
        remove_hash_from_url();
    }
    document.body.classList.remove('show-modal-message');
}

function openModalNewPassword() {
    scrollDisable();
    document.body.classList.add('show-modal-new-password');
    setVisible(modalNewPass);
    window.location.hash = "#modal-new-password";
}

function closeModalNewPassword() {
    scrollEnable();
    setHidden(modalNewPass);
    if (window.location.hash == "#modal-new-password") {
        remove_hash_from_url();
    }
    document.body.classList.remove('show-modal-new-password');
}

function closeModalMobileNewPassWord() {
    scrollEnable();
    setHidden(modalNewPass);
    if (window.location.hash == "#modal-new-password") {
        remove_hash_from_url();
    }
    document.body.classList.remove('show-modal-new-password');
}

function openShopCard() {
	// Custom code
	if ( iv_shop_cart == 'yes' ) return false;
    scrollDisable();
    document.body.classList.add('show-modal-shop-card');
    setVisible(modalShopCard);
    window.location.hash = "#modal-shop-card";
}

function openModalShopFilter() {
    scrollDisable();
    document.body.classList.add('show-modal-shop-filter');
    setVisible(modalShopFilter);
    window.location.hash = "#modal-shop-filter";
}

function closeShopFilter() {
    scrollEnable();
    setHidden(modalShopFilter);
    if (window.location.hash == "#modal-shop-filter") {
        remove_hash_from_url();
    }
    document.body.classList.remove('show-modal-shop-filter');
}

function closeMobileShopCard() {
    scrollEnable();
    setHidden(modalShopCard);
    if (window.location.hash == "#modal-shop-card") {
        remove_hash_from_url();
    }
    document.body.classList.remove('show-modal-shop-card');
}

function openMobileShopCard() {
	// Custom code
	if ( iv_shop_cart == 'yes' ) return false;
    scrollDisable();
    document.body.classList.add('show-modal-shop-card');
    setVisible(modalShopCard);
    window.location.hash = "#modal-shop-card";
}

function closeShopCard() {
    scrollEnable();
    setHidden(modalShopCard);
    if (window.location.hash == "#modal-shop-card") {
        remove_hash_from_url();
    }
    document.body.classList.remove('show-modal-shop-card');
}

function remove_hash_from_url() {
    var uri = window.location.toString();
    if (uri.indexOf("#") > 0) {
        var clean_uri = uri.substring(0, uri.indexOf("#"));
        window.history.replaceState({}, document.title, clean_uri);
    }
}

function openModalRememberPassword() {
    scrollDisable();
    document.body.classList.add('show-modal-remember-password');
    setVisible(modalRememberPass);
    window.location.hash = "#modal-remember-password";
}

function closeModalMobileRememberPassword() {
    scrollEnable();
    setHidden(modalRememberPass);
    if (window.location.hash == "#modal-remember-password") {
        remove_hash_from_url();
    }
    document.body.classList.remove('show-modal-remember-password');
}

function closeModalRememberPassword() {
    scrollEnable();
    setHidden(modalRememberPass);
    if (window.location.hash == "#modal-remember-password") {
        remove_hash_from_url();
    }
    document.body.classList.remove('show-modal-remember-password');
}

// scroll 
function scrollDisable() {
    let x = window.scrollX;
    let y = window.scrollY;
    window.onscroll = function () { window.scrollTo(x, y); };
}

function scrollEnable() {
    window.onscroll = function () { };
}

// ********************************************************************************
// all function for open / close modal end
// ********************************************************************************

// ********************************************************************************
// allFunction start
// ********************************************************************************
function showSubMenu() {
    this.parentElement.classList.toggle('active_show_sub-menu');
}

function showImgForSubMenu() {
    let getSrc = this.getAttribute('data-menu-src');
    // let blockImg = document.querySelector('#section_menu-catalog_img_for-link img');

    let blockImg = this.parentElement.parentElement.parentElement.parentElement.children[1].children[0];

    blockImg.removeAttribute('src');
    blockImg.setAttribute('src', `${getSrc}`);
}

function showMenuForCatalog() {
    if (document.querySelector('body').classList.contains('active_modal_header-catalog') == false) {
        this.classList.add('active_btn');
        document.querySelector('body').classList.add('active_modal_header-catalog');
        setVisible(modalMenuCatalog);

        // let ModalMenuCatalogWrapper = document.querySelector('.show_menu-catalog_wrapper');

        // let rez = modalMenuCatalog.scrollWidth - ModalMenuCatalogWrapper.scrollWidth;

        // containerSubMenuCatalog.style.width = `calc(83% + ${rez}px)`;
        // containerSubMenuCatalog.style.marginLeft = `calc(${rez}px)`;

        // if (document.body.scrollWidth <= 1800) {
        //     containerSubMenuCatalog.style.width = `calc(92% + ${rez}px)`;
        //     containerSubMenuCatalog.style.marginLeft = `calc(${rez}px)`;
        // }

    } else {
        closeMenuForCatalog();
    }
}

function closeMenuForCatalog() {
    document.querySelector('body').classList.remove('active_modal_header-catalog');
    btnCatalog.classList.remove('active_btn');
    setHidden(modalMenuCatalog);
}


function setHidden(element) {
    element.style.opacity = '0';
    element.style.visibility = 'hidden';
    // setTimeout(function () {
    element.style.display = `none`
    // }, timerStart);
    element.style.transition = `all 0s ease -in `;
}

function setVisible(element) {
    element.style.display = `flex`;
    element.style.visibility = 'visible';
    // setTimeout(function () {
    element.style.opacity = '1';
    // }, timerStart);
    element.style.transition = `all 0s ease -in `;
}

// ********************************************************************************
// allFunction start
// ********************************************************************************

// ********************************************************************************
// script validate all form
// ********************************************************************************
// validate name
let maskName = /^[a-z]$/i;
function validName(item) {
    let valid = true;

    if (item.type == 'text') {
        if (maskName.test(item.value) == false && item.value.length < 3) {
            let thisDivError = item.nextSibling.nextSibling;

            item.classList.add('input-mail-error');

            thisDivError.style.display = 'flex';
            thisDivError.style.visibility = 'visible';
            thisDivError.style.opacity = '1';
            valid = false;
        }
    }

    return valid;
}

// validate checkbox
function validCheckbox(item) {
    let valid = true;

    if (item.type == 'checkbox') {
        if (item.checked == false) {
            item.parentElement.style.color = 'red';
            item.parentElement.children[1].style.color = 'red';
            valid = false;
        } else {
            item.parentElement.style.color = '#000000';
            item.parentElement.children[1].style.color = '#000000';
        }
    }

    return valid;
}

function validPasswordLogin(input) {
    let valid = true;

    if (input.type == 'password' && input.getAttribute('id') == 'password-enter') {
        let divM = input.parentElement.children[2];
        if (input.value == '') {
            divM.children[0].innerHTML = iv_ajax_object.pass_fill_in;
            ifInputError(input, divM);
            valid = false;
        } else if (input.value != '' && input.value.length >= 1 && input.value.length < 6) {
            divM.children[0].innerHTML = iv_ajax_object.pass_long;
            ifInputError(input, divM);
            valid = false;
        } else {
            ifInputValid(input, divM);
            valid = true;
        }
    }
    return valid;
}

function validPasswordRegistration(input) {
    let valid1 = true;
    let valid2 = true;

    if (input.type == 'password' && input.getAttribute('id') == 'password-registration') {
        let divM = input.parentElement.children[2];
        if (input.value == '') {

            divM.children[0].innerHTML = iv_ajax_object.pass_fill_in;
            ifInputError(input, divM);
            valid1 = false;
        } else if (input.value != '' && input.value.length >= 1 && input.value.length < 6) {
            divM.children[0].innerHTML = iv_ajax_object.pass_long;
            ifInputError(input, divM);
            valid1 = false;
        } else if (input.value != pass2.value && input.value != '' && pass2.value != '' && input.value.length >= 6) {
            divM.children[0].innerHTML = iv_ajax_object.pass_not_match;
            ifInputError(input, divM);
            valid1 = false;
        } else {
            ifInputValid(input, divM);
            ifInputValid(pass2, divM);
            valid1 = true;
        }
    } else if (pass2.type == 'password' && input.getAttribute('id') == 'repeat-password-registration') {
        let divM = pass2.parentElement.children[2];
        if (pass2.value == '') {
            divM.children[0].innerHTML = iv_ajax_object.pass_repeat;
            ifInputError(pass2, divM);
            valid2 = false;
        } else if (pass2.value != pass1.value && pass2.value != '' && pass1.value != '') {
            divM.children[0].innerHTML = iv_ajax_object.pass_not_match;
            ifInputError(pass2, divM);
            valid2 = false;
        } else {
            ifInputValid(input, divM);
            ifInputValid(pass1, divM);
            valid2 = true;
        }
    }

    valid = valid1 && valid2;
    return valid;
}

function validPasswordNew(input) {
    let valid1 = true;
    let valid2 = true;

    if (input.type == 'password' && newpass1.getAttribute('id') == 'new-password') {
        let divM = input.parentElement.children[2];
        if (input.value == '') {
            divM.children[0].innerHTML = iv_ajax_object.pass_fill_in;
            ifInputError(input, divM);
            valid1 = false;
        } else if (input.value != '' && input.value.length >= 1 && input.value.length < 6) {
            divM.children[0].innerHTML = iv_ajax_object.pass_long;
            ifInputError(input, divM);
            valid1 = false;
        } else if (input.value != newpass2.value && input.value != '' && newpass2.value != '' && input.value.length >= 6) {
            let repeatPass = newpass2.parentElement.children[2];
            divM.children[0].innerHTML = iv_ajax_object.pass_not_match;
            ifInputError(input, divM);
            valid1 = false;
        } else {
            ifInputValid(input, divM);
            ifInputValid(pass2, divM);
            valid1 = true;
        }
    } else if (newpass2.type == 'password' && newpass2.getAttribute('id') == 'registration-repeat-new-password') {
        let divM = newpass2.parentElement.children[2];
        if (newpass2.value == '') {
            divM.children[0].innerHTML = iv_ajax_object.pass_repeat;
            ifInputError(newpass2, divM);
            valid2 = false;
        } else if (newpass2.value != pass1.value && newpass2.value != '' && newpass1.value != '') {
            divM.children[0].innerHTML = iv_ajax_object.pass_not_match;
            ifInputError(newpass2, divM);
            valid2 = false;
        } else {
            ifInputValid(input, divM);
            ifInputValid(pass1, divM);
            valid2 = true;
        }
    }

    valid = valid1 && valid2;
    return valid;
}

// validate phone number
// let maskPhone = /^\+[\d]{2}\ \([\d]{2,3}\)\ [\d]{2,3} - [\d]{2,3} - [\d]{2,3}$/;
let maskPhone = /^\+[\d]{3} [\d]{2,3} [\d]{1,2} [\d]{3,4}$/;
function validPhone(item) {
    let valid = true;

    if (item.type == 'tel') {
        if (maskPhone.test(item.value) == false) {
            let thisDivError = item.nextSibling.nextSibling;

            item.classList.add('input-mail-error');

            thisDivError.style.display = 'flex';
            thisDivError.style.visibility = 'visible';
            thisDivError.style.opacity = '1';
            valid = false;
        }
    }
    return valid;
}

// validate mail
// let maskMail = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
let maskMail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
function validMail(item) {
    let valid = true;

    let checkMail = maskMail.test(item.value);
    if (item.type == 'email') {
        if (checkMail == false) {
            let thisDivError = item.nextSibling.nextSibling;

            item.classList.add('input-mail-error');

            thisDivError.style.display = 'flex';
            thisDivError.style.visibility = 'visible';
            thisDivError.style.opacity = '1';

            valid = false;
        }
    }
    return valid;
}

// input + label
function getLabelInput() {
    let input = this.querySelector('input');
    let label = this.querySelector('label');
    let divMessage;
    if (this.querySelector('.input-mail-error__message') != null && this.querySelector('.input-mail-error__message') != undefined) {
        divMessage = this.querySelector('.input-mail-error__message');
    }

    input.addEventListener('input', () => {
        if (input.value == '') {
            if (label != null) {
                label.classList.remove('active-field-input');
            }
        } else {
            if (label != null) {
                label.classList.add('active-field-input');
            }
        }
    })

    input.addEventListener('focusin', () => {
        if (input.value == '') {
            if (label != null) {
                label.classList.remove('active-field-input');
            }
        } else {
            if (label != null) {
                label.classList.add('active-field-input');
            }
        }
    })

    input.addEventListener('keypress', () => {
        if (label != null) {
            label.classList.add('active-field-input');
        }
    });

    input.addEventListener('blur', () => {
        if (input.type == 'email') {
            if (maskMail.test(input.value) == false && divMessage != undefined) {
                input.classList.add('input-mail-error');
                divMessage.style.display = 'flex';
                divMessage.style.visibility = 'visible';
                divMessage.style.opacity = '1';
            }
        } else
            if (input.type == 'text' && input.value.length < 1 && divMessage != undefined) { // Custom code input.value.length < 3
                input.classList.add('input-mail-error');
                divMessage.style.display = 'flex';
                divMessage.style.visibility = 'visible';
                divMessage.style.opacity = '1';
            } else
                if (input.type == 'tel' && divMessage != undefined) {
                    if (maskPhone.test(input.value) == false) {
                        input.classList.add('input-mail-error');
                        divMessage.style.display = 'flex';
                        divMessage.style.visibility = 'visible';
                        divMessage.style.opacity = '1';
                    }
                } else
                    if (input.type == 'password' && input.getAttribute('id') == 'password-registration') {
                        if (input.value == '') {
                            divMessage.children[0].innerHTML = iv_ajax_object.pass_fill_in;
                            ifInputError(input, divMessage);
                        } else if (input.value != '' && input.value.length >= 1 && input.value.length < 6) {
                            divMessage.children[0].innerHTML = iv_ajax_object.pass_long;
                            ifInputError(input, divMessage);
                        } if (input.value != pass2.value && input.value != '' && pass2.value != '' && input.value.length >= 6) {
                            divMessage.children[0].innerHTML = iv_ajax_object.pass_not_match;
                            ifInputError(input, divMessage);
                        }
                    } else
                        if (input.type == 'password' && input.getAttribute('id') == 'repeat-password-registration') {
                            if (input.value == '') {
                                divMessage.children[0].innerHTML = iv_ajax_object.pass_repeat;
                                ifInputError(input, divMessage);
                            } else if (input.value != pass1.value && input.value != '' && pass1.value != '') {
                                divMessage.children[0].innerHTML = iv_ajax_object.pass_not_match;
                                ifInputError(input, divMessage);
                            }
                        } else
                            if (input.type == 'password' && input.getAttribute('id') == 'new-password') {
                                //new
                                if (input.value == '') {
                                    divMessage.children[0].innerHTML = iv_ajax_object.pass_fill_in;
                                    ifInputError(input, divMessage);
                                } else if (input.value != '' && input.value.length >= 1 && input.value.length < 6) {
                                    divMessage.children[0].innerHTML = iv_ajax_object.pass_long;
                                    ifInputError(input, divMessage);
                                } if (input.value != newpass2.value && input.value != '' && newpass2.value != '' && input.value.length >= 6) {
                                    divMessage.children[0].innerHTML = iv_ajax_object.pass_not_match;
                                    ifInputError(input, divMessage);
                                }
                            } else
                                if (input.type == 'password' && input.getAttribute('id') == 'registration-repeat-new-password') {
                                    if (input.value == '') {
                                        divMessage.children[0].innerHTML = iv_ajax_object.pass_repeat;
                                        ifInputError(input, divMessage);
                                    } else if (input.value != newpass1.value && input.value != '' && newpass1.value != '') {
                                        divMessage.children[0].innerHTML = iv_ajax_object.pass_not_match;
                                        ifInputError(input, divMessage);
                                    }
                                }
                                else
                                    if (input.type == 'password' && input.getAttribute('id') == 'password-enter') {
                                        if (input.value == '') {
                                            divMessage.children[0].innerHTML = iv_ajax_object.pass_fill_in;
                                            ifInputError(input, divMessage);
                                        } else if (input.value != '' && input.value.length >= 1 && input.value.length < 6) {
                                            divMessage.children[0].innerHTML = iv_ajax_object.pass_long;
                                            ifInputError(input, divMessage);
                                        }
                                    }
    });

    input.addEventListener('focus', () => {
        if (input.classList.contains('input-mail-error')) {
            input.classList.remove('input-mail-error');
            divMessage.style.display = 'none';
            divMessage.style.visibility = 'hidden';
            divMessage.style.opacity = '0';
        }
    });
}

function ifInputValid(input, divMessage) {
    if (input != undefined && input != null) {
        input.classList.remove('input-mail-error');
        input.parentElement.parentElement.classList.remove('error-border');
    }
    if (divMessage != undefined) {
        divMessage.style.display = 'none';
        divMessage.style.visibility = 'hidden';
        divMessage.style.opacity = '0';
    }
}

function ifInputError(input, divMessage) {
    if (input != undefined && input != null) {
        input.classList.add('input-mail-error');
        input.parentElement.parentElement.classList.add('error-border');
    }
    if (divMessage != undefined) {
        divMessage.style.display = 'flex';
        divMessage.style.visibility = 'visible';
        divMessage.style.opacity = '1';
    }
}

