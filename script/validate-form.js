document.addEventListener('DOMContentLoaded', function () {
    let timerStart = 500;

    //form validate when user keypress simple 
    let sectionInput;
    if (document.querySelectorAll('.section-all-input') != null) {
        sectionInput = document.querySelectorAll('.section-all-input');
    }

    for (let item of sectionInput) {
        item.addEventListener('click', getLabelInput);
    }

    let popUpOoops,
        formFollowing;

    if (document.getElementById('section-pop-up-ooops') != null) {
        popUpOoops = document.getElementById('section-pop-up-ooops');
    }

    //following  to news
    if (document.getElementById('following_to_news') != null) {
        formFollowing = document.getElementById('following_to_news');

        formFollowing.onsubmit = () => {
            event.preventDefault();
            let validM = false;

            for (let item of formFollowing) {
                if (validMail(item) == true && item.type == 'email') {
                    validM = true;
                }
            }

            let allValid = validM;

            if (allValid == false) {
                // popUpOoops.style.visibility = 'visible';
                // popUpOoops.style.display = 'flex';
                // setTimeout(function () {
                //     popUpOoops.style.opacity = '1';
                // }, timerStart);
                // popUpOoops.style.transition = `all .5s ease-in`;

                console.log('false');
            } else {
                console.log('true');
            }

            return allValid;
        }
    }
});


// validate name
let maskName = /^[a-z]$/i;
function validName(item) {
    let valid = true;

    if (item.type == 'text') {
        if (maskName.test(item.value) == false && item.value.length < 3) {
            let thisDivError = item.nextSibling.nextSibling;

            item.classList.add('input-mail-error');

            thisDivError.style.display = 'flex';
            thisDivError.style.visibility = 'visible';
            thisDivError.style.opacity = '1';
            valid = false;
        }
    }

    return valid;
}

// validate checkbox
function validCheckbox(item) {
    let valid = true;

    if (item.type == 'checkbox') {
        if (item.checked == false) {
            item.parentElement.parentElement.style.color = 'red';
            item.parentElement.parentElement.children[1].style.color = 'red';
            valid = false;
        } else {
            item.parentElement.parentElement.style.color = '#3A1551';
            item.parentElement.parentElement.children[1].style.color = '#3A1551';
        }
    }

    return valid;
}

// validate phone number
let maskPhone = /^\+[\d]{2}\ \([\d]{2,3}\)\ [\d]{2,3} - [\d]{2,3} - [\d]{2,3}$/;
function validPhone(item) {
    let valid = true;

    if (item.type == 'tel') {
        if (maskPhone.test(item.value) == false) {
            let thisDivError = item.nextSibling.nextSibling;

            item.classList.add('input-mail-error');

            thisDivError.style.display = 'flex';
            thisDivError.style.visibility = 'visible';
            thisDivError.style.opacity = '1';
            valid = false;
        }
    }
    return valid;
}

// validate mail
let maskMail = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
function validMail(item) {
    let valid = true;

    let checkMail = maskMail.test(item.value);
    if (item.type == 'email') {
        if (checkMail == false) {
            let thisDivError = item.nextSibling.nextSibling;

            item.classList.add('input-mail-error');

            thisDivError.style.display = 'flex';
            thisDivError.style.visibility = 'visible';
            thisDivError.style.opacity = '1';

            valid = false;
        }
    }
    return valid;
}

// input + label
function getLabelInput() {
    let input = this.querySelector('input');
    let label = this.querySelector('label');
    let divMessage;
    if (this.querySelector('.input-mail-error__message') != null && this.querySelector('.input-mail-error__message') != undefined) {
        divMessage = this.querySelector('.input-mail-error__message');
    }

    input.addEventListener('input', () => {
        if (input.value == '') {
            if (label != null) {
                label.classList.remove('active-field-input');
            }
        } else {
            if (label != null) {
                label.classList.add('active-field-input');
            }
        }
    })

    input.addEventListener('focusin', () => {
        if (input.value == '') {
            if (label != null) {
                label.classList.remove('active-field-input');
            }
        } else {
            if (label != null) {
                label.classList.add('active-field-input');
            }
        }
    })

    input.addEventListener('keypress', () => {
        if (label != null) {
            label.classList.add('active-field-input');
        }
    });

    input.addEventListener('blur', () => {
        if (input.type == 'email') {
            if (maskMail.test(input.value) == false && divMessage != undefined) {
                input.classList.add('input-mail-error');
                divMessage.style.display = 'flex';
                divMessage.style.visibility = 'visible';
                divMessage.style.opacity = '1';
            }
        }
        else if (input.type == 'text' && input.value.length < 3 && divMessage != undefined) {
            input.classList.add('input-mail-error');
            divMessage.style.display = 'flex';
            divMessage.style.visibility = 'visible';
            divMessage.style.opacity = '1';
        }
        else if (input.type == 'tel' && divMessage != undefined) {
            if (maskPhone.test(input.value) == false) {
                console.log(input);
                input.classList.add('input-mail-error');
                divMessage.style.display = 'flex';
                divMessage.style.visibility = 'visible';
                divMessage.style.opacity = '1';
            }
        }
    });

    input.addEventListener('focus', () => {
        if (input.classList.contains('input-mail-error')) {
            input.classList.remove('input-mail-error');
            divMessage.style.display = 'none';
            divMessage.style.visibility = 'hidden';
            divMessage.style.opacity = '0';
        }
    });
}

