// Login form
function ivAjaxLogin() {	
	$('#form-for-enter .status_login').show().text(iv_ajax_object.loadingmessage);
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: iv_ajax_object.ajaxurl,
		data: { 
			'action': 'ajax_login',
			'username': $('#form-for-enter #enail-enter').val(), 
			'password': $('#form-for-enter #password-enter').val(), 
			//'security': $('.login_box #security').val()
		},
		success: function(data){
		$('#form-for-enter .status_login').text(data.message);
			if (data.loggedin == true){
				document.location.href = iv_ajax_object.redirecturl;
			}
		}
	});
}

// Register form
function ivAjaxRegistration() {	
	$('.status_registration').show().text(iv_ajax_object.loadingmessage);
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: iv_ajax_object.ajaxurl,
		data: { 
			'action': 'ajax_registration', //calls wp_ajax_nopriv_ajaxlogin
			'login': $('#name-registration').val(),
			'last-name': $('#last-name-registration').val(),
			'email': $('#enail-registration').val(),
			'phone': $('#phone-registration').val(),
			'password': $('#password-registration').val(), 
			'check-password': $('#repeat-password-registration').val(),
			'checkbox-registration': $('#checkbox-registration').val(),
			'security': $('#form-for-registration #security').val()
		},
		success: function(data){
			//console.log(data);
			$('.status_registration').text(data.message);
			if (data.registration == true){
				// ? тут може очистити форму реєстрації
				$('.status_login').text(data.reg_message);
				$('#tab-modal-enter').click();
			}
		}
	});
}

// ResetPassword form
function ivAjaxResetPassword() {
	$('.status_reset_password').show().text(iv_ajax_object.loadingmessage);	
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: iv_ajax_object.ajaxurl, //ajax_recpass_object.ajaxurl,
		data: {
			'action' : 'ajax_reset_password',
			'lost_pass' : $('#form-for-remember-password #email-remember-pas').val(),
			'security_reset' : $('#form-for-remember-password #security_reset').val()
		},
		beforeSend:function(){
		},
		success: function(data){
			$('.status_reset_password').html(data.message);
			//console.log(data.message);
		}
	});
}

// Change count product in mini cart
function changeCountProduct(obj, event, sign) {
	event.preventDefault();
	let input = obj.parentElement.children[1].children[0];
	if ( sign == 'plus' ) {
		input.value++;
	} else if ( sign == 'minus' && input.value > 1 ) {
		input.value--;
	} else {
		return false;
	}
	
	var mini_cart = $('#la_fleur_mini_cart');
	mini_cart.css('opacity', '0.3');
	var data = {
		action: 'iv_mini_cart_quanity_update',
		product_key: obj.parentElement.children[1].children[1].value,
		number: input.value,
		security: jQuery('#container_mini_cart #security').val()
	};

	jQuery.post( iv_ajax_object.ajaxurl, data, function( response ) {
		mini_cart.css('opacity', '1');
		mini_cart.html(response);
		$('.la_fleur_mini_cart_count').text( jQuery('#custom_cart_count').val() );
	});				
}

// Change count product in cart
function changeCountProductCart(obj, event, sign) {
	event.preventDefault();
	let input = obj.parentElement.children[1].children[0];
	if ( sign == 'plus' ) {
		input.value++;
	} else if ( sign == 'minus' && input.value > 1 ) {
		input.value--;
	} else {
		return false;
	}
	
	var cart = $('#la_fleur_cart');
	cart.css('opacity', '0.3');
	var data = {
		action: 'iv_cart_quanity_update',
		product_key: obj.parentElement.children[1].children[1].value,
		number: input.value,
		security: jQuery('#iv_cart #security').val()
	};

	jQuery.post( iv_ajax_object.ajaxurl, data, function( response ) {
		cart.css('opacity', '1');
		cart.html(response);
		$('.la_fleur_mini_cart_count').text( jQuery('#cart_count').val() );
	});				
}

// Change count product in checkout
function changeCountProductCheckout(obj, event, sign) {
	event.preventDefault();
	let input = obj.parentElement.children[1].children[0];
	if ( sign == 'plus' ) {
		input.value++;
	} else if ( sign == 'minus' && input.value > 1 ) {
		input.value--;
	} else {
		return false;
	}
	
	var checkout = $('#la_fleur_checkout');
	checkout.css('opacity', '0.3');
	var data = {
		action: 'iv_checkout_quanity_update',
		product_key: obj.parentElement.children[1].children[1].value,
		number: input.value,
		security: jQuery('#iv_checkout #security').val()
	};

	jQuery.post( iv_ajax_object.ajaxurl, data, function( response ) {
		checkout.css('opacity', '1');
		checkout.html(response);
		$('.la_fleur_mini_cart_count').text( jQuery('#cart_count').val() );
	});				
}

// Remove from mini cart - button
function removeFromMiniCart() {
	$('#la_fleur_mini_cart').css('opacity', '0.3');
}

// Remove from cart - button
function removeFromCart() {
	$('#la_fleur_cart').css('opacity', '0.3');
}

// Remove from checkout - button
function removeFromCheckout() {
	$('#la_fleur_checkout').css('opacity', '0.3');
}

// Close mini cart
function closeMiniCart() {
	closeShopCard();
}

// Following to news
function followingToNews() {
	$('.input-mail-error__message_two').remove();
	
	var online_email = $('#online-email').val();
	if ( online_email.length > 0 ) {
		var data = {
			action : 'following_to_news',
			security_email : jQuery('#security_email').val(),
			online_email : online_email
		};
		
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			beforeSend: function (response) {
				$('#following_to_news').addClass('la-fleur-loading');
			},
			complete: function (response) {
				$('#following_to_news').removeClass('la-fleur-loading');
			},
			success: function(responseJson){
				//console.log(responseJson);
				var response = JSON.parse( responseJson );
				if ( response.error ) {
					$('#following_to_news .section-all-input').append('<div class="input-mail-error__message_two"><p>'+ response.error +'</p></div');				
					return false;
				} else if ( response.success ) {
					// start write down in google sheet		
					var http = new XMLHttpRequest();
					var url = "https://script.google.com/macros/s/AKfycbyXrFO9QyuY27qMVlud3HcY96YjKl8RhN8ZV-rfMorbIMEh8i0/exec";
					var params = "p1="+online_email;
					http.open("GET", url+"?"+params, true);
					http.onreadystatechange = function() {
						if(http.readyState == 4 && http.status == 200) {
							//alert(http.responseText);
						} else {  }
					}
					http.send(null);
					// end
					$('label.active-field-input').removeClass('active-field-input');
					$('#online-email').val('');
					openModalAllMessage( iv_ajax_object.thank, iv_ajax_object.thank_msg );
					if (document.body.classList.contains('show-modal-message') == true) {
                    setTimeout(function () {
                        closeModalAllMessage();
                    }, timerClosModal);
                }
				} else {
					$('#following_to_news .section-all-input').append('<div class="input-mail-error__message_two"><p>'+ iv_ajax_object.error +'</p></div');				
					return false;
				}
			}
		});
	} else {
		return false;
	}
}

jQuery(document).ready(function($) {
	// Page sales - button load more
	$('#sales_loadmore').click(function(event){
		event.preventDefault();
		$(this).text(iv_ajax_object.downloading);
		var data = {
			'action' : 'page_sales_loadmore',
			'query': true_posts,
			'page' : current_page,
		};
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			success:function(data){
				if( data ) { 
					$('#sales_loadmore').text(iv_ajax_object.watch_all);
					$('#sales_posts').append(data);
					current_page++;		
					if (current_page == max_pages) $("#sales_loadmore").parent().parent().remove();				
					// тут можна нести зміни в скролл
					$(window).scroll( function() {
						$('._anim-items').addClass('_active-scroll-animation');
					});
				} else {
					$('#sales_loadmore').parent().parent().remove();
				}
			}
		});
	});
	
	// Shop - button load more
	$('#products_loadmore').click(function(){
		event.preventDefault();
		$(this).text(iv_ajax_object.downloading);
		var data = {
			'action': 'products_loadmore',
			'query': true_posts,
			'page' : current_page
		};
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			success:function(data){
				if( data ) { 
					//console.log(data);
					$('#products_loadmore').text(iv_ajax_object.watch_all);
					$('.sectio-all-product').append(data);
					current_page++;
					if (current_page == max_pages) $("#products_loadmore").remove();
					// тут можна нести зміни в скролл
					$(window).scroll( function() {
						$('._anim-items').addClass('_active-scroll-animation');
					});
				} else {
					$('#products_loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
				}
			}
		});
	});
	
	// Single add to cart by ajax
	$( document ).on( 'click', '.single_add_to_cart_button', function(e) {
		var this_button = $(this);	
		if ( this_button.hasClass('wc-variation-selection-needed') ) { return false; }		
		e.preventDefault();
		
		var variation_form = $( this ).closest( 'form.cart' );
		var product_id = variation_form.find( 'input[name=product_id]' ).val();
		var quantity = variation_form.find( 'input[name=quantity]' ).val();
		
		if ( variation_form.hasClass('variations_form') ) {
			var var_id = variation_form.find( 'input[name=variation_id]' ).val();
			
			if ( ! var_id.length ) { var_id = ''; }
			var item = {};
			var variations = variation_form.find( 'select[name^=attribute]' );
				if ( !variations.length) {
					variations = variation_form.find( '[name^=attribute]:checked' );
				}
				if ( !variations.length) {
					variations = variation_form.find( 'input[name^=attribute]' );
				}
						
				variations.each( function() {
					var $this = $( this ),
						attributeName = $this.attr( 'name' ),
						attributevalue = $this.val();
					if ( attributevalue.length > 0 ) {
						item[attributeName] = attributevalue;
					}
				} );
				
				if ( !variations.length) { alert('Need to chouse any variation!'); return false; }
		
			var data = {
				action: 'product_ajax_add_to_cart',
				security: jQuery('#security').val(),
				product_id: product_id,
				quantity: quantity,
				variation_id: var_id,
				variation: item
			};
		} else {
			var data = {
				action: 'product_ajax_add_to_cart',
				security: jQuery('#security').val(),
				product_id: product_id,
				quantity: quantity
			};
		}
		
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			beforeSend: function (response) {
				this_button.addClass('la-fleur-loading');
			},
			complete: function (response) {
				this_button.removeClass('la-fleur-loading');
			},
			success:function(response){
				jQuery('#la_fleur_mini_cart').html(response);
				jQuery('.la_fleur_mini_cart_count').text( jQuery('#custom_cart_count').val() );
				openShopCard();
			}
		});
	});
	
	// Beaty_set add to cart by ajax
	$('.btn_array_add_to_cart').click(function(e){
		e.preventDefault();
		var this_button = $(this);		
		var beauty_set = $('#array_add_to_cart').val();
		if ( ! beauty_set.length ) { return false; }
		
		var data = {
			action: 'beauty_set_ajax_add_to_cart',
			security: jQuery('#security').val(),
			beauty_set: beauty_set
		};
		
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			beforeSend: function (response) {
				this_button.addClass('la-fleur-loading');
			},
			complete: function (response) {
				this_button.removeClass('la-fleur-loading');
			},
			success:function(response){			
				$('#la_fleur_mini_cart').html(response);
				$('.la_fleur_mini_cart_count').text( jQuery('#custom_cart_count').val() );
				openShopCard();
			}
		});
	});
	
	// Grouped product add to cart
	$('.group_add_to_cart_button').click( function(e) {
		//var this_form = $('form.grouped_form');
		var this_button = $(this);		
		e.preventDefault();
		
		var arr = $('.grouped_form .qty');
		var check = false;
		var item = {};
		$.each(arr, function() {		
			var attributeName = $(this).attr( 'name' );
			var attributevalue = $(this).val();
			if ( attributevalue.length > 0 ) {
				item[attributeName] = attributevalue;
				check = true;
			}		
		});
		if ( check === false ) { alert('Need to chouse any quantity!'); return false; }
		
		var data = {
			action: 'group_ajax_add_to_cart',
			security: $('#security').val(),
			item: item
		};
		
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			beforeSend: function (response) {
				this_button.addClass('la-fleur-loading');
			},
			complete: function (response) {
				this_button.removeClass('la-fleur-loading');
			},
			success:function(response){	
				console.log(response);		
				$('#la_fleur_mini_cart').html(response);
				$('.la_fleur_mini_cart_count').text( jQuery('#custom_cart_count').val() );
				openShopCard();
				$.each(arr, function() {
					$(this).val(0); 
				});
			}
		});
	});
	
	// My account - remove address
	$('body.page-template-tpl-my-account .cabinet-adress-remove').click( function(e) {
		var this_obj = $(this).closest('.cabinet-show-all-adress_itm');		
		e.preventDefault();
		
		var data = {
			action: 'my_account_remove_address',
			security: $('#security').val(),
			address_id: this_obj.attr('data-address-id')
		};
		
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			beforeSend: function (responseJson) {
				this_obj.addClass('la-fleur-loading');
			},
			complete: function (responseJson) {
				this_obj.removeClass('la-fleur-loading');
			},
			success:function(responseJson){	
				//console.log(responseJson);
				var response = JSON.parse( responseJson );		
				this_obj.remove();
				$('#response_text').text( response.text );
				
			}
		});		
	});
	
	// My account - active_shipping_address
	$('body.page-template-tpl-my-account .block-for-input-filter, body.page-template-tpl-my-account .cabinet-your-adrss' ).click( function(e) {
		var this_obj = $(this).closest('.cabinet-show-all-adress_itm');		
		e.preventDefault();
		
		var data = {
			action: 'my_account_active_shipping_address',
			security: $('#security').val(),
			address_id: this_obj.attr('data-address-id')
		};
		
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			beforeSend: function (responseJson) {
				this_obj.addClass('la-fleur-loading');
			},
			complete: function (responseJson) {
				this_obj.removeClass('la-fleur-loading');
			},
			success:function(responseJson){	
				console.log(responseJson);
				var response = JSON.parse( responseJson );		
				//this_obj.remove();
				$('#response_text').text( response.text );
				this_obj.find('input[type="radio"]').attr('checked', 'checked');
			}
		});		
	});
	
	// My accaunt - edit address
	$('body.page-template-tpl-my-account .cabinet-adress-edit').click( function(e) {
		var this_obj = $(this).closest('.cabinet-show-all-adress_itm');		
		e.preventDefault();

		var data = {
			action: 'my_account_edit_address',
			security: $('#security').val(),
			address_id: this_obj.attr('data-address-id')
		};
		
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			beforeSend: function (responseJson) {
				this_obj.addClass('la-fleur-loading');
			},
			complete: function (responseJson) {
				this_obj.removeClass('la-fleur-loading');
			},
			success:function(responseJson){	
				console.log(responseJson);
				var response = JSON.parse( responseJson );		
				//this_obj.remove();
				$('#response_text').text( response.text );
				//this_obj.find('input[type="radio"]').attr('checked', 'checked');
				
				$('#city-personal-address').val( response.city );
				$('#street-personal-address').val( response.street );
				$('#house-personal-address').val( response.house );			
				$('#flat-personal-address').val( response.flat );
				$('input[name="active_shipping_address"]').val( response.active_shipping_address );
				$('input[name="address_id"]').val( response.address_id );
				
				//alert( $('header').height() );
				$('.cabinet-small-title').text( iv_ajax_object.edit_address );
				
				var fixed_offset = $('header').height();
				$('html,body').stop().animate({ scrollTop: fixed_offset }, 1000);
			}
		});		
	});
	
	// Start test, for category tests
	$('#link_start_test').click(function(){		
		jQuery('form#start_test').submit();
		return false;
	});
	
	// Sent test, for category tests
	$('#link_sent_test').click(function(){		
		jQuery('form#page-test').submit();
		return false;
	});
	
	// add to cart	
	$('.ajax_add_to_cart').on( 'click', function(e) {
		$(this).addClass('la-fleur-loading');
	});
	
	// added_to_cart button
	$('body').on( 'added_to_cart', function(e) {
		if ( $('.ajax_add_to_cart').hasClass('la-fleur-loading') ) {
			$('.ajax_add_to_cart').removeClass('la-fleur-loading')
		}
		openShopCard();
	});
	
	// remove from mini cart	
	$('body').on( 'removed_from_cart', function(e) {
		$('#la_fleur_mini_cart').css('opacity', '1');
	});
	
	// remove from cart	
	$('body').on( 'removed_from_cart', function(e) {
		var data = {
			action: 'ajax_remove_from_cart',
			security: jQuery('#security').val(),
			//beauty_set: beauty_set
		};
		
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			//beforeSend: function (response) {
				//this_button.addClass('la-fleur-loading');
			//},
			complete: function (response) {
				//this_button.removeClass('la-fleur-loading');
				$('#la_fleur_cart').css('opacity', '1');
			},
			success:function(response){			
				$('#la_fleur_cart').html(response);
				$('.la_fleur_mini_cart_count').text( jQuery('#cart_count').val() );
				//openShopCard();
			}
		});	
	});
	
	// remove from checkout
	$('body').on( 'removed_from_cart', function(e) {
		var data = {
			action: 'ajax_remove_from_checkout',
			security: jQuery('#security').val(),
			//beauty_set: beauty_set
		};
		
		$.ajax({
			url:iv_ajax_object.ajaxurl,
			data:data,
			type:'POST',
			//beforeSend: function (response) {
				//this_button.addClass('la-fleur-loading');
			//},
			complete: function (response) {
				//this_button.removeClass('la-fleur-loading');
				$('#la_fleur_checkout').css('opacity', '1');
			},
			success:function(response){			
				$('#la_fleur_checkout').html(response);
				$('.la_fleur_mini_cart_count').text( jQuery('#cart_count').val() );
				//openShopCard();
			}
		});	
	});
	
	// Shop, filters, duble of link
	$('body.post-type-archive-product .block-for-input-filter').on('click', function(){
		var termLink;
		termLink = $(this).parent().find('a').attr('href');
		window.location.href=termLink;
	});
	
	// content-single-product.php change quantity
	$('.product_count-plus').on('click', function(){
		event.preventDefault();
		let num, input;
		input = $(this).parent().find('input[type="number"]');
		num = input.val();
		num = Number( num ) + 1;
		input.val(num);
	});
	$('.product_count-minus').on('click', function(){
		event.preventDefault();
		let num, input;
		input = $(this).parent().find('input[type="number"]');
		num = input.val();
		if ( num > 1 ) {
			num = Number( num ) - 1;
			input.val(num);
		}		
	});
	
	// orders, filter by date on frontend
	$('.iv-filter-by-date').change( function(e) {
		event.preventDefault();	
		var mLink;
		mLink = $(this).val();
		window.location.href=mLink;
	});
	
	// on update payment, page payment
	$('#order-paymend-method').change(function(event){
		let payment_method = $(this).val();
		$('input[type="radio"][value="'+ payment_method +'"]').click();
	});
	
	// on update shipping, page payment
	$('#order-delivery').change(function(){
		let shipping_method = $(this).val();
		let shipping_payment_method = jQuery('#order-paymend-method').val(); // bacs cod
		$('input[type="radio"][value="'+ shipping_method +'"]').click();
		
		let calculated_total = 0;
		let checkout_sum = Number( $('#checkout_sum').text() );
		let checkout_shipping = $('#order-delivery option:selected').attr('data-shippinf-cost');
		$('#checkout_shipping').html( checkout_shipping );
		calculated_total += parseInt( checkout_sum );
		calculated_total += parseInt( checkout_shipping );
		$('#checkout_total').html( Number(calculated_total) );
			
		// local_pickup:2 flat_rate:4 flat_rate:5
		if ( shipping_method == 'local_pickup:2' ) {
			//$('#city-order-st-2').prop('disabled', false);
			
			jQuery('#by_courier_city').css('display', 'none');
			jQuery('#nova_poshta_option').css('display', 'none');
			jQuery('#by_courier_street').css('display', 'none');
			jQuery('#by_courier_house').css('display', 'none');
			
			$('#order-paymend-method').prop('disabled', false);
		}
		if ( shipping_method == 'flat_rate:4' ) {
			jQuery('#by_courier_city').css('display', 'block');
			jQuery('#nova_poshta_option').css('display', 'none');
			jQuery('#by_courier_street').css('display', 'block');
			jQuery('#by_courier_house').css('display', 'flex');
			
			//$('#city-order-st-2').val('Кишинёв');
			//$('#city-order-st-2').prop('disabled', true);
			
			$('#order-paymend-method').prop('disabled', false);
		}		
		if ( shipping_method == 'flat_rate:5' ) {
			//$('#city-order-st-2').prop('disabled', false);
			
			jQuery('#by_courier_city').css('display', 'none');
			jQuery('#nova_poshta_option').css('display', 'block');
			jQuery('#by_courier_street').css('display', 'none');
			jQuery('#by_courier_house').css('display', 'none');			
					
			if ( shipping_payment_method == 'cod' ) {
				jQuery('#order-paymend-method').val('bacs').change();
				//jQuery('#order-paymend-method option[value="cod"]').css('display', 'none');				
			}
			$('#order-paymend-method').prop('disabled', true);
		}
		
	});
	
	
	
});


jQuery(window).on('load', function() {
	let shipping_method = jQuery('#order-delivery').val();
	let shipping_payment_method = jQuery('#order-paymend-method').val();
	
	// local_pickup:2, flat_rate:4, flat_rate:5, checkout.php -> str. 177
	if ( shipping_method == 'local_pickup:2' ) {
			//$('#city-order-st-2').prop('disabled', false);
			
			jQuery('#by_courier_city').css('display', 'none');
			jQuery('#nova_poshta_option').css('display', 'none');
			jQuery('#by_courier_street').css('display', 'none');
			jQuery('#by_courier_house').css('display', 'none');
			
			$('#order-paymend-method').prop('disabled', false);
		}
		if ( shipping_method == 'flat_rate:4' ) {
			jQuery('#by_courier_city').css('display', 'block');
			jQuery('#nova_poshta_option').css('display', 'none');
			jQuery('#by_courier_street').css('display', 'block');
			jQuery('#by_courier_house').css('display', 'flex');
			
			//$('#city-order-st-2').val('Кишинёв');
			//$('#city-order-st-2').prop('disabled', true);
			
			$('#order-paymend-method').prop('disabled', false);
		}		
		if ( shipping_method == 'flat_rate:5' ) {
			//$('#city-order-st-2').prop('disabled', false);
			
			jQuery('#by_courier_city').css('display', 'none');
			jQuery('#nova_poshta_option').css('display', 'block');
			jQuery('#by_courier_street').css('display', 'none');
			jQuery('#by_courier_house').css('display', 'none');			
					
			if ( shipping_payment_method == 'cod' ) {
				jQuery('#order-paymend-method').val('bacs').change();
				//jQuery('#order-paymend-method option[value="cod"]').css('display', 'none');				
			}
			$('#order-paymend-method').prop('disabled', true);
		}
	
	// woocommerce notice for checkout
	var billing_first_name = $('.woocommerce-error li').attr('data-id', 'billing_first_name');
	var billing_last_name = $('.woocommerce-error li').attr('data-id', 'billing_last_name');
	var billing_phone = $('.woocommerce-error li').attr('data-id', 'billing_phone');
	var billing_email = $('.woocommerce-error li').attr('data-id', 'billing_email');
	
	if ( billing_first_name.length > 0 || billing_last_name.length > 0 || billing_phone.length > 0 || billing_email.length > 0  ) {
		$('.checkout-p-required').css('display', 'block');
	} else {
		$('.checkout-p-required').css('display', 'none');
	}
});
