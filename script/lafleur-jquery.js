$(document).ready(function () {

    $('#section-all-breds__img').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#section-all-breds',
        fade: true,
        cssEase: 'linear',
    });

    $('#section-all-breds').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: $('#btn-prev_brand'),
        nextArrow: $('#btn-next_brand'),
        asNavFor: '#section-all-breds__img',
        centerMode: true,
        focusOnSelect: false,
    });

    // page-test-slider_this
    // slider for test
    $('#page-test-slider_this').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        prevArrow: $('.btn-left-slider-test'),
        nextArrow: $('.btn-right-slider-test'),
        cssEase: 'linear',
    });

    // test rezult strt

    // rezult small
    $('#test-result-pr-info').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#test-result-pr-other',
        fade: true,
        cssEase: 'linear',
    });

    // rezult big
    $('#test-result-pr-other').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: $('#btn-left-slider-rezult'),
        nextArrow: $('#btn-right-slider-rezult'),
        asNavFor: '#test-result-pr-info',
        centerMode: false,
        focusOnSelect: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    speed: 500,
                    fade: true,
                    cssEase: 'linear',
                }
            },
        ]
    });
    // test rezult end

    // sales main slider
    // page-sales-rec-pr-slider
    $('#page-sales-rec-pr-slider').slick({
        // infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: $('#btn-left-slider-p-sales'),
        nextArrow: $('#btn-right-slider-p-sales'),
        cssEase: 'linear',
        // centerMode: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    speed: 500,
                    fade: true,
                    cssEase: 'linear',
                }
            },
        ]
    });

    // slider product big img
    $('#product-big-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#product-small-slider',
        fade: true,
        cssEase: 'linear',
    });

    // slider product small img
    $('#product-small-slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: $('#btn-left-product-big-slider'),
        nextArrow: $('#btn-right-product-big-slider'),
        asNavFor: '#product-big-slider',
        centerMode: false,
        focusOnSelect: false,
    });

    $('#mobile-brand-items').slick({
        infinite: false,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        prevArrow: $('#btn-prev_mobile-brand'),
        nextArrow: $('#btn-next_mobile-brand'),
    });

    $(".small-select").each(function () { // бежим по всем селектам
        $(this).select2({ // ини циализируем каждый отдельно
            minimumResultsForSearch: Infinity,
            width: '100%',
            allowClear: false,
            dropdownParent: $(this).closest('.container_for_small-select') // выбираем конкретный элемент с классом, относительно текущего селекта
        })
    });


    $(".select-with-border-into").each(function () { // бежим по всем селектам
        $(this).select2({ // ини циализируем каждый отдельно
            minimumResultsForSearch: Infinity,
            width: '100%',
            allowClear: false,
            dropdownParent: $(this).closest('.container-with-border-into') // выбираем конкретный элемент с классом, относительно текущего селекта
        })
    });

    if (window.innerWidth <= 1024) {
        // slider mobile brand
        $('#main-brand_show-news_slider').slick({
            // slidesToShow: 1,
            // variableWidth: true,

            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            prevArrow: $('#btn-prev_mobile-perfumency-news'),
            nextArrow: $('#btn-next_mobile-perfumency-news'),
        });
    }

    $('.mask-phone').inputmask('+399 999 99 9999', {
        jitMasking: 30,
        showMaskOnHover: false,
    });
});