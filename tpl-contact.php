<?php
/**
 * Template name: Template Contact
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package  LaFleur
 */

get_header();
?>

        <!-- <section pagination start -->
		<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
		<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
			<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
				<div class="section-pagination__wrapper all-width">
					<ul class="breadcrumb">
						<?php woocommerce_breadcrumb(); ?>
					</ul>
				</div>
			</div>
		<?php } ?>
		<!-- <section pagination end -->

        <!-- сщтефсе title -->
        <?php
        $lf_main_data = get_field( 'lf_main_data', 'option' );
        $page_contact = get_field( 'page_contact' );
        ?>
        <div class="page-about_title w_100">
            <div class="page-about_title_wr all-width d-f-column-f_c-f_s">
                <div class="all_big_title abt-title_this">
				
                    <h2 class="animation-opacity _anim-items _active-scroll-animation w_100">
                        <?php if ( ! empty( $page_contact['text_one'] ) ) { ?>
							<div class="abt-title">
								<p><?php echo esc_html( $page_contact['text_one'] ); ?></p>
								<span></span>
							</div>
						<?php } ?>

						<?php if ( ! empty( $page_contact['text_two']) ) { ?>
							<div class="abt-title">
								<span></span>
								<p><?php echo esc_html( $page_contact['text_two'] ); ?></p>
							</div>
						<?php } ?>
                    </h2>                   
                    
                </div>
            </div>
        </div>

        <div class="page-contact-info w_100">
            <div class="page-contact-info_wr all-width d-f-row-stre-s_b">
				<div class="page-contact-info_l">
					<?php if ( ! empty( $page_contact['image'] ) ) { ?>					
						<img src="<?php echo esc_url( $page_contact['image']['url'] ); ?>" alt="<?php echo esc_html( $page_contact['image']['alt'] ); ?>">
					<?php } else { ?>
						<img src="<?php echo LA_FLEUR_THEME_URI; ?>/img/png/contact.png" alt="image">					
					<?php } ?>
				</div>

                <div class="page-contact-info_r">
                    <!-- top -->
                    <div class="page-contact-info_r_top w_100">
						<?php if ( ! empty( $lf_main_data['address'] ) || ! empty( $lf_main_data['address_google_map'] ) ) { ?>
							<div class="contact-title w_100 d-f-row-c-f_s">
								<svg width="21" height="21" viewBox="0 0 21 21" fill="none"
									xmlns="http://www.w3.org/2000/svg">
									<circle cx="10.0461" cy="10.1731" r="9.69612" stroke="black" stroke-width="0.7" />
									<path
										d="M10.0447 3.26123C7.21204 3.26123 4.91016 5.58003 4.91016 8.4335C4.91016 9.3401 5.29957 10.3164 5.34573 10.424C5.4813 10.7465 5.74668 11.2463 5.93995 11.5427L9.45911 16.9125C9.60334 17.1334 9.8168 17.2612 10.0447 17.2612C10.2726 17.2612 10.486 17.1334 10.6302 16.9154L14.1494 11.5456C14.3427 11.2521 14.6081 10.7494 14.7436 10.4269C14.7609 10.3833 15.1792 9.36625 15.1792 8.43641C15.1792 5.58003 12.8744 3.26123 10.0447 3.26123ZM10.0447 16.6539C10.0187 16.6539 9.98699 16.6074 9.96679 16.5755L6.44763 11.2056C6.27455 10.9412 6.02648 10.4734 5.90821 10.1886C5.89091 10.1479 5.5188 9.2384 5.5188 8.4335C5.5188 5.92001 7.54953 3.87435 10.0447 3.87435C12.5398 3.87435 14.5706 5.92001 14.5706 8.4335C14.5706 9.25583 14.1869 10.1799 14.1811 10.1886C14.0629 10.4763 13.8148 10.9412 13.6417 11.2056L10.1226 16.5755C10.1024 16.6074 10.0706 16.6539 10.0447 16.6539Z"
										fill="black" />
									<path
										d="M9.84515 5.69385C8.34518 5.69385 7.125 6.92291 7.125 8.4338C7.125 9.9447 8.34518 11.1738 9.84515 11.1738C11.3451 11.1738 12.5653 9.9447 12.5653 8.4338C12.5653 6.92291 11.3451 5.69385 9.84515 5.69385ZM9.84515 10.5607C8.67979 10.5607 7.73365 9.60766 7.73365 8.4338C7.73365 7.25995 8.67979 6.30692 9.84515 6.30692C11.0105 6.30692 11.9567 7.25995 11.9567 8.4338C11.9567 9.60475 11.0105 10.5607 9.84515 10.5607Z"
										fill="black" />
								</svg>

								<h3><?php _e( 'Адрес:', 'la-fleur' ); ?></h3>
							</div>

							<a href="<?php echo esc_url( $lf_main_data['address_google_map'] ); ?>">
								<?php echo wp_kses_post( $lf_main_data['address'] ); ?>
							</a>
						<?php } ?>
                    </div>

                    <!-- bottom -->
                    <div class="page-contact-info_r_bottom w_100 d-f-row-stre-s_b">
                        <!-- left -->
                        <?php if ( ! empty( $lf_main_data['phone_one'] ) || ! empty( $lf_main_data['phone_two'] ) ) { ?>
							<div class="page-contact-info_r_bottom_itm d-f-column-f_s-f_s">
								<div class="contact-title w_100 d-f-row-c-f_s">
									<svg width="21" height="21" viewBox="0 0 21 21" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<path
											d="M15.77 13.1247L13.8633 11.2247C13.4305 10.7909 12.728 10.79 12.2943 11.2228C12.2936 11.2234 12.293 11.224 12.2924 11.2247L11.1999 12.3172C11.1172 12.4 10.987 12.4115 10.8912 12.3443C10.2932 11.9264 9.73347 11.4563 9.2185 10.9397C8.75729 10.4795 8.33301 9.98375 7.94956 9.45697C7.88006 9.36234 7.89016 9.23113 7.9733 9.14823L9.09294 8.02859C9.52521 7.59548 9.52521 6.89419 9.09294 6.46108L7.18617 4.55431C6.74678 4.13424 6.05468 4.13424 5.61529 4.55431L5.01137 5.15822C4.09932 6.06002 3.77372 7.39907 4.16993 8.61895C4.46559 9.51144 4.8835 10.3587 5.41172 11.1364C5.88725 11.8495 6.43279 12.5133 7.04029 13.1179C7.70074 13.7831 8.43233 14.3736 9.22188 14.8788C10.0898 15.4452 11.0429 15.869 12.0447 16.1341C12.3023 16.1976 12.5666 16.2295 12.8319 16.2291C13.7412 16.2235 14.6112 15.8575 15.251 15.2113L15.7701 14.6921C16.2023 14.2591 16.2023 13.5578 15.77 13.1247ZM15.2896 14.2294C15.2891 14.2299 15.2887 14.2303 15.2882 14.2308L15.2916 14.2206L14.7725 14.7397C14.1055 15.4151 13.1341 15.693 12.2109 15.4726C11.2807 15.2236 10.3964 14.8273 9.59161 14.2986C8.84388 13.8208 8.15094 13.2621 7.52536 12.6327C6.94975 12.0613 6.43264 11.4339 5.9816 10.7599C5.48826 10.0346 5.09777 9.24445 4.82124 8.41199C4.50423 7.43405 4.76693 6.36097 5.49983 5.64004L6.10374 5.03612C6.27165 4.86746 6.54448 4.86686 6.7131 5.03477C6.71354 5.03521 6.71402 5.03564 6.71445 5.03612L8.62123 6.9429C8.78989 7.1108 8.79048 7.38364 8.62258 7.55226C8.62214 7.5527 8.6217 7.55314 8.62123 7.55361L7.50158 8.67326C7.18032 8.99102 7.13993 9.49598 7.4066 9.86077C7.81155 10.4165 8.25969 10.9395 8.74679 11.4249C9.28987 11.9703 9.88027 12.4665 10.5111 12.9076C10.8755 13.1618 11.3697 13.119 11.685 12.8058L12.7673 11.7065C12.9352 11.5379 13.208 11.5373 13.3766 11.7052C13.3771 11.7056 13.3775 11.706 13.378 11.7065L15.2882 13.6201C15.4569 13.788 15.4575 14.0608 15.2896 14.2294Z"
											fill="black" />
										<circle cx="10.0461" cy="10.2336" r="9.69612" stroke="black" stroke-width="0.7" />
									</svg>

									<h3><?php _e( 'Телефон:', 'la-fleur' ); ?></h3>
								</div>
								<a href="tel: <?php echo esc_html( $lf_main_data['phone_one'] ); ?>"><?php echo esc_html( $lf_main_data['phone_one'] ); ?></a>
								<a href="tel: <?php echo esc_html( $lf_main_data['phone_two'] ); ?>"><?php echo esc_html( $lf_main_data['phone_two'] ); ?></a>
							</div>
						<?php } ?>

                        <!-- right -->
                        <?php if ( ! empty( $lf_main_data['working_days'] ) || ! empty( $lf_main_data['weekend'] ) ) { ?>
							<div class="page-contact-info_r_bottom_itm d-f-column-f_s-f_s">
								<div class="contact-title w_100 d-f-row-c-f_s">
									<svg width="21" height="21" viewBox="0 0 21 21" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<circle cx="10.5617" cy="10.1877" r="9.69612" stroke="black" stroke-width="0.7" />
										<path fill-rule="evenodd" clip-rule="evenodd"
											d="M9.67394 10.7737L16.7386 10.7737C16.9434 10.7737 17.1094 10.9396 17.1094 11.1444C17.1094 11.3491 16.9434 11.5151 16.7386 11.5151L9.67394 11.5151L9.67394 10.7737Z"
											fill="black" />
										<path fill-rule="evenodd" clip-rule="evenodd"
											d="M9.67648 10.7736L9.67648 4.61928C9.67648 4.41452 9.84247 4.24854 10.0472 4.24854C10.252 4.24854 10.418 4.41452 10.418 4.61928L10.418 10.7736L9.67648 10.7736Z"
											fill="black" />
									</svg>

									<h3><?php _e( 'ГРАФИК РАБОТЫ:', 'la-fleur' ); ?></h3>
								</div>
								<p><?php echo esc_html( $lf_main_data['working_days'] ); ?></p>
								<p><?php echo esc_html( $lf_main_data['weekend'] ); ?></p>
							</div>
						<?php } ?>
                    </div>
                </div>
            </div>
        </div>

<?php
get_footer();
