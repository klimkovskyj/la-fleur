<?php

/**
 * Template name: Template Test
 * Template Post Type: post, page
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package  LaFleur
 */

get_header();
?>

        <!-- <section pagination start -->
		<?php if ( ! IS_MOBIL ) { $desktop_or_mobil = 'is_descktop'; } else { $desktop_or_mobil = 'is_mobile'; } ?>
		<?php if ( function_exists('woocommerce_breadcrumb') ) { ?>
			<div class="section-pagination <?php echo $desktop_or_mobil; ?>">
				<div class="section-pagination__wrapper all-width">
					<ul class="breadcrumb">
						<?php woocommerce_breadcrumb(); ?>
					</ul>
				</div>
			</div>
		<?php } ?>
		<!-- <section pagination end -->
        
        <?php
        if ( ( ! isset( $_POST['test_start'] ) || $_POST['test_start'] != 'go' ) && ( ! isset( $_POST['test_completed'] ) || $_POST['test_completed'] != 'yes' ) ) {
			$singl_test_main = get_field( 'singl_test_main' );
			?>
			<div class="page-test w_100">
				<div class="page-test_wr w_100 d-f-column-f_c-f_s">
					<!-- title -->
					<div class="page-test_title w_100">
						<div class="page-test_title_wr all-width d-f-column-f_c-f_s">
							<?php
							if ( ! IS_MOBIL ) {
								if ( ! empty( $singl_test_main['title_one'] ) ) {
									echo '<div class="page-test_title-1 is_descktop">';
									echo '<h2> '. esc_html( $singl_test_main['title_one'] ) .' </h2>';
									echo '</div>';
								}
								if ( ! empty( $singl_test_main['title_two'] ) ) {	
									echo '<div class="page-test_title-2 is_descktop">';
									echo '<h2> '. esc_html( $singl_test_main['title_two'] ) .' </h2>';
									echo '</div>';
								}
							} else {
								if ( ! empty( $singl_test_main['title_three'] ) ) {
									echo '<div class="page-test_title-itm is_mobile">';
									echo '<p>'. esc_html( $singl_test_main['title_three'] ) .'</p>';
									echo '<span></span>';
									echo '</div>';
								}
								if ( ! empty( $singl_test_main['title_four'] ) ) {
									echo '<div class="page-test_title-itm is_mobile">';
									echo '<span></span>';
									echo '<p>'. esc_html( $singl_test_main['title_four'] ) .'</p>';
									echo '</div>';
								}
								if ( ! empty( $singl_test_main['title_five'] ) ) {
									echo '<div class="page-test_title-itm is_mobile">';
									echo '<p>'. esc_html( $singl_test_main['title_five'] ) .'</p>';
									echo '<span></span>';
									echo '</div>';
								}
							}
							?>	
						</div>
					</div>

					<!-- img for mobile -->
					<div class="page-test_img is_mobile">
						<?php if ( ! empty( $singl_test_main['img_right'] ) ) { ?>
							<img src="<?php echo esc_url( $singl_test_main['img_right']['url'] ); ?>" alt="<?php echo esc_html( $singl_test_main['img_right']['alt'] ); ?>">
						<?php } ?>
					</div>

					<!-- info -->
					<div class="page-test_cnt w_100">
						<div class="page-test_cnt_wr all-width d-f-row-c-s_b">
							<p><?php if ( ! empty( $singl_test_main['info_one'] ) ) { echo esc_html( $singl_test_main['info_one'] ); } ?> 
							<br><?php if ( ! empty( $singl_test_main['info_two'] ) ) { echo esc_html( $singl_test_main['info_two'] ); } ?></p>

							<div class="all_link hover_effect-for">							
								<form id="start_test" action="" method="post">
								<input type="hidden" name="test_start" value="go"></form>
								<?php
								if ( ! empty( $singl_test_main['button_start'] ) ) {
									echo '<a id="link_start_test" href="#">';
									echo '<p>'. esc_html( $singl_test_main['button_start'] ) .'</p>';
									echo '</a>';
								}
								?>	
							</div>
						</div>
					</div>
				</div>
			</div>
        <?php } // part one?>
        
        <?php
        if ( isset( $_POST['test_start'] ) && $_POST['test_start'] == 'go' ) {
			$singl_test_main = get_field( 'singl_test_main' );
			$singl_test_step = get_field( 'singl_test_step' );
			$button_sent = get_field( 'button_sent' );
			?>
			<div class="page-test w_100">
				<div class="page-test_wr w_100 d-f-column-f_c-f_s">
					<!-- title -->
					<div class="page-test_title w_100 is_descktop">
						<div class="page-test_title_wr page-test_title_wr-step all-width d-f-column-f_c-f_s">
							<?php
							if ( ! IS_MOBIL ) {
								if ( ! empty( $singl_test_main['title_one'] ) ) {
									echo '<div class="page-test_title-1 is_descktop">';
									echo '<h2> '. esc_html( $singl_test_main['title_one'] ) .' </h2>';
									echo '</div>';
								}
								if ( ! empty( $singl_test_main['title_two'] ) ) {	
									echo '<div class="page-test_title-2 is_descktop">';
									echo '<h2> '. esc_html( $singl_test_main['title_two'] ) .' </h2>';
									echo '</div>';
								}
							} else {
								if ( ! empty( $singl_test_main['title_three'] ) ) {
									echo '<div class="page-test_title-itm is_mobile">';
									echo '<p>'. esc_html( $singl_test_main['title_three'] ) .'</p>';
									echo '<span></span>';
									echo '</div>';
								}
								if ( ! empty( $singl_test_main['title_four'] ) ) {
									echo '<div class="page-test_title-itm is_mobile">';
									echo '<span></span>';
									echo '<p>'. esc_html( $singl_test_main['title_four'] ) .'</p>';
									echo '</div>';
								}
								if ( ! empty( $singl_test_main['title_five'] ) ) {
									echo '<div class="page-test_title-itm is_mobile">';
									echo '<p>'. esc_html( $singl_test_main['title_five'] ) .'</p>';
									echo '<span></span>';
									echo '</div>';
								}
							}
							?>
						</div>
					</div>

					<div class="page-test-slider w_100">
						<div class="page-test-slider_wr all-width">
							<form id="page-test" action="" method="post" class="w_100">

								<div id="page-test-slider_this" class="page-test-slider_this w_100">

									<!-- slide 1 -->
									<?php
									if ( ! empty( $singl_test_step ) ) {
										$count_steps = count( $singl_test_step );
										$current_step = 1;
										foreach ( $singl_test_step as $step ) {
											?>
											<div class="page-test-slider_this_itm">
												<div class="page-test-slider_itm w_100">
													<div class="page-test-slider_itm_wr w_100">
														<!-- step -->
														<div class="text-step-slider">
															<p><?php _e( 'шаг', 'la-fleur' ); ?> <span><?php echo $current_step; ?>/<span class="span-step-test"><?php echo $count_steps; ?></span></span></p>
														</div>
													</div>
												</div>

												<div class="page-test-our-question w_100 d-f-column-f_c-f_s">
													<div class="page-test-our-question_title w_100">
														<?php
														if ( ! empty( $step['question_text'] ) ) {
															echo '<p>'. esc_html( $step['question_text'] ) .'</p>';
														}
														?>
													</div>

													<div class="page-test-our-question_answer">
														<?php if ( ! empty( $step['answers'] ) ) { ?>
															<?php foreach ( $step['answers'] as $answer ) { ?>
																<div class="page-test-our-question_answer_itm">
																	<div class="section-shop-filter_item animation-this-duration">
																		<label class="d-f-row-c-f_s">
																			<input type="radio" name="results[<?php echo $current_step; ?>]" value="<?php echo intval( $answer['answer_point'] ); ?>" checked>
																			<div class="block-for-input-filter"></div>
																			<p><?php echo esc_html( $answer['answer_text'] ); ?></p>
																		</label>
																	</div>
																</div>
															<?php } ?>	
														<?php } ?>
														
													</div>
												</div>

												<!-- btn -->
												<div class="page-test-btn-slider">
													<!-- left -->
													<div class="beauty-big-slider_btn__arrow slick-arrow btn-left-slider-test">
														<svg width="107" height="14" viewBox="0 0 107 14" fill="none"
															xmlns="http://www.w3.org/2000/svg">
															<line x1="106.219" y1="7.23242" x2="1.00139" y2="7.23244"
																stroke="black"></line>
															<line x1="7.28512" y1="13.086" x2="0.867469" y2="6.66832"
																stroke="black"></line>
															<line x1="0.86715" y1="7.33199" x2="7.2848" y2="0.914344"
																stroke="black"></line>
														</svg>
													</div>

													<!-- right -->
													<?php if ( $count_steps == $current_step ) { ?>
														<div class="beauty-big-slider_btn__arrow slick-arrow btn-right-slider-test">
															<input type="hidden" name="test_completed" value="yes">
															<?php
															echo '<a id="link_sent_test" href="#">';
															echo '<p>'. esc_html( $button_sent ) .'</p>';
															echo '</a>';
															?>
														</div>
													<?php } else { ?>	
														<div class="beauty-big-slider_btn__arrow slick-arrow btn-right-slider-test">
															<svg width="107" height="14" viewBox="0 0 107 14" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<line x1="0.902344" y1="6.76782" x2="106.12" y2="6.76781"
																	stroke="black"></line>
																<line x1="99.836" y1="0.914269" x2="106.254" y2="7.33192"
																	stroke="black"></line>
																<line x1="106.254" y1="6.66825" x2="99.8363" y2="13.0859"
																	stroke="black"></line>
															</svg>
														</div>
													<?php } ?>
												</div>
											</div>
										<?php
										$current_step++;
										}
									}
									?>
									
								</div>

							</form>
						</div>
					</div>

				</div>
			</div>

		<?php
		} // part two
		?>
		
		<?php
		if ( isset( $_POST['test_completed'] ) && $_POST['test_completed'] == 'yes' ) {
			$singl_test_main = get_field( 'singl_test_main' );
			$singl_test_results_info = get_field( 'singl_test_results_info' );
			$test_results = get_field( 'singl_test_results' );
			$button_show_all = get_field( 'button_show_all' );
			
			$sum = 0;
			foreach ( $_POST['results'] as $result ) {
				$sum = $sum + intval($result); 
			}
			
			$temp_points = array();
			foreach ( $test_results as $result ) {
				$temp_points[] = intval($result['count_points']);
				 
			}
			sort($temp_points, SORT_NUMERIC);
			
			$i = 0;
			foreach ( $temp_points as $point ) {
				if ( $sum <= $point ) {
					$i = $point;
					break;
				}			
			}		
			
			foreach ( $test_results as $result ) {
				if ( $i == intval( $result['count_points'] ) ) :
					$actual_result = $result;
				endif;
			}
			
			if ( empty( $actual_result ) ) {
				$max_point = end($temp_points);
				
				foreach ( $test_results as $result ) :
					if ( $max_point == intval( $result['count_points'] ) ) :
						$actual_result = $result;
					endif;
				endforeach;
			}
			?>	
			<div class="page-test w_100">
				<div class="page-test_wr w_100 d-f-column-f_c-f_s">
					<!-- title -->
					<div class="page-test_title w_100 is_descktop">
						<div class="page-test_title_wr page-test_title_wr-step all-width d-f-column-f_c-f_s">
							<?php
							if ( ! IS_MOBIL ) {
								if ( ! empty( $singl_test_main['title_one'] ) ) {
									echo '<div class="page-test_title-1 is_descktop">';
									echo '<h2> '. esc_html( $singl_test_main['title_one'] ) .' </h2>';
									echo '</div>';
								}
								if ( ! empty( $singl_test_main['title_two'] ) ) {	
									echo '<div class="page-test_title-2 is_descktop">';
									echo '<h2> '. esc_html( $singl_test_main['title_two'] ) .' </h2>';
									echo '</div>';
								}
							} else {
								if ( ! empty( $singl_test_main['title_three'] ) ) {
									echo '<div class="page-test_title-itm is_mobile">';
									echo '<p>'. esc_html( $singl_test_main['title_three'] ) .'</p>';
									echo '<span></span>';
									echo '</div>';
								}
								if ( ! empty( $singl_test_main['title_four'] ) ) {
									echo '<div class="page-test_title-itm is_mobile">';
									echo '<span></span>';
									echo '<p>'. esc_html( $singl_test_main['title_four'] ) .'</p>';
									echo '</div>';
								}
								if ( ! empty( $singl_test_main['title_five'] ) ) {
									echo '<div class="page-test_title-itm is_mobile">';
									echo '<p>'. esc_html( $singl_test_main['title_five'] ) .'</p>';
									echo '<span></span>';
									echo '</div>';
								}
							}
							?>
						</div>
					</div>

					<div class="page-test-rezult w_100">
						<div class="page-test-rezult_wr w_100">

							<!-- info about rezult -->
							<div class="page-test-rezult_info d-f-row-stre-s_b all-width">
								<div class="page-test-rezult_info_l">
									<?php
									if ( ! empty( $singl_test_results_info ) ) {
										echo '<p>'. esc_html( $singl_test_results_info ) .'</p>';
									}
									?>
								</div>

								<div class="page-test-rezult_info_r">
								<?php
									if ( ! empty( $actual_result['description'] ) ) {									
										echo '<p>'. wp_kses_post( $actual_result['description'] ) .'</p>';									
									}
								?>
								</div>
							</div>

							<!-- reccomended product -->
							<div class="page-test-rezult_pr w_100">
								<div class="page-test-rezult_pr-wr d-f-row-stre-s_b all-width">
									<div class="page-test-rezult_pr_l">

										<!-- slider -->
										<div id="test-result-pr-info" class="page-test-rezult_pr_l_slider w_100">
											<?php
											
											if ( ! empty( $actual_result['offer_products'] ) ) {
												$posts = get_posts( array(
													'post_type' => 'product',
													'post_status' => 'publish',
													'include' => implode( ',', $actual_result['offer_products'] ),
													'orderby' => 'post__in',
												) );
											
												$product_images = '';
												foreach ( $posts as $post ) {
													setup_postdata( $post );
													global $product;
													if ( empty( $product ) || ! $product->is_visible() ) { return; }
													
													?>
													<div class="perfumency-with-product">
														<?php wc_get_template_part( 'content', 'product' ); ?>
													</div>
													<?php
													$product_images .= '<div class="test-result-pr-other_itm">';
													$product_images .= '<a href="'. esc_url( get_the_permalink() ) .'">';
													$product_images .= woocommerce_get_product_thumbnail('full');
													$product_images .= '</a></div>';
												}
												wp_reset_postdata();
											}
											?>
										</div>

									</div>

									<div class="page-test-rezult_pr_r">
										<div class="page-test-rezult_pr_r_wr">
											<!-- slider with img -->
											<div id="test-result-pr-other">
												<?php
													echo $product_images;
												?>
											</div>

											<div class="page-test-btn-slider">
												<!-- left -->
												<button class="beauty-big-slider_btn__arrow slick-arrow"
													id="btn-left-slider-rezult" aria-disabled="false" style="">
													<svg width="107" height="14" viewBox="0 0 107 14" fill="none"
														xmlns="http://www.w3.org/2000/svg">
														<line x1="106.219" y1="7.23242" x2="1.00139" y2="7.23244"
															stroke="black"></line>
														<line x1="7.28512" y1="13.086" x2="0.867469" y2="6.66832"
															stroke="black"></line>
														<line x1="0.86715" y1="7.33199" x2="7.2848" y2="0.914344"
															stroke="black"></line>
													</svg>
												</button>

												<!-- right -->
												<button class="beauty-big-slider_btn__arrow slick-arrow"
													id="btn-right-slider-rezult" style="" aria-disabled="false">
													<svg width="107" height="14" viewBox="0 0 107 14" fill="none"
														xmlns="http://www.w3.org/2000/svg">
														<line x1="0.902344" y1="6.76782" x2="106.12" y2="6.76781"
															stroke="black"></line>
														<line x1="99.836" y1="0.914269" x2="106.254" y2="7.33192"
															stroke="black"></line>
														<line x1="106.254" y1="6.66825" x2="99.8363" y2="13.0859"
															stroke="black"></line>
													</svg>
												</button>
											</div>
											<!-- link show more product -->
											<div class="all_link hover_effect-for link-more-rez-test is_mobile">
												<?php
												if ( ! empty( $button_show_all ) ) {
													echo '<a href="'. esc_url( home_url( '/catalog/' ) ) .'">';
													echo '<p>'. esc_html( $button_show_all ) .'</p>';
													echo '</a>';
												}
												?>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>

				</div>
			</div>
		<?php	
		} // part three
		?>
		
        
<?php
get_footer();
